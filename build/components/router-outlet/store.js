var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { isString, isFunction } from 'lodash';
var Router = /** @class */ (function () {
    function Router() {
        this._debug = false;
        this._subs = [];
        this._instructions = [];
    }
    Router.prototype.enableDebugging = function () {
        this._debug = true;
    };
    Router.prototype.subscribe = function (callback) {
        var _this = this;
        this._subs.push(callback);
        callback();
        return function () {
            _this._subs = _this._subs.filter(function (sub) { return sub !== callback; });
        };
    };
    Router.prototype.dispatch = function () {
        this._subs.forEach(function (callback) { return callback(); });
        if (this._debug) {
            console.log(this.path, this.component, this.params, this.data);
        }
    };
    Router.prototype.config = function (conf) {
        var _this = this;
        var keys = Object.keys(conf);
        this._instructions = keys.map(function (key) {
            return {
                path: _this.urlToParts(key),
                component: conf[key].component,
                redirect: conf[key].redirect
            };
        });
    };
    /**
     * navigate to a given url
     */
    Router.prototype.navigate = function (path, data) {
        if (data === void 0) { data = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var config, params, redirectTo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        config = this.match(path);
                        if (!config) return [3 /*break*/, 2];
                        params = this.parseParams(path, config.path);
                        return [4 /*yield*/, this.conformRedirect(config.redirect, path, params, data)];
                    case 1:
                        redirectTo = _a.sent();
                        if (redirectTo) {
                            return [2 /*return*/, this.navigate(redirectTo, data)];
                        }
                        else {
                            if (config.component) {
                                this.path = path;
                                this.params = params;
                                this.data = data;
                                if (isFunction(config.component)) {
                                    this.component = config.component(path, params, data);
                                }
                                else {
                                    this.component = config.component;
                                }
                                this.dispatch();
                                window.history.pushState('', '', path + this.encodeData(data));
                                window.scrollTo({ left: 0, top: 0 });
                            }
                            return [2 /*return*/];
                        }
                        return [3 /*break*/, 3];
                    case 2: return [2 /*return*/, this.navigate(Router.route404, data)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Router.prototype.urlToParts = function (url) {
        var cleanup = url.replace(/^\/+/g, '');
        return cleanup.split('/');
    };
    Router.prototype.parseParams = function (url, configParts) {
        var parts = this.urlToParts(url);
        return configParts.reduce(function (params, part, i) {
            if (part.substr(0, 1) === ':') {
                params[part.substr(1)] = parts[i];
                return params;
            }
            else {
                return params;
            }
        }, {});
    };
    Router.prototype.encodeData = function (data) {
        var keys = Object.keys(data);
        if (keys.length) {
            var params = keys.map(function (key) {
                return key + '=' + encodeURIComponent(data[key]);
            }, '');
            return '?' + params.join('&');
        }
        else {
            return '';
        }
    };
    Router.prototype.decodeData = function (data) {
        if (data) {
            var arr = data.substr(1).split('&');
            return arr.reduce(function (obj, pair) {
                var _a;
                var couple = pair.split('=');
                return __assign((_a = {}, _a[couple[0]] = decodeURIComponent(couple[1]), _a), obj);
            }, {});
        }
        else {
            return {};
        }
    };
    Router.prototype.match = function (url) {
        var parts = this.urlToParts(url);
        var configs = this._instructions.filter(function (entry) { return entry.path.length === parts.length; });
        var _loop_1 = function (i) {
            configs = configs.filter(function (entry) { return parts[i] === entry.path[i] || entry.path[i].substr(0, 1) === ':'; });
        };
        for (var i = 0; i < parts.length; i++) {
            _loop_1(i);
        }
        if (configs.length === 0) {
            console.warn('[ROUTER] no instruction match "' + url + '"');
            return null;
        }
        if (configs.length > 1) {
            console.warn('[ROUTER] multiple instruction match "' + url + '"');
        }
        return configs[0];
    };
    Router.prototype.conformRedirect = function (redirect, path, params, data) {
        if (redirect === void 0) { redirect = null; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (redirect === null || isString(redirect)) {
                            return [2 /*return*/, redirect];
                        }
                        return [4 /*yield*/, redirect(path, params, data)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Router.route404 = '@@router-404';
    return Router;
}());
export { Router };
export var router = new Router();
//# sourceMappingURL=store.js.map