import * as React from 'react';
import { Unsubscriber } from './store';
interface IState {
    component?: JSX.Element | null;
}
export declare class RouterOutlet extends React.PureComponent {
    state: IState;
    sub: Unsubscriber;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element | null;
}
export {};
