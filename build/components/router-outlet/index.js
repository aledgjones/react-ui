var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { router } from './store';
var RouterOutlet = /** @class */ (function (_super) {
    __extends(RouterOutlet, _super);
    function RouterOutlet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { component: router.component };
        return _this;
    }
    RouterOutlet.prototype.componentDidMount = function () {
        var _this = this;
        this.sub = router.subscribe(function () {
            _this.setState({ component: router.component });
        });
    };
    RouterOutlet.prototype.componentWillUnmount = function () {
        this.sub();
    };
    RouterOutlet.prototype.render = function () {
        return this.state.component || null;
    };
    return RouterOutlet;
}(React.PureComponent));
export { RouterOutlet };
//# sourceMappingURL=index.js.map