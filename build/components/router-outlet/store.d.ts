/// <reference types="react" />
export declare type Callback = () => void;
export declare type Unsubscriber = () => void;
export declare type Redirect = ((path: Path, params: Params, data: Data) => Promise<string | null>) | ((path: Path, params: Params, data: Data) => string | null) | string | null;
export declare type ComponentDef = ((path: Path, params: Params, data: Data) => JSX.Element) | JSX.Element;
declare type Path = string;
declare type Component = JSX.Element;
declare type Params = {
    [name: string]: string;
};
declare type Data = {
    [name: string]: string;
};
export interface IRouterConfig {
    [url: string]: {
        component?: ComponentDef;
        redirect?: Redirect;
    };
}
export interface IRouteConfig {
    path: string[];
    component?: ComponentDef;
    redirect?: Redirect;
}
export declare class Router {
    static route404: string;
    private _debug;
    private _subs;
    private _instructions;
    path: Path;
    component: Component;
    params: Params;
    data: Data;
    enableDebugging(): void;
    subscribe(callback: Callback): Unsubscriber;
    dispatch(): void;
    config(conf: IRouterConfig): void;
    /**
     * navigate to a given url
     */
    navigate(path: Path, data?: Data): Promise<void>;
    private urlToParts;
    private parseParams;
    private encodeData;
    decodeData(data: string): {
        [key: string]: string;
    };
    private match;
    private conformRedirect;
}
export declare const router: Router;
export {};
