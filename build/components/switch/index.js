// tslint:disable:no-any
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var Switch = /** @class */ (function (_super) {
    __extends(Switch, _super);
    function Switch() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onClick = function () { return _this.props.onChange && _this.props.onChange(!_this.props.value); };
        return _this;
    }
    Switch.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-switch', this.props.className, { 'ui-switch--active': this.props.value, 'ui-switch--disabled': this.props.disabled }), onClick: this.onClick },
            React.createElement("div", { className: "ui-switch__track" }),
            React.createElement("div", { className: "ui-switch__button" },
                React.createElement(Ink, { center: true }))));
    };
    return Switch;
}(React.PureComponent));
export { Switch };
//# sourceMappingURL=index.js.map