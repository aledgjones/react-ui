import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value: boolean;
    disabled?: boolean;
    onChange?: (val: boolean) => void;
}
export declare class Switch extends React.PureComponent<IProps> {
    onClick: () => void | undefined;
    render(): JSX.Element;
}
