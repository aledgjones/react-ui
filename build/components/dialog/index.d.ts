import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    visible: boolean;
    backdrop: boolean;
    onClick?: () => void;
}
export declare class Dialog extends React.PureComponent<IProps> {
    private generateContainer;
    private portal;
    private container;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): React.ReactPortal;
}
export {};
