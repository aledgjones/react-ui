var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Backdrop } from '../backdrop';
import './styles.css';
var Dialog = /** @class */ (function (_super) {
    __extends(Dialog, _super);
    function Dialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.portal = document.getElementById('portal');
        _this.container = _this.generateContainer();
        return _this;
    }
    Dialog.prototype.generateContainer = function () {
        var container = document.createElement('div');
        container.classList.add('ui-dialog');
        if (this.props.id) {
            container.id = this.props.id;
        }
        return container;
    };
    Dialog.prototype.componentDidMount = function () {
        if (this.portal) {
            this.portal.appendChild(this.container);
        }
    };
    Dialog.prototype.componentWillUnmount = function () {
        if (this.portal) {
            this.portal.removeChild(this.container);
        }
    };
    Dialog.prototype.render = function () {
        this.container.classList.toggle('ui-dialog--visible', this.props.visible);
        return ReactDOM.createPortal(React.createElement(React.Fragment, null,
            React.createElement(Backdrop, { visible: this.props.visible, onClick: this.props.onClick, transparent: !this.props.backdrop }),
            this.props.visible && this.props.children), this.container);
    };
    return Dialog;
}(React.PureComponent));
export { Dialog };
//# sourceMappingURL=index.js.map