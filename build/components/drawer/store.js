var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Fluxify } from "../../utils/fluxify";
// tslint:disable:no-any
// tslint:disable:align
var OPEN_DRAWER = '@drawer/open';
var CLOSE_DRAWER = '@drawer/close';
var Drawer = /** @class */ (function (_super) {
    __extends(Drawer, _super);
    function Drawer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Drawer.prototype.reducer = function (action, payload, state) {
        switch (action) {
            case OPEN_DRAWER:
                return state.indexOf(payload) === -1 ? state.concat([payload]) : state;
            case CLOSE_DRAWER:
                return state.filter(function (id) { return id !== payload; });
            default:
                return state;
        }
    };
    Drawer.prototype.show = function (id) {
        this.dispatch(OPEN_DRAWER, id);
    };
    Drawer.prototype.hide = function (id) {
        this.dispatch(CLOSE_DRAWER, id);
    };
    return Drawer;
}(Fluxify));
export var drawer = new Drawer([]);
//# sourceMappingURL=store.js.map