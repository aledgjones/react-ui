import * as React from 'react';
import { Unlistener } from '../../utils/fluxify';
import './styles.css';
interface IProps {
    id: string;
    className?: string;
    disabled?: boolean;
}
interface IState {
    open: boolean;
    transition: boolean;
    offset: number;
}
export declare class Drawer extends React.PureComponent<IProps, IState> {
    sub: Unlistener;
    startX: number;
    state: {
        open: boolean;
        offset: number;
        transition: boolean;
    };
    start: (e: any) => void;
    move: (e: any) => void;
    end: () => void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    readonly drawerStyles: {
        transform: string;
    } | {
        transform?: undefined;
    };
    readonly backdropStyles: {
        opacity: number;
    };
    render(): JSX.Element;
}
export {};
