import { Fluxify } from "../../utils/fluxify";
declare class Drawer extends Fluxify<string[]> {
    reducer(action: string, payload: any, state: string[]): any[];
    show(id: string): void;
    hide(id: string): void;
}
export declare const drawer: Drawer;
export {};
