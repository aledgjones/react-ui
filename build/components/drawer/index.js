var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import { drawer } from './store';
import './styles.css';
var Drawer = /** @class */ (function (_super) {
    __extends(Drawer, _super);
    function Drawer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { open: drawer.data.indexOf(_this.props.id) > -1, offset: 0, transition: true };
        _this.start = function (e) {
            if (!_this.props.disabled && e.pointerType === 'touch') {
                var left = e.screenX;
                if (!_this.state.open && left < 5) {
                    _this.setState({ transition: false });
                    _this.startX = left;
                    addEventListener('pointermove', _this.move);
                    addEventListener('pointerup', _this.end);
                }
            }
        };
        _this.move = function (e) {
            _this.setState({ offset: e.screenX - _this.startX });
        };
        _this.end = function () {
            var offset = _this.state.offset;
            _this.startX = 0;
            _this.setState({ offset: 0, transition: true });
            removeEventListener('pointermove', _this.move);
            removeEventListener('pointerup', _this.end);
            if (offset > 64) {
                drawer.show(_this.props.id);
            }
        };
        return _this;
    }
    Drawer.prototype.componentDidMount = function () {
        var _this = this;
        this.sub = drawer.listen(function (action, state) {
            _this.setState({ open: state.indexOf(_this.props.id) > -1 });
        });
        document.addEventListener('pointerdown', this.start);
    };
    Drawer.prototype.componentWillUnmount = function () {
        this.sub();
        document.removeEventListener('pointerdown', this.start);
    };
    Object.defineProperty(Drawer.prototype, "drawerStyles", {
        get: function () {
            if (this.state.offset > 360) {
                return {
                    transform: "translate3d(0px, 0, 0)"
                };
            }
            if (this.state.offset > 0) {
                return {
                    transform: "translate3d(" + (this.state.offset - 360) + "px, 0, 0)"
                };
            }
            return {};
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Drawer.prototype, "backdropStyles", {
        get: function () {
            if (this.state.offset > 360) {
                return {
                    opacity: 1
                };
            }
            if (this.state.offset > 0) {
                return {
                    opacity: this.state.offset / 360
                };
            }
            return {
                opacity: 0
            };
        },
        enumerable: true,
        configurable: true
    });
    Drawer.prototype.render = function () {
        var _this = this;
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { style: this.drawerStyles, className: mergeClasses(this.props.className, 'ui-drawer', { 'ui-drawer--open': this.state.open, 'ui-drawer--transition': this.state.transition }) }, this.props.children),
            React.createElement("div", { style: this.backdropStyles, className: mergeClasses('ui-drawer__backdrop', { 'ui-drawer__backdrop--visible': this.state.open, 'ui-drawer__backdrop--transition': this.state.transition }), onClick: function () { return drawer.hide(_this.props.id); } })));
    };
    return Drawer;
}(React.PureComponent));
export { Drawer };
//# sourceMappingURL=index.js.map