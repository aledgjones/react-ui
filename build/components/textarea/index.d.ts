import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value?: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange?: (value: string) => void;
    onEnter?: () => void;
    onFocus?: () => void;
    onBlur?: () => void;
}
interface IState {
    focus: boolean;
    touched: boolean;
    height: number;
}
export declare class Textarea extends React.PureComponent<IProps, IState> {
    private textarea;
    state: IState;
    onFocus: () => void;
    onBlur: () => void;
    onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void | undefined;
    onEnter: (e: React.KeyboardEvent<HTMLTextAreaElement>) => false | void | undefined;
    onClear: () => void | undefined;
    getError(): "Required" | null;
    componentDidUpdate(prev: IProps): void;
    render(): JSX.Element;
}
export {};
