var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { hasValue, mergeClasses } from '../../utils';
import './styles.css';
var Textarea = /** @class */ (function (_super) {
    __extends(Textarea, _super);
    function Textarea() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.textarea = React.createRef();
        _this.state = { focus: false, touched: false, height: 0 };
        _this.onFocus = function () {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.onBlur = function () {
            _this.setState({ focus: false, touched: true });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.onChange = function (e) { return _this.props.onChange && _this.props.onChange(e.target.value); };
        _this.onEnter = function (e) { return e.which === 13 && _this.props.onEnter && _this.props.onEnter(); };
        _this.onClear = function () { return _this.props.onChange && _this.props.onChange(''); };
        return _this;
    }
    Textarea.prototype.getError = function () {
        if (this.props.required && !hasValue(this.props.value)) {
            return 'Required';
        }
        return null;
    };
    Textarea.prototype.componentDidUpdate = function (prev) {
        if (this.props.value !== prev.value) {
            var split = this.props.value.split('\n');
            this.setState({
                height: this.textarea.current ? this.textarea.current.clientHeight + (split.length > 2 && !split[split.length - 1] ? 16 : 0) : 0
            });
        }
    };
    Textarea.prototype.render = function () {
        var error = this.getError();
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-textarea', this.props.className, {
                'ui-textarea--disabled': this.props.disabled,
                'ui-textarea--focus': this.state.focus,
                'ui-textarea--has-value': hasValue(this.props.value),
                'ui-textarea--invalid': !!error,
                'ui-textarea--touched': this.state.touched
            }), style: this.props.style },
            React.createElement("div", { className: "ui-textarea__wrapper" },
                React.createElement("p", { className: "ui-textarea__placeholder" },
                    React.createElement("span", null, this.props.placeholder),
                    !!this.props.required && React.createElement("span", null, "*")),
                React.createElement("textarea", { value: this.props.value, onFocus: this.onFocus, onBlur: this.onBlur, onChange: this.onChange, onKeyUp: this.onEnter, disabled: this.props.disabled, style: {
                        height: this.state.height
                    } }),
                React.createElement("div", { ref: this.textarea, className: "ui-textarea--slave" }, this.props.value),
                React.createElement("div", { className: "ui-textarea__bar" }),
                (this.state.touched && !!error) && React.createElement("div", { className: "ui-textarea__error" }, error))));
    };
    return Textarea;
}(React.PureComponent));
export { Textarea };
//# sourceMappingURL=index.js.map