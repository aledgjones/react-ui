import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    disabled?: boolean;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}
export declare class ListItem extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
