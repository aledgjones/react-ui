// tslint:disable:no-any
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var ListItem = /** @class */ (function (_super) {
    __extends(ListItem, _super);
    function ListItem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ListItem.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-list-item', this.props.className, { 'ui-list-item--disabled': this.props.disabled }), onClick: this.props.onClick },
            React.createElement(Ink, { cover: true }),
            React.createElement("div", { className: "ui-list-item__content" }, this.props.children)));
    };
    return ListItem;
}(React.PureComponent));
export { ListItem };
//# sourceMappingURL=index.js.map