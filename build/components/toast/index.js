var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Button } from '..';
import { toast } from './store';
import { mergeClasses } from '../../utils';
import './styles.css';
var Toast = /** @class */ (function (_super) {
    __extends(Toast, _super);
    function Toast() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { toasts: [] };
        _this.triggerOnClick = function (key) { return toast.triggerOnClick(key); };
        return _this;
    }
    Toast.prototype.componentWillMount = function () {
        this.setState({ toasts: toast.data });
    };
    Toast.prototype.componentDidMount = function () {
        var _this = this;
        this.sub = toast.listen(function (action, data) {
            _this.setState({ toasts: data });
        });
    };
    Toast.prototype.componentWillUnmount = function () {
        this.sub();
    };
    Toast.prototype.render = function () {
        var _this = this;
        return (React.createElement(React.Fragment, null, this.state.toasts.map(function (config) {
            return (React.createElement("div", { key: config.key, className: mergeClasses('ui-toast', { 'ui-toast--hide': config.hide }) },
                React.createElement("span", { className: "ui-toast__text" }, config.text),
                !!config.button && React.createElement(Button, { className: "ui-button--compact", onClick: function () { return _this.triggerOnClick(config.key); } }, config.button)));
        })));
    };
    return Toast;
}(React.PureComponent));
export { Toast };
//# sourceMappingURL=index.js.map