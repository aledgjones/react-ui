import { Fluxify } from "../../utils/fluxify";
export interface IToastConfig {
    text: string;
    button?: string;
    onClick?: () => void;
    onTimeout?: () => void;
}
export interface IToastInstance {
    key: number;
    text: string;
    button?: string;
    onClick?: () => void;
    onTimeout?: () => void;
    hide?: boolean;
    timeout: any;
}
export declare class Toast extends Fluxify<IToastInstance[]> {
    reducer(action: string, payload: any, state: IToastInstance[]): any[];
    show(config: IToastConfig, duration?: number): void;
    triggerOnClick(key: number): void;
    private getInstanceFromKey;
    private triggerOnTimeout;
    private hide;
    private destroy;
    private cleanup;
}
export declare const toast: Toast;
