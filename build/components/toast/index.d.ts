import * as React from 'react';
import { Unlistener } from '../../utils/fluxify';
import { IToastInstance } from './store';
import './styles.css';
interface IState {
    toasts: IToastInstance[];
}
export declare class Toast extends React.PureComponent {
    sub: Unlistener;
    state: IState;
    componentWillMount(): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    triggerOnClick: (key: number) => void;
    render(): JSX.Element;
}
export {};
