var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Fluxify } from "../../utils/fluxify";
var ADD_TOAST = '@toast/add';
var HIDE_TOAST = '@toast/hide';
var DESTROY_TOAST = '@toast/destroy';
var Toast = /** @class */ (function (_super) {
    __extends(Toast, _super);
    function Toast() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Toast.prototype.reducer = function (action, payload, state) {
        switch (action) {
            case ADD_TOAST:
                return state.concat([payload]);
            case HIDE_TOAST:
                return state.map(function (instance) {
                    if (instance.key === payload) {
                        clearTimeout(instance.timeout);
                        instance.hide = true;
                        return instance;
                    }
                    else {
                        return instance;
                    }
                });
            case DESTROY_TOAST:
                return state.filter(function (instance) { return instance.key !== payload; });
            default:
                return state;
        }
    };
    Toast.prototype.show = function (config, duration) {
        var _this = this;
        if (duration === void 0) { duration = 5000; }
        this.cleanup();
        var key = Date.now();
        var instance = __assign({ key: key }, config, { hide: false, timeout: setTimeout(function () {
                _this.triggerOnTimeout(key);
            }, duration) });
        this.dispatch(ADD_TOAST, instance);
    };
    Toast.prototype.triggerOnClick = function (key) {
        var instance = this.getInstanceFromKey(key);
        if (instance && instance.onClick) {
            instance.onClick();
        }
        this.hide(key);
    };
    Toast.prototype.getInstanceFromKey = function (key) {
        return this.data.find(function (instance) { return instance.key === key; });
    };
    Toast.prototype.triggerOnTimeout = function (key) {
        var instance = this.getInstanceFromKey(key);
        if (instance && instance.onTimeout) {
            instance.onTimeout();
        }
        this.hide(key);
    };
    Toast.prototype.hide = function (key) {
        var _this = this;
        this.dispatch(HIDE_TOAST, key);
        setTimeout(function () {
            _this.destroy(key);
        }, 1000);
    };
    Toast.prototype.destroy = function (key) {
        this.dispatch(DESTROY_TOAST, key);
    };
    Toast.prototype.cleanup = function () {
        var _this = this;
        this.data.filter(function (instance) { return !instance.hide; })
            .forEach(function (instance) {
            _this.triggerOnTimeout(instance.key);
        });
    };
    return Toast;
}(Fluxify));
export { Toast };
export var toast = new Toast([]);
//# sourceMappingURL=store.js.map