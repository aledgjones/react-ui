import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
}
export declare class Content extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
