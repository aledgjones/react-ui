import * as React from 'react';
import { IContact } from '../contact-picker';
import { Tab } from '../contact-picker-dialog';
import './styles.css';
interface IState {
    search: string;
}
interface IProps {
    contacts: IContact[];
    onCancel: () => void;
    onSelect: (value: IContact) => void;
    onNavigate: (tab: Tab) => void;
}
export declare class ContactPickerDialogList extends React.PureComponent<IProps> {
    state: IState;
    private clearSearch;
    private updateSearch;
    private createContact;
    render(): JSX.Element;
}
export {};
