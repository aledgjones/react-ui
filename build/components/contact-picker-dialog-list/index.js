var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mdiAccountPlus, mdiMagnify, mdiClose } from '@mdi/js';
import { Content } from '../content';
import { Button } from '../button';
import { Icon } from '../icon';
import { List } from '../list';
import { ListItem } from '../list-item';
import { Label } from '../label';
import { Avatar } from '../avatar';
import { IconButton } from '../icon-button';
import { Toolbar } from '../toolbar';
import { ToolbarLine } from '../toolbar-line';
import { hasValue } from '../../utils';
import { Divider } from '../divider';
import { Tab } from '../contact-picker-dialog';
import './styles.css';
var ContactPickerDialogList = /** @class */ (function (_super) {
    __extends(ContactPickerDialogList, _super);
    function ContactPickerDialogList() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { search: '' };
        _this.clearSearch = function () { return _this.setState({ search: '' }); };
        _this.updateSearch = function (e) { return _this.setState({ search: e.target.value }); };
        _this.createContact = function () { return _this.props.onNavigate(Tab.CREATE); };
        return _this;
    }
    ContactPickerDialogList.prototype.render = function () {
        var _this = this;
        var term = this.state.search.toLocaleUpperCase();
        var contacts = hasValue(this.state.search) ? this.props.contacts.filter(function (contact) { return contact.name.toLocaleUpperCase().indexOf(term) > -1; }) : this.props.contacts;
        return React.createElement(React.Fragment, null,
            React.createElement(Toolbar, null,
                React.createElement(ToolbarLine, null,
                    React.createElement(Icon, { color: "rgb(150,150,150)", path: mdiMagnify }),
                    React.createElement("input", { autoFocus: true, className: "ui-contact-picker-dialog-list__search", type: "text", value: this.state.search, onChange: this.updateSearch }),
                    !hasValue(this.state.search) && React.createElement(IconButton, { className: "ui-contact-picker-dialog-list__clear", onClick: this.createContact, path: mdiAccountPlus }),
                    hasValue(this.state.search) && React.createElement(IconButton, { color: "rgb(150,150,150)", className: "ui-contact-picker-dialog-list__clear", onClick: this.clearSearch, path: mdiClose }))),
            React.createElement(Divider, { className: "ui-divider--no-margin" }),
            React.createElement(List, { className: "ui-contact-picker-dialog-list__content" }, contacts.map(function (contact) {
                return React.createElement(ListItem, { key: contact.key, onClick: function () { return _this.props.onSelect(contact); } },
                    React.createElement(Avatar, { name: contact.name, src: contact.avatar }),
                    React.createElement(Label, null,
                        React.createElement("p", null, contact.name),
                        contact.email && React.createElement("p", null, contact.email)));
            })),
            React.createElement(Divider, { className: "ui-divider--no-margin" }),
            React.createElement(Content, { className: "ui-contact-picker-dialog-list__buttons ui-content--buttons ui-content--compact" },
                React.createElement(Button, { className: "ui-button--compact", onClick: this.props.onCancel }, "Cancel")));
    };
    return ContactPickerDialogList;
}(React.PureComponent));
export { ContactPickerDialogList };
//# sourceMappingURL=index.js.map