import * as React from 'react';
import { Tab } from '../contact-picker-dialog';
import './styles.css';
import { IContactCreator } from '../contact-picker';
interface IState {
    working: boolean;
    name: string;
    email: string;
    address: string;
    avatar?: string;
    _avatar?: File;
}
interface IProps {
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onNavigate: (tab: Tab) => void;
}
export declare class ContactPickerDialogCreate extends React.PureComponent<IProps> {
    state: IState;
    private updateName;
    private updateEmail;
    private updateAddress;
    private toList;
    private selectImage;
    componentWillUnmount(): void;
    private create;
    render(): JSX.Element;
}
export {};
