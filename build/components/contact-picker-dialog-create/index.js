var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import * as React from 'react';
import { Content } from '../content';
import { Button } from '../button';
import { Tab } from '../contact-picker-dialog';
import { Input } from '../input';
import { Title } from '../title';
import { Avatar } from '../avatar';
import { Label } from '../label';
import { chooseFiles, hasValue } from '../../utils';
import { toast } from '../toast/store';
import './styles.css';
import { Textarea } from '../textarea';
var ContactPickerDialogCreate = /** @class */ (function (_super) {
    __extends(ContactPickerDialogCreate, _super);
    function ContactPickerDialogCreate() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { working: false, name: '', email: '', address: '' };
        _this.updateName = function (val) { return _this.setState({ name: val }); };
        _this.updateEmail = function (val) { return _this.setState({ email: val }); };
        _this.updateAddress = function (val) { return _this.setState({ address: val }); };
        _this.toList = function () { return _this.props.onNavigate(Tab.LIST); };
        _this.selectImage = function () { return __awaiter(_this, void 0, void 0, function () {
            var files;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, chooseFiles({ accept: 'image/*', multiple: false })];
                    case 1:
                        files = _a.sent();
                        if (this.state.avatar) {
                            URL.revokeObjectURL(this.state.avatar);
                        }
                        if (files.length > 0) {
                            this.setState({ avatar: URL.createObjectURL(files[0]), _avatar: files[0] });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        _this.create = function () { return __awaiter(_this, void 0, void 0, function () {
            var contact, url, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!hasValue(this.state.name)) return [3 /*break*/, 6];
                        this.setState({ working: true });
                        contact = {
                            name: this.state.name,
                        };
                        if (this.state.email) {
                            contact.email = this.state.email;
                        }
                        if (this.state.address) {
                            contact.address = this.state.address;
                        }
                        if (!this.state._avatar) return [3 /*break*/, 4];
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.props.onUpload(this.state._avatar)];
                    case 2:
                        url = _b.sent();
                        contact.avatar = url;
                        return [3 /*break*/, 4];
                    case 3:
                        _a = _b.sent();
                        toast.show({ text: 'Something went wrong' });
                        this.setState({ working: false });
                        return [2 /*return*/, false];
                    case 4: return [4 /*yield*/, this.props.onCreate(contact)];
                    case 5:
                        _b.sent();
                        this.props.onNavigate(Tab.LIST);
                        return [2 /*return*/, true];
                    case 6:
                        toast.show({ text: 'Must have name.' });
                        return [2 /*return*/, false];
                }
            });
        }); };
        return _this;
    }
    ContactPickerDialogCreate.prototype.componentWillUnmount = function () {
        if (this.state.avatar) {
            URL.revokeObjectURL(this.state.avatar);
        }
    };
    ContactPickerDialogCreate.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement(Content, { className: "ui-contact-picker-dialog-create" },
                React.createElement(Title, null, "New Contact"),
                React.createElement("div", { className: "ui-contact-picker-dialog-create__avatar-container" },
                    React.createElement("div", { className: "ui-contact-picker-dialog-create__avatar", onClick: this.selectImage },
                        React.createElement(Avatar, { size: 64, name: this.state.name || 'A', src: this.state.avatar })),
                    React.createElement(Label, { className: "ui-contact-picker-dialog-create__label" },
                        React.createElement("p", null, this.state.name || 'New contact'),
                        React.createElement("p", null, this.state.email || 'contact@example.com'))),
                React.createElement(Input, { type: "text", required: true, placeholder: "Name", value: this.state.name, onChange: this.updateName }),
                React.createElement(Input, { type: "email", placeholder: "Email", value: this.state.email, onChange: this.updateEmail }),
                React.createElement(Textarea, { placeholder: "Address", value: this.state.address, onChange: this.updateAddress })),
            React.createElement(Content, { className: "ui-contact-picker-dialog-create__buttons ui-content--buttons ui-content--compact" },
                React.createElement(Button, { disabled: this.state.working, className: "ui-button--compact", onClick: this.toList }, "Cancel"),
                React.createElement(Button, { disabled: this.state.working, working: this.state.working, className: "ui-button--compact", onClick: this.create }, "Create")));
    };
    return ContactPickerDialogCreate;
}(React.PureComponent));
export { ContactPickerDialogCreate };
//# sourceMappingURL=index.js.map