import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    working?: boolean;
    href?: string;
}
export declare class Button extends React.PureComponent<IProps> {
    private button;
    render(): JSX.Element;
}
export {};
