var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { CircularProgress, Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Button.prototype.button = function () {
        return React.createElement("button", { id: this.props.id, className: mergeClasses('ui-button', this.props.className, { 'ui-button--disabled': this.props.disabled }), onClick: this.props.onClick, disabled: this.props.disabled },
            React.createElement(Ink, { cover: true }),
            React.createElement("div", { className: "ui-button__content" },
                this.props.children,
                !!this.props.working && React.createElement(CircularProgress, { className: "ui-circular-progress--tiny" })));
    };
    Button.prototype.render = function () {
        if (this.props.href) {
            return React.createElement("a", { href: this.props.href, target: "_blank" }, this.button());
        }
        else {
            return this.button();
        }
    };
    return Button;
}(React.PureComponent));
export { Button };
//# sourceMappingURL=index.js.map