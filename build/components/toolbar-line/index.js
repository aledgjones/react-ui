// tslint:disable:no-any
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var ToolbarLine = /** @class */ (function (_super) {
    __extends(ToolbarLine, _super);
    function ToolbarLine() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ToolbarLine.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-toolbar__line', this.props.className), onClick: this.props.onClick }, this.props.children));
    };
    return ToolbarLine;
}(React.PureComponent));
export { ToolbarLine };
//# sourceMappingURL=index.js.map