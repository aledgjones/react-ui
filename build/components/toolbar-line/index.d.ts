import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    onClick?: () => void;
}
export declare class ToolbarLine extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
