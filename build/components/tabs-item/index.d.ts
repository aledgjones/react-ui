import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    value: string;
}
export declare class TabsItem extends React.PureComponent<IProps> {
    render: () => null;
}
interface IPropsComponent {
    id?: string;
    className?: string;
    onClick: (value: string, e: HTMLDivElement | null) => void;
    onInit: (e: HTMLDivElement | null) => void;
    selected: boolean;
    value: string;
    content: any;
}
export declare class TabsItemComponent extends React.PureComponent<IPropsComponent> {
    private element;
    componentDidMount(): void;
    private onClick;
    render(): JSX.Element;
}
export {};
