var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var TabsItem = /** @class */ (function (_super) {
    __extends(TabsItem, _super);
    function TabsItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.render = function () { return null; };
        return _this;
    }
    return TabsItem;
}(React.PureComponent));
export { TabsItem };
var TabsItemComponent = /** @class */ (function (_super) {
    __extends(TabsItemComponent, _super);
    function TabsItemComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.element = React.createRef();
        _this.onClick = function (e) {
            _this.props.onClick(_this.props.value, _this.element.current);
        };
        return _this;
    }
    TabsItemComponent.prototype.componentDidMount = function () {
        if (this.props.selected) {
            this.props.onInit(this.element.current);
        }
    };
    TabsItemComponent.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-tabs-item', this.props.className, { 'ui-tabs-item--selected': this.props.selected }), onClick: this.onClick, ref: this.element },
            React.createElement(Ink, { cover: true }),
            React.createElement("div", { className: "ui-tabs-item__content" }, this.props.content)));
    };
    return TabsItemComponent;
}(React.PureComponent));
export { TabsItemComponent };
//# sourceMappingURL=index.js.map