import * as React from 'react';
import './styles.css';
export interface IColor {
    r: number;
    g: number;
    b: number;
    a: number;
}
export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value?: string;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value?: string) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}
export interface IState {
    focus: boolean;
    touched: boolean;
}
export declare class ColorPicker extends React.PureComponent<IProps, IState> {
    state: IState;
    private hasError;
    private showDialog;
    private hideDialog;
    private clear;
    private updateValue;
    private colorString;
    render(): JSX.Element;
}
