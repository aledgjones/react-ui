var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mdiClose, mdiEyedropper } from '@mdi/js';
import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { ColorPickerDialog } from '../color-picker-dialog';
import './styles.css';
var ColorPicker = /** @class */ (function (_super) {
    __extends(ColorPicker, _super);
    function ColorPicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            focus: false,
            touched: false
        };
        _this.showDialog = function () {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.hideDialog = function () {
            _this.setState({ focus: false });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.clear = function () {
            _this.props.onChange();
        };
        _this.updateValue = function (color) {
            _this.props.onChange(color);
            _this.hideDialog();
        };
        return _this;
    }
    ColorPicker.prototype.hasError = function () {
        return this.props.required && !hasValue(this.props.value);
    };
    ColorPicker.prototype.colorString = function (css) {
        if (css === void 0) { css = false; }
        if (this.props.value) {
            return this.props.value;
        }
        else {
            return css ? 'white' : '';
        }
    };
    ColorPicker.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement("div", { id: this.props.id, style: this.props.style, className: mergeClasses('ui-color-picker', this.props.className, {
                    'ui-color-picker--disabled': this.props.disabled,
                    'ui-color-picker--focus': this.state.focus,
                    'ui-color-picker--has-value': hasValue(this.props.value),
                    'ui-color-picker--invalid': this.hasError(),
                    'ui-color-picker--touched': this.state.touched
                }) },
                React.createElement("div", { className: "ui-color-picker__preview-swatch" },
                    React.createElement("div", { className: "ui-color-picker__hatching" }),
                    React.createElement("div", { className: "ui-color-picker__fill", style: { backgroundColor: this.colorString(true) } })),
                React.createElement("div", { className: "ui-color-picker__value", onClick: this.showDialog }, this.props.placeholder + (this.props.required ? '*' : '')),
                (hasValue(this.props.value) && !this.props.required) ?
                    React.createElement(IconButton, { className: "ui-color-picker__clear-icon", onClick: this.clear, path: mdiClose, color: "rgb(150,150,150)" }) :
                    React.createElement(IconButton, { className: "ui-color-picker__down-icon", onClick: this.showDialog, path: mdiEyedropper, color: "rgb(150,150,150)" }),
                React.createElement("div", { className: "ui-color-picker__bar" })),
            React.createElement(Dialog, { visible: !!this.state.focus, backdrop: true },
                React.createElement(ColorPickerDialog, { value: this.props.value, onCancel: this.hideDialog, onComplete: this.updateValue })));
    };
    return ColorPicker;
}(React.PureComponent));
export { ColorPicker };
//# sourceMappingURL=index.js.map