import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    value: number;
    max: number;
    color?: string;
    disabled?: boolean;
    onChange: (val: number) => void;
}
interface IState {
    active: boolean;
}
export declare class Slider extends React.PureComponent<IProps, IState> {
    state: IState;
    setValue: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    private wheel;
    onPointerDown: (e: React.PointerEvent<any>) => void;
    render(): JSX.Element;
}
export {};
