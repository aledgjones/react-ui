var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Ink } from '..';
import { mergeClasses, pointerMoveGenerator } from '../../utils';
import './styles.css';
var Slider = /** @class */ (function (_super) {
    __extends(Slider, _super);
    function Slider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { active: false };
        _this.setValue = function (e) {
            var box = e.currentTarget.getBoundingClientRect();
            var left = e.clientX - box.left;
            left = left < 0 ? 0 : left;
            left = left > box.width ? box.width : left;
            var value = Math.round((left / box.width) * _this.props.max);
            return _this.props.onChange(value);
        };
        _this.wheel = function (e) {
            if (e.deltaY > 0 && _this.props.value < _this.props.max) {
                return _this.props.onChange(_this.props.value + 1);
            }
            if (e.deltaY < 0 && _this.props.value > 0) {
                return _this.props.onChange(_this.props.value - 1);
            }
        };
        _this.onPointerDown = pointerMoveGenerator(function (e, data) {
            var parent = e.currentTarget.closest('.ui-slider');
            if (parent) {
                _this.setState({ active: true });
                var box = parent.getBoundingClientRect();
                data.left = box.left;
                data.width = box.width;
                return true;
            }
            else {
                return false;
            }
        }, function (event, data) {
            var left = event.clientX - data.left;
            left = left < 0 ? 0 : left;
            left = left > data.width ? data.width : left;
            var value = Math.round((left / data.width) * _this.props.max);
            // only fire if the value has actually changed
            if (value !== _this.props.value) {
                _this.props.onChange(value);
            }
        }, function () { return _this.setState({ active: false }); });
        return _this;
    }
    Slider.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-slider', this.props.className, {
                'ui-slider--disabled': this.props.disabled,
                'ui-slider--is-zero': this.props.value === 0,
                'ui-slider--active': this.state.active
            }), onClick: this.setValue, onWheel: this.wheel },
            React.createElement("div", { className: "ui-slider__track", style: {
                    left: (this.props.value / this.props.max) * 100 + "%",
                    width: ((this.props.max - this.props.value) / this.props.max) * 100 + "%"
                } }),
            React.createElement("div", { className: "ui-slider__value", style: {
                    width: (this.props.value / this.props.max) * 100 + "%",
                    backgroundColor: this.props.color
                } }),
            React.createElement("div", { className: "ui-slider__button", style: {
                    left: (this.props.value / this.props.max) * 100 + "%",
                    backgroundColor: this.props.color
                }, onPointerDown: this.onPointerDown },
                React.createElement(Ink, { center: true }))));
    };
    return Slider;
}(React.PureComponent));
export { Slider };
//# sourceMappingURL=index.js.map