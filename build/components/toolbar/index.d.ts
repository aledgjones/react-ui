import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
}
export declare class Toolbar extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
