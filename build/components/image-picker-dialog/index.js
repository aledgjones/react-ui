var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import React from 'react';
import pretty from 'pretty-bytes';
import { Toolbar } from '../toolbar';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { ToolbarLine } from '../toolbar-line';
import { Icon } from '../icon';
import { Button } from '../button';
import { Card } from '../card';
import { chooseFiles } from '../../utils/choose-files';
import { Content } from '../content';
import { Image } from '../image';
import { Label } from '../label';
import { LinearProgress } from '../linear-progress';
import { toast } from '../toast/store';
import { Subheader } from '../subheader';
import { pluralize } from '../../utils/pluralize';
import { mergeClasses } from '../../utils/merge-classes';
import { ImageViewer } from '../image-viewer';
import { List } from '../list';
import { ListItem } from '../list-item';
import { copy } from '../../utils/copy';
import { mdiFolderMultipleImage, mdiCloudUploadOutline, mdiContentCopy, mdiMagnifyPlusOutline, mdiClose } from '@mdi/js';
import './styles.css';
var ImagePickerDialog = /** @class */ (function (_super) {
    __extends(ImagePickerDialog, _super);
    function ImagePickerDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { working: false, upload: null, preview: null };
        _this.chooseFile = function () { return __awaiter(_this, void 0, void 0, function () {
            var file, files, e_1, src_1, name_1, size_1, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({ working: true });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, chooseFiles({ accept: 'image/*', multiple: false })];
                    case 2:
                        files = _a.sent();
                        file = files[0];
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        toast.show({ text: 'Something went wrong' });
                        return [3 /*break*/, 4];
                    case 4:
                        if (!file) return [3 /*break*/, 9];
                        src_1 = URL.createObjectURL(file);
                        name_1 = file.name;
                        size_1 = file.size;
                        this.setState({ upload: { src: src_1, name: name_1, size: size_1, progress: 0 } });
                        _a.label = 5;
                    case 5:
                        _a.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, this.props.onUpload(file, function (progress) { return _this.setState({ upload: { src: src_1, name: name_1, size: size_1, progress: progress } }); })];
                    case 6:
                        _a.sent();
                        return [3 /*break*/, 8];
                    case 7:
                        err_1 = _a.sent();
                        toast.show({ text: err_1 });
                        return [3 /*break*/, 8];
                    case 8:
                        ;
                        URL.revokeObjectURL(src_1);
                        _a.label = 9;
                    case 9:
                        this.setState({ upload: null, working: false });
                        return [2 /*return*/];
                }
            });
        }); };
        _this.copyURL = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this.state.preview) {
                    try {
                        copy(this.state.preview.src);
                        toast.show({ text: 'Copied to clipboard' }, 2000);
                    }
                    catch (err) {
                        toast.show({ text: 'Sorry, could not copy to clipboard' });
                    }
                }
                return [2 /*return*/];
            });
        }); };
        _this.closePreview = function () { return _this.setState({ preview: null }); };
        return _this;
    }
    ImagePickerDialog.prototype.emptyState = function () {
        return React.createElement("div", { className: "ui-image-picker-dialog__content ui-image-picker-dialog__content--empty" },
            React.createElement(Icon, { size: 48, color: "rgba(0, 0, 0, 0.3)", path: mdiFolderMultipleImage }),
            React.createElement("p", null, "No images yet..."));
    };
    ImagePickerDialog.prototype.uploads = function (upload) {
        return React.createElement(Content, { className: "ui-image-picker-dialog__uploads" },
            React.createElement("div", { key: upload.src, className: "ui-image-picker-dialog__upload" },
                React.createElement(Image, { className: "ui-image-picker-dialog__upload-image", src: upload.src }),
                React.createElement(Label, { className: "ui-image-picker-dialog__upload-label" },
                    React.createElement("p", null, upload.name),
                    React.createElement(LinearProgress, { progress: Math.round((upload.progress / upload.size) * 100), className: "ui-image-picker-dialog__upload-progress" }),
                    React.createElement("p", null,
                        pretty(upload.progress),
                        " / ",
                        pretty(upload.size)))));
    };
    ImagePickerDialog.prototype.images = function (images) {
        var _this = this;
        return React.createElement("div", { className: mergeClasses("ui-image-picker-dialog__content", { "ui-image-picker-dialog__content--working": this.state.upload !== null }) },
            React.createElement(Subheader, { className: "ui-subheader--margin" }, pluralize(images.length, 'image', 'images')),
            React.createElement("div", { className: "ui-image-picker-dialog__images" }, images.map(function (src) {
                return React.createElement("div", { className: "ui-image-picker-dialog__image", key: src },
                    React.createElement(Image, { onClick: function () { return _this.props.onComplete(src); }, className: "ui-image-picker-dialog__canvas", src: src }),
                    React.createElement("div", { className: "ui-image-picker-dialog__image-scrim" }),
                    React.createElement(IconButton, { onClick: function () { return _this.setState({ preview: { src: src } }); }, className: "ui-image-picker-dialog__image-expand", path: mdiMagnifyPlusOutline, color: "white" }));
            })));
    };
    ImagePickerDialog.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement(Card, { className: "ui-dialog__card ui-image-picker-dialog__card" },
                React.createElement(Toolbar, null,
                    React.createElement(ToolbarLine, null,
                        React.createElement(IconButton, { disabled: this.state.working, onClick: this.props.onCancel, className: "ui-icon-button--margin", path: mdiClose }),
                        React.createElement(Title, null, "Select Image"),
                        React.createElement(Button, { disabled: this.state.working, working: this.state.working, onClick: this.chooseFile },
                            React.createElement(Icon, { path: mdiCloudUploadOutline }),
                            React.createElement("span", null, "Upload")))),
                this.state.upload && this.uploads(this.state.upload),
                this.props.images.length === 0 && this.emptyState(),
                this.props.images.length > 0 && this.images(this.props.images)),
            this.state.preview && React.createElement(ImageViewer, { croppable: false, src: this.state.preview.src, onClose: this.closePreview },
                React.createElement(List, null,
                    React.createElement(ListItem, { onClick: this.copyURL },
                        React.createElement(Label, { className: "ui-image-picker-dialog__label" },
                            React.createElement("p", null, "Download URL"),
                            React.createElement("p", { className: "ui-label--clipped" }, this.state.preview.src)),
                        React.createElement(Icon, { path: mdiContentCopy })))));
    };
    return ImagePickerDialog;
}(React.PureComponent));
export { ImagePickerDialog };
//# sourceMappingURL=index.js.map