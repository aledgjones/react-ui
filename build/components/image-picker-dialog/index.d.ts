import React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    required?: boolean;
    images: string[];
    onCancel: () => void;
    onUpload: (file: File, update: (progress: number) => void) => Promise<void>;
    onComplete: (value: string) => void;
}
interface IUpload {
    src: string;
    name: string;
    size: number;
    progress: number;
}
interface IState {
    working: boolean;
    upload: IUpload | null;
    preview: {
        src: string;
    } | null;
}
export declare class ImagePickerDialog extends React.PureComponent<IProps, IState> {
    state: IState;
    private emptyState;
    private chooseFile;
    private copyURL;
    private uploads;
    private images;
    private closePreview;
    render(): JSX.Element;
}
export {};
