var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { Card, Content, Button } from '..';
import { Slider } from '../slider';
import './styles.css';
var ColorPickerDialog = /** @class */ (function (_super) {
    __extends(ColorPickerDialog, _super);
    function ColorPickerDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = __assign({}, _this.componentsFromString(_this.props.value), { recents: JSON.parse(localStorage.getItem('ui:colors') || '[]') });
        _this.updateValue = function () {
            var value = { r: _this.state.r, g: _this.state.g, b: _this.state.b, a: _this.state.a };
            _this.addRecent(value);
            _this.props.onComplete(_this.colorString(value));
        };
        _this.updateR = function (val) { return _this.setState({ r: val }); };
        _this.updateG = function (val) { return _this.setState({ g: val }); };
        _this.updateB = function (val) { return _this.setState({ b: val }); };
        _this.updateA = function (val) { return _this.setState({ a: val }); };
        _this.updateAll = function (color) { return _this.setState(__assign({}, color)); };
        return _this;
    }
    ColorPickerDialog.prototype.componentsFromString = function (value) {
        if (value) {
            var matches = value.match(/[.?\d]+/g);
            if (matches) {
                return { r: parseInt(matches[0]), g: parseInt(matches[1]), b: parseInt(matches[2]), a: parseFloat(matches[3]) * 100 };
            }
        }
        return { r: 0, g: 0, b: 0, a: 100 };
    };
    ColorPickerDialog.prototype.addRecent = function (value) {
        var _this = this;
        var updated = this.state.recents.filter(function (color) { return _this.colorString(color) !== _this.colorString(value); });
        updated.unshift(value);
        var clipped = updated.slice(0, 6);
        localStorage.setItem('ui:colors', JSON.stringify(clipped));
    };
    ColorPickerDialog.prototype.colorString = function (value) {
        return "rgba(" + value.r + ", " + value.g + ", " + value.b + ", " + (value.a / 100).toFixed(2) + ")";
    };
    ColorPickerDialog.prototype.render = function () {
        var _this = this;
        var color = this.colorString({ r: this.state.r, g: this.state.g, b: this.state.b, a: this.state.a });
        return React.createElement(Card, { className: "ui-dialog__card ui-color-picker-dialog__card" },
            React.createElement(Content, null,
                React.createElement("div", { className: "ui-color-picker-dialog__preview" },
                    React.createElement("div", { className: "ui-color-picker-dialog__hatching" }),
                    React.createElement("div", { className: "ui-color-picker-dialog__fill", style: { backgroundColor: color } }))),
            React.createElement(Content, { className: "ui-color-picker-dialog__values" },
                React.createElement("div", { className: "ui-color-picker-dialog__value-container" },
                    React.createElement("span", { className: "ui-color-picker-dialog__value-label" }, "R"),
                    React.createElement(Slider, { max: 255, className: "ui-slider--dark", value: this.state.r, onChange: this.updateR }),
                    React.createElement("span", { className: "ui-color-picker-dialog__value" }, this.state.r)),
                React.createElement("div", { className: "ui-color-picker-dialog__value-container" },
                    React.createElement("span", { className: "ui-color-picker-dialog__value-label" }, "G"),
                    React.createElement(Slider, { max: 255, className: "ui-slider--dark", value: this.state.g, onChange: this.updateG }),
                    React.createElement("span", { className: "ui-color-picker-dialog__value" }, this.state.g)),
                React.createElement("div", { className: "ui-color-picker-dialog__value-container" },
                    React.createElement("span", { className: "ui-color-picker-dialog__value-label" }, "B"),
                    React.createElement(Slider, { max: 255, className: "ui-slider--dark", value: this.state.b, onChange: this.updateB }),
                    React.createElement("span", { className: "ui-color-picker-dialog__value" }, this.state.b)),
                React.createElement("div", { className: "ui-color-picker-dialog__value-container" },
                    React.createElement("span", { className: "ui-color-picker-dialog__value-label" }, "A"),
                    React.createElement(Slider, { color: "grey", max: 100, className: "ui-slider--dark", value: this.state.a, onChange: this.updateA }),
                    React.createElement("span", { className: "ui-color-picker-dialog__value" }, (this.state.a / 100).toFixed(2)))),
            this.state.recents.length > 0 && React.createElement(Content, { className: "ui-color-picker-dialog__recents" },
                React.createElement("div", { className: "ui-color-picker-dialog__recents-grid" }, this.state.recents.map(function (color) {
                    var str = _this.colorString(color);
                    return React.createElement("div", { key: str, className: "ui-color-picker-dialog__swatch", onClick: function () { return _this.updateAll(color); } },
                        React.createElement("div", { className: "ui-color-picker-dialog__hatching" }),
                        React.createElement("div", { className: "ui-color-picker-dialog__fill", style: { backgroundColor: str } }));
                }))),
            React.createElement(Content, { className: "ui-content--buttons ui-content--compact" },
                React.createElement(Button, { className: "ui-button--compact", onClick: this.props.onCancel }, "Cancel"),
                React.createElement(Button, { className: "ui-button--compact", onClick: this.updateValue }, "Select")));
    };
    return ColorPickerDialog;
}(React.PureComponent));
export { ColorPickerDialog };
//# sourceMappingURL=index.js.map