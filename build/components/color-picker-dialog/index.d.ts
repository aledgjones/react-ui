import * as React from 'react';
import { IColor } from '../color-picker';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value?: string;
    required?: boolean;
    onCancel: () => void;
    onComplete: (value: string) => void;
}
export interface IState {
    r: number;
    g: number;
    b: number;
    a: number;
    recents: IColor[];
}
export declare class ColorPickerDialog extends React.PureComponent<IProps, IState> {
    state: IState;
    private componentsFromString;
    private addRecent;
    private updateValue;
    private colorString;
    private updateR;
    private updateG;
    private updateB;
    private updateA;
    private updateAll;
    render(): JSX.Element;
}
