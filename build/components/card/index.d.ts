import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}
export declare class Card extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
