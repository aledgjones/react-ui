// tslint:disable:align
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var Ink = /** @class */ (function (_super) {
    __extends(Ink, _super);
    function Ink() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.timeouts = [];
        _this.state = { blobs: [] };
        _this.create = function (e) {
            var key = Date.now();
            var rec = e.currentTarget.getBoundingClientRect();
            var blobs = _this.state.blobs.concat([
                {
                    diameter: _this.calcDiameter(rec.width, rec.height, e.clientX - rec.left, e.clientY - rec.top),
                    key: key,
                    x: _this.props.center ? rec.width / 2 : e.clientX - rec.left,
                    y: _this.props.center ? rec.height / 2 : e.clientY - rec.top,
                }
            ]);
            _this.setState({ blobs: blobs });
            _this.catchFire(key);
        };
        return _this;
    }
    Ink.prototype.componentWillUnmount = function () {
        this.state.blobs.forEach(function (blob) {
            clearTimeout(blob.timeout);
        });
    };
    Ink.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-ink', this.props.className), onPointerDown: this.create }, this.state.blobs.map(function (blob) {
            return (React.createElement("div", { key: blob.key, className: mergeClasses('ui-ink__blob', { 'ui-ink__blob--fire': blob.fire }), style: { height: blob.diameter, width: blob.diameter, top: blob.y, left: blob.x } }));
        })));
    };
    Ink.prototype.calcDiameter = function (width, height, x, y) {
        if (this.props.cover) {
            if (this.props.center) {
                return Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)) / 2;
            }
            else {
                var corners = [
                    { a: x, b: y },
                    { a: width - x, b: y },
                    { a: width - x, b: height - y },
                    { a: x, b: height - y }
                ];
                var radius = corners.reduce(function (max, corner) {
                    var distance = Math.sqrt(Math.pow(corner.a, 2) + Math.pow(corner.b, 2));
                    return distance > max ? distance : max;
                }, 0);
                return radius;
            }
        }
        else {
            return (this.props.diameter || 48) / 2;
        }
    };
    Ink.prototype.destroy = function (key) {
        var blobs = this.state.blobs.filter(function (blob) { return blob.key !== key; });
        this.setState({ blobs: blobs });
    };
    Ink.prototype.fire = function (key) {
        var _this = this;
        var blobs = this.state.blobs.map(function (blob) {
            if (blob.key === key) {
                blob.fire = true;
                blob.timeout = setTimeout(function () {
                    _this.destroy(key);
                }, 1000);
                return blob;
            }
            else {
                return blob;
            }
        });
        this.setState({ blobs: blobs });
    };
    Ink.prototype.catchFire = function (key) {
        var _this = this;
        var listen = function () {
            _this.fire(key);
            document.removeEventListener('pointerup', listen);
            document.removeEventListener('pointercancel', listen);
        };
        document.addEventListener('pointerup', listen);
        document.addEventListener('pointercancel', listen);
    };
    return Ink;
}(React.PureComponent));
export { Ink };
//# sourceMappingURL=index.js.map