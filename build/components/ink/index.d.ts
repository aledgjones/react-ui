import * as React from 'react';
import './styles.css';
export interface InkBlob {
    key: number;
    x: number;
    y: number;
    diameter: number;
    fire?: boolean;
    timeout?: any;
}
export interface IProps {
    id?: string;
    className?: string;
    diameter?: number;
    cover?: boolean;
    center?: boolean;
}
export interface IState {
    blobs: InkBlob[];
}
export declare class Ink extends React.PureComponent<IProps, IState> {
    timeouts: any[];
    state: IState;
    componentWillUnmount(): void;
    render(): JSX.Element;
    private calcDiameter;
    private destroy;
    private fire;
    private catchFire;
    private create;
}
