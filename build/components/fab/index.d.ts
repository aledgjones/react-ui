import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    hidden?: boolean;
    path: string;
    color?: string;
}
export declare class Fab extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
