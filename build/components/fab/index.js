var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Icon, Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var Fab = /** @class */ (function (_super) {
    __extends(Fab, _super);
    function Fab() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Fab.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-fab', this.props.className, { 'ui-fab--disabled': this.props.disabled, 'ui-fab--hidden': this.props.hidden }), onClick: this.props.onClick },
            React.createElement(Ink, { cover: true }),
            React.createElement(Icon, { path: this.props.path, color: this.props.color || 'white' })));
    };
    return Fab;
}(React.PureComponent));
export { Fab };
//# sourceMappingURL=index.js.map