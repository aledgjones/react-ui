import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    path?: string;
    size?: number;
    color?: string;
}
export declare class Icon extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
