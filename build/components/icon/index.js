var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var Icon = /** @class */ (function (_super) {
    __extends(Icon, _super);
    function Icon() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Icon.prototype.render = function () {
        var size = this.props.size || 24;
        var color = this.props.color || 'rgba(0,0,0,.87)';
        return (React.createElement("svg", { viewBox: "0 0 24 24", id: this.props.id, className: mergeClasses('ui-icon', this.props.className), style: __assign({ width: size, height: size }, this.props.style) },
            React.createElement("path", { d: this.props.path, style: { fill: color } })));
    };
    return Icon;
}(React.PureComponent));
export { Icon };
//# sourceMappingURL=index.js.map