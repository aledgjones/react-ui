import * as React from 'react';
import { IContact, IContactCreator } from '../contact-picker';
import './styles.css';
export declare enum Tab {
    LIST = "list",
    CREATE = "create",
    EDIT = "edit"
}
interface IState {
    tab: Tab;
}
interface IProps {
    id?: string;
    className?: string;
    contacts: IContact[];
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onCancel: () => void;
    onSelect: (value: IContact) => void;
}
export declare class ContactPickerDialog extends React.PureComponent<IProps> {
    state: IState;
    private toTab;
    render(): JSX.Element | null;
}
export {};
