var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { ContactPickerDialogList } from '../contact-picker-dialog-list';
import { ContactPickerDialogCreate } from '../contact-picker-dialog-create';
import { Card } from '../card';
import './styles.css';
export var Tab;
(function (Tab) {
    Tab["LIST"] = "list";
    Tab["CREATE"] = "create";
    Tab["EDIT"] = "edit";
})(Tab || (Tab = {}));
var ContactPickerDialog = /** @class */ (function (_super) {
    __extends(ContactPickerDialog, _super);
    function ContactPickerDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { tab: Tab.LIST };
        _this.toTab = function (value) { return _this.setState({ tab: value }); };
        return _this;
    }
    ContactPickerDialog.prototype.render = function () {
        switch (this.state.tab) {
            case Tab.LIST:
                return (React.createElement(Card, { className: "ui-dialog__card ui-contact-picker-dialog__card" },
                    React.createElement(ContactPickerDialogList, { contacts: this.props.contacts, onNavigate: this.toTab, onCancel: this.props.onCancel, onSelect: this.props.onSelect })));
            case Tab.CREATE:
                return (React.createElement(Card, { className: "ui-dialog__card ui-contact-picker-dialog__card" },
                    React.createElement(ContactPickerDialogCreate, { onNavigate: this.toTab, onUpload: this.props.onUpload, onCreate: this.props.onCreate })));
            default:
                return null;
        }
    };
    return ContactPickerDialog;
}(React.PureComponent));
export { ContactPickerDialog };
//# sourceMappingURL=index.js.map