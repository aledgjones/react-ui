var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import { TabsItemComponent } from '../tabs-item';
import './styles.css';
var Tabs = /** @class */ (function (_super) {
    __extends(Tabs, _super);
    function Tabs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { left: 0, width: 100 };
        _this.onChange = function (value, tab) {
            _this.updateBar(tab);
            _this.props.onChange(value);
        };
        _this.updateBar = function (tab) {
            if (tab) {
                _this.setState({ left: tab.offsetLeft, width: tab.offsetWidth });
            }
        };
        return _this;
    }
    Tabs.prototype.render = function () {
        var _this = this;
        var children = React.Children.map(this.props.children, function (tab) {
            return React.createElement(TabsItemComponent, { id: tab.props.id, className: tab.props.className, value: tab.props.value, selected: _this.props.value === tab.props.value, onClick: _this.onChange, onInit: _this.updateBar, content: tab.props.children });
        });
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-tabs', this.props.className) },
            children,
            React.createElement("div", { className: "ui-tabs__bar", style: { left: this.state.left, width: this.state.width } })));
    };
    return Tabs;
}(React.PureComponent));
export { Tabs };
//# sourceMappingURL=index.js.map