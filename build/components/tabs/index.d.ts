import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    value: string;
    onChange: (value: string) => void;
}
interface IState {
    width: number;
    left: number;
}
export declare class Tabs extends React.PureComponent<IProps, IState> {
    state: IState;
    private onChange;
    private updateBar;
    render(): JSX.Element;
}
export {};
