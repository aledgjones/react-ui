import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
}
export declare class Subheader extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
