var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mdiClose, mdiAccountSearchOutline, mdiAccount } from '@mdi/js';
import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { Label } from '../label';
import { ContactPickerDialog } from '../contact-picker-dialog';
import { Avatar } from '../avatar';
import { Icon } from '../icon';
import './styles.css';
var ContactPicker = /** @class */ (function (_super) {
    __extends(ContactPicker, _super);
    function ContactPicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            focus: false,
            touched: false
        };
        _this.showDialog = function () {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.hideDialog = function () {
            _this.setState({ focus: false });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.clear = function () {
            _this.props.onChange(undefined);
        };
        _this.updateValue = function (contact) {
            _this.props.onChange(contact);
            _this.hideDialog();
        };
        return _this;
    }
    ContactPicker.prototype.hasError = function () {
        return this.props.required && !hasValue(this.props.value);
    };
    ContactPicker.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement("div", { className: mergeClasses('ui-contact-picker', this.props.className, {
                    'ui-contact-picker--disabled': this.props.disabled,
                    'ui-contact-picker--has-value': hasValue(this.props.value),
                    'ui-contact-picker--invalid': this.hasError(),
                    'ui-contact-picker--touched': this.state.touched
                }) },
                !this.props.value && React.createElement(Icon, { color: "rgb(150,150,150)", className: "ui-contact-picker__preview", path: mdiAccount }),
                this.props.value && React.createElement(Avatar, { className: "ui-contact-picker__preview", size: 24, src: this.props.value ? this.props.value.avatar : undefined, name: this.props.value ? this.props.value.name : 'A' }),
                !this.props.value && React.createElement("div", { className: "ui-contact-picker__placeholder", onClick: this.showDialog }, this.props.placeholder + (this.props.required ? '*' : '')),
                this.props.value && React.createElement("div", { className: "ui-contact-picker__value", onClick: this.showDialog },
                    React.createElement(Label, null,
                        React.createElement("p", null, this.props.value.name),
                        this.props.value.email && React.createElement("p", null, this.props.value.email))),
                (hasValue(this.props.value) && !this.props.required) ?
                    React.createElement(IconButton, { className: "ui-contact-picker__clear-icon", onClick: this.clear, path: mdiClose, color: "rgb(150,150,150)" }) :
                    React.createElement(IconButton, { className: "ui-contact-picker__down-icon", onClick: this.showDialog, path: mdiAccountSearchOutline, color: "rgb(150,150,150)" }),
                React.createElement("div", { className: "ui-contact-picker__bar" })),
            React.createElement(Dialog, { visible: !!this.state.focus, backdrop: true },
                React.createElement(ContactPickerDialog, { contacts: this.props.contacts, onCancel: this.hideDialog, onSelect: this.updateValue, onUpload: this.props.onUpload, onCreate: this.props.onCreate })));
    };
    return ContactPicker;
}(React.PureComponent));
export { ContactPicker };
//# sourceMappingURL=index.js.map