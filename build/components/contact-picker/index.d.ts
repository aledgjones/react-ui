import * as React from 'react';
import './styles.css';
export interface IContactCreator {
    name: string;
    avatar?: string;
    address?: string;
    email?: string;
}
export interface IContact extends IContactCreator {
    key: string;
}
export interface IProps {
    id?: string;
    className?: string;
    value: IContact | undefined;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onChange: (value: IContact | undefined) => void;
    onFocus?: () => void;
    onBlur?: () => void;
    contacts: IContact[];
}
export interface IState {
    focus: boolean;
    touched: boolean;
}
export declare class ContactPicker extends React.PureComponent<IProps, IState> {
    state: IState;
    private hasError;
    private showDialog;
    private hideDialog;
    private clear;
    private updateValue;
    render(): JSX.Element;
}
