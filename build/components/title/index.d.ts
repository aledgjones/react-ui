import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
}
export declare class Title extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
