import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    onClick?: () => void;
}
export declare class Label extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
