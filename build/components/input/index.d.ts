import * as React from 'react';
import './styles.css';
declare type InputType = 'text' | 'email' | 'password' | 'pin' | 'number';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    type: InputType;
    value?: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    autoComplete?: 'new-password' | 'off';
    allowFloat?: boolean;
    min?: number;
    max?: number;
    units?: string;
    onChange?: (value: string) => void;
    onEnter?: () => void;
    onFocus?: () => void;
    onBlur?: () => void;
    working?: boolean;
}
interface IState {
    focus: boolean;
    touched: boolean;
}
export declare class Input extends React.PureComponent<IProps, IState> {
    state: IState;
    onFocus: (e: React.FocusEvent<HTMLInputElement>) => void;
    onBlur: (e: React.FocusEvent<HTMLInputElement>) => void;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void | undefined;
    onEnter: (e: React.KeyboardEvent<HTMLInputElement>) => false | void | undefined;
    onClear: () => void | undefined;
    getError(): string | null;
    private inputTypeToType;
    render(): JSX.Element;
}
export {};
