// tslint:disable:no-any
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { CircularProgress } from '..';
import { hasValue, isEmail, isLongEnough, mergeClasses, isNumber, isLessThan, isGreaterThan, isInteger } from '../../utils';
import './styles.css';
var Input = /** @class */ (function (_super) {
    __extends(Input, _super);
    function Input() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { focus: false, touched: false };
        _this.onFocus = function (e) {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.onBlur = function (e) {
            _this.setState({ focus: false, touched: true });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.onChange = function (e) { return _this.props.onChange && _this.props.onChange(e.target.value); };
        _this.onEnter = function (e) { return e.which === 13 && _this.props.onEnter && _this.props.onEnter(); };
        _this.onClear = function () { return _this.props.onChange && _this.props.onChange(''); };
        return _this;
    }
    Input.prototype.getError = function () {
        if (this.props.required && !hasValue(this.props.value)) {
            return 'Required';
        }
        if (this.props.type === 'email' && !isEmail(this.props.value)) {
            return 'Must be a valid email';
        }
        if (this.props.type === 'password' && !isLongEnough(this.props.value, 8)) {
            return 'Must be at least 8 charecters long';
        }
        if (this.props.type === 'pin') {
            if (!isNumber(this.props.value)) {
                return 'Must contain only digits';
            }
            if (!isLongEnough(this.props.value, 4)) {
                return 'Must be at least 4 digits long';
            }
        }
        if (this.props.type === 'number') {
            if (!isNumber(this.props.value)) {
                return 'Must be a number';
            }
            if (!this.props.allowFloat && !isInteger(this.props.value)) {
                return 'Must be a integer';
            }
            if ((hasValue(this.props.min) && hasValue(this.props.max)) && (isLessThan(this.props.value, this.props.min) || isGreaterThan(this.props.value, this.props.max))) {
                return "Must be between " + this.props.min + " and " + this.props.max + " inclusive";
            }
            if (hasValue(this.props.min) && isLessThan(this.props.value, this.props.min)) {
                return "Must be " + (this.props.min || 0) + " or greater";
            }
            if (hasValue(this.props.max) && isGreaterThan(this.props.value, this.props.max)) {
                return "Must be " + (this.props.max || 0) + " or less";
            }
        }
        return null;
    };
    Input.prototype.inputTypeToType = function (type) {
        switch (type) {
            case 'pin':
            case 'password':
                return 'password';
            default:
                return 'text';
        }
    };
    Input.prototype.render = function () {
        var error = this.getError();
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-input', this.props.className, {
                'ui-input--disabled': this.props.disabled,
                'ui-input--focus': this.state.focus,
                'ui-input--has-value': hasValue(this.props.value),
                'ui-input--invalid': !!error,
                'ui-input--touched': this.state.touched
            }), style: this.props.style },
            React.createElement("div", { className: "ui-input__wrapper" },
                React.createElement("p", { className: "ui-input__placeholder" },
                    React.createElement("span", null, this.props.placeholder),
                    !!this.props.required && React.createElement("span", null, "*")),
                React.createElement("input", { type: this.inputTypeToType(this.props.type), value: this.props.value, onFocus: this.onFocus, onBlur: this.onBlur, onChange: this.onChange, onKeyUp: this.onEnter, disabled: this.props.disabled, autoComplete: this.props.autoComplete }),
                this.props.units && React.createElement("span", { className: "ui-input__units" }, this.props.units),
                !!this.props.working && React.createElement(CircularProgress, { className: "ui-circular-progress--mini ui-circular-progress--primary" }),
                React.createElement("div", { className: "ui-input__bar" }),
                (this.state.touched && !!error) && React.createElement("div", { className: "ui-input__error" }, error))));
    };
    return Input;
}(React.PureComponent));
export { Input };
//# sourceMappingURL=index.js.map