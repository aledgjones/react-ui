import * as React from 'react';
import { ICrop } from '../../utils/image';
import './styles.css';
interface IProps {
    src: string;
    height: number;
    width: number;
    zoom: number;
    crop: ICrop;
    croppable: boolean;
    onCropUpdate: (points: ICrop) => void;
    x: number;
    y: number;
    draggable: boolean;
    onDragUpdate: (x: number, y: number) => void;
    saving: boolean;
}
interface IState {
    active: boolean;
}
export declare class ImageViewerChild extends React.PureComponent<IProps, IState> {
    state: IState;
    private pxHeightToPercent;
    private pxWidthToPercent;
    private percentHeightTopx;
    private percentWidthTopx;
    private minHeight;
    private minWidth;
    private maxHeight;
    private maxWidth;
    private startDrag;
    private startCropDrag;
    private startCrop;
    private onDrag;
    render(): JSX.Element;
}
export {};
