var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { CircularProgress } from '../circular-progress';
import { mergeClasses } from '../../utils/merge-classes';
import { pointerMoveGenerator } from '../../utils/pointer-move-generator';
import './styles.css';
var ImageViewerChild = /** @class */ (function (_super) {
    __extends(ImageViewerChild, _super);
    function ImageViewerChild() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { active: false };
        _this.pxHeightToPercent = function (px) { return px * (100 / _this.props.height); };
        _this.pxWidthToPercent = function (px) { return px * (100 / _this.props.width); };
        _this.percentHeightTopx = function (percent) { return percent * (_this.props.height / 100); };
        _this.percentWidthTopx = function (percent) { return percent * (_this.props.width / 100); };
        _this.minHeight = _this.pxHeightToPercent(48);
        _this.minWidth = _this.pxWidthToPercent(48);
        _this.maxHeight = 100;
        _this.maxWidth = 100;
        _this.startDrag = pointerMoveGenerator(function (e, data) {
            if (_this.props.draggable) {
                _this.setState({ active: true });
                data.pointer = { x: e.screenX, y: e.screenY };
                data.box = { x: _this.props.x, y: _this.props.y };
                return true;
            }
            else {
                return false;
            }
        }, function (e, init) {
            var x = init.box.x + (e.screenX - init.pointer.x);
            var y = init.box.y + (e.screenY - init.pointer.y);
            _this.props.onDragUpdate(x, y);
        }, function () { return _this.setState({ active: false }); });
        _this.startCropDrag = pointerMoveGenerator(function (e, data) {
            _this.setState({ active: true });
            data.pointer = { x: e.screenX, y: e.screenY };
            data.crop = [
                { x: _this.props.crop[0].x, y: _this.props.crop[0].y },
                { x: _this.props.crop[1].x, y: _this.props.crop[1].y }
            ];
            return true;
        }, function (e, data) {
            var crop = [__assign({}, data.crop[0]), __assign({}, data.crop[1])];
            var x = _this.pxWidthToPercent(e.screenX - data.pointer.x);
            var y = _this.pxHeightToPercent(e.screenY - data.pointer.y);
            if (data.crop[0].x + x < 0) {
                crop[0].x = 0;
                crop[1].x = data.crop[1].x - data.crop[0].x;
            }
            else if (data.crop[1].x + x > _this.maxWidth) {
                crop[0].x = _this.maxWidth - (data.crop[1].x - data.crop[0].x);
                crop[1].x = _this.maxWidth;
            }
            else {
                crop[0].x = crop[0].x + x;
                crop[1].x = crop[1].x + x;
            }
            if (data.crop[0].y + y < 0) {
                crop[0].y = 0;
                crop[1].y = data.crop[1].y - data.crop[0].y;
            }
            else if (data.crop[1].y + y > _this.maxHeight) {
                crop[0].y = _this.maxHeight - (data.crop[1].y - data.crop[0].y);
                crop[1].y = _this.maxHeight;
            }
            else {
                crop[0].y = crop[0].y + y;
                crop[1].y = crop[1].y + y;
            }
            _this.props.onCropUpdate(crop);
        }, function () { return _this.setState({ active: false }); });
        _this.startCrop = pointerMoveGenerator(function (e, data) {
            _this.setState({ active: true });
            data.direction = e.target.dataset.direction;
            data.pointer = { x: e.screenX, y: e.screenY };
            data.crop = [
                { x: _this.props.crop[0].x, y: _this.props.crop[0].y },
                { x: _this.props.crop[1].x, y: _this.props.crop[1].y }
            ];
            return true;
        }, function (e, data) {
            var crop = [__assign({}, data.crop[0]), __assign({}, data.crop[1])];
            if (data.direction.includes('n')) {
                var y = data.crop[0].y + _this.pxHeightToPercent(e.screenY - data.pointer.y);
                if (y < 0) {
                    crop[0].y = 0;
                }
                else if (y > crop[1].y - _this.minHeight) {
                    crop[0].y = crop[1].y - _this.minHeight;
                }
                else {
                    crop[0].y = y;
                }
            }
            if (data.direction.includes('e')) {
                var x = data.crop[1].x + _this.pxWidthToPercent(e.screenX - data.pointer.x);
                if (x < crop[0].x + _this.minWidth) {
                    crop[1].x = crop[0].x + _this.minWidth;
                }
                else if (x > _this.maxWidth) {
                    crop[1].x = _this.maxWidth;
                }
                else {
                    crop[1].x = x;
                }
            }
            if (data.direction.includes('s')) {
                var y = data.crop[1].y + _this.pxHeightToPercent(e.screenY - data.pointer.y);
                if (y < data.crop[0].y + _this.minHeight) {
                    crop[1].y = data.crop[0].y + _this.minHeight;
                }
                else if (y > _this.maxHeight) {
                    crop[1].y = _this.maxHeight;
                }
                else {
                    crop[1].y = y;
                }
            }
            if (data.direction.includes('w')) {
                var x = data.crop[0].x + _this.pxWidthToPercent(e.screenX - data.pointer.x);
                if (x < 0) {
                    crop[0].x = 0;
                }
                else if (x > crop[1].x - _this.minWidth) {
                    crop[0].x = crop[1].x - _this.minWidth;
                }
                else {
                    crop[0].x = x;
                }
            }
            _this.props.onCropUpdate(crop);
        }, function () { return _this.setState({ active: false }); });
        _this.onDrag = function (e) {
            if (_this.props.croppable) {
                return _this.startCropDrag(e);
            }
            if (_this.props.draggable) {
                return _this.startDrag(e);
            }
        };
        return _this;
    }
    ImageViewerChild.prototype.render = function () {
        var top = this.percentHeightTopx(this.props.crop[0].y * -1);
        var left = this.percentWidthTopx(this.props.crop[0].x * -1);
        return React.createElement("div", { className: mergeClasses("photo", {
                'photo--saving': this.props.saving,
                'photo--croppable': this.props.croppable,
                'photo--draggable': this.props.croppable || this.props.draggable,
                'photo--no-transition': this.state.active
            }), style: {
                width: this.percentWidthTopx(this.props.crop[1].x - this.props.crop[0].x),
                height: this.percentHeightTopx(this.props.crop[1].y - this.props.crop[0].y),
                transform: "translate(" + this.props.x + "px, " + this.props.y + "px) scale(" + this.props.zoom + ")"
            } },
            React.createElement("div", { className: "photo__ghost", style: {
                    backgroundImage: "url(" + this.props.src + ")",
                    top: top,
                    left: left,
                    width: this.props.width,
                    height: this.props.height
                } }),
            React.createElement("div", { className: "photo__paint-container" },
                React.createElement("div", { className: "photo__paint", style: {
                        backgroundImage: "url(" + this.props.src + ")",
                        top: top,
                        left: left,
                        width: this.props.width,
                        height: this.props.height
                    } }),
                this.props.saving && React.createElement(CircularProgress, { className: "photo__spinner" })),
            React.createElement("div", { onPointerDown: this.onDrag, className: "photo__drag-box", style: {
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                } }),
            this.props.croppable && React.createElement(React.Fragment, null,
                React.createElement("div", { "data-direction": "n", onPointerDown: this.startCrop, className: "photo__handle photo__handle--n" }),
                React.createElement("div", { "data-direction": "ne", onPointerDown: this.startCrop, className: "photo__handle photo__handle--ne" }),
                React.createElement("div", { "data-direction": "e", onPointerDown: this.startCrop, className: "photo__handle photo__handle--e" }),
                React.createElement("div", { "data-direction": "se", onPointerDown: this.startCrop, className: "photo__handle photo__handle--se" }),
                React.createElement("div", { "data-direction": "s", onPointerDown: this.startCrop, className: "photo__handle photo__handle--s" }),
                React.createElement("div", { "data-direction": "sw", onPointerDown: this.startCrop, className: "photo__handle photo__handle--sw" }),
                React.createElement("div", { "data-direction": "w", onPointerDown: this.startCrop, className: "photo__handle photo__handle--w" }),
                React.createElement("div", { "data-direction": "nw", onPointerDown: this.startCrop, className: "photo__handle photo__handle--nw" })));
    };
    return ImageViewerChild;
}(React.PureComponent));
export { ImageViewerChild };
//# sourceMappingURL=index.js.map