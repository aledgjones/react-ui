var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { DateTime } from 'luxon';
import { mdiChevronLeft, mdiChevronRight } from '@mdi/js';
import { Card } from '..';
import { Toolbar } from '../toolbar';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { ToolbarLine } from '../toolbar-line';
import { Content } from '../content';
import { Button } from '../button';
import { Divider } from '../divider';
import { mergeClasses } from '../../utils';
import './styles.css';
var DatePickerDialog = /** @class */ (function (_super) {
    __extends(DatePickerDialog, _super);
    function DatePickerDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = (function () {
            var dt = _this.props.value ? DateTime.fromMillis(_this.props.value) : DateTime.local();
            return {
                value: dt,
                view: dt.set({ day: 1 })
            };
        })();
        _this.updateValue = function (year, month, day) {
            var timestamp = DateTime.local(year, month, day).toMillis();
            _this.props.onComplete(timestamp);
        };
        _this.prev = function () {
            _this.setState({ view: _this.state.view.minus({ months: 1 }) });
        };
        _this.next = function () {
            _this.setState({ view: _this.state.view.plus({ months: 1 }) });
        };
        return _this;
    }
    DatePickerDialog.prototype.buildGrid = function (view, value) {
        var _this = this;
        var now = DateTime.local();
        var grid = new Array(view.weekday - 1).fill(null);
        var _loop_1 = function (i) {
            grid.push({
                current: now.hasSame(view, 'year') && now.hasSame(view, 'month') && now.day === i,
                selected: value.hasSame(view, 'year') && value.hasSame(view, 'month') && value.day === i,
                value: i,
                onClick: function () { return _this.updateValue(view.year, view.month, i); }
            });
        };
        for (var i = 1; i <= view.daysInMonth; i++) {
            _loop_1(i);
        }
        return grid;
    };
    DatePickerDialog.prototype.cell = function (cell) {
        if (cell) {
            return React.createElement("div", { onClick: cell.onClick, className: "ui-date-picker-dialog__cell" },
                React.createElement("div", { className: mergeClasses("ui-date-picker-dialog__cell-value", { 'ui-date-picker-dialog__cell-value--selected': cell.selected, 'ui-date-picker-dialog__cell-value--current': cell.current }) },
                    React.createElement("div", { className: "ui-date-picker-dialog__cell-digit" }, cell.value)));
        }
        else {
            return React.createElement("div", { className: "ui-date-picker-dialog__cell" });
        }
    };
    DatePickerDialog.prototype.render = function () {
        var grid = this.buildGrid(this.state.view, this.state.value);
        return React.createElement(Card, { className: "ui-dialog__card ui-date-picker-dialog__card" },
            React.createElement(Toolbar, null,
                React.createElement(ToolbarLine, null,
                    React.createElement(Title, null,
                        this.state.view.toLocaleString({ month: 'long' }),
                        " ",
                        this.state.view.year),
                    React.createElement(IconButton, { className: "ui-icon-button--margin", onClick: this.prev, path: mdiChevronLeft }),
                    React.createElement(IconButton, { onClick: this.next, path: mdiChevronRight }))),
            React.createElement("div", { className: "ui-date-picker-dialog__header" },
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "M"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "T"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "W"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "T"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "F"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "S"),
                React.createElement("div", { className: "ui-date-picker-dialog__label" }, "S")),
            React.createElement(Divider, null),
            React.createElement("div", { className: "ui-date-picker-dialog__grid" }, grid.map(this.cell)),
            React.createElement(Content, { className: "ui-content--buttons ui-content--compact" },
                React.createElement(Button, { className: "ui-button--compact", onClick: this.props.onCancel }, "Cancel")));
    };
    return DatePickerDialog;
}(React.PureComponent));
export { DatePickerDialog };
//# sourceMappingURL=index.js.map