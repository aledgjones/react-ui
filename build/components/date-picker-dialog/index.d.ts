import * as React from 'react';
import { DateTime } from 'luxon';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value?: number;
    required?: boolean;
    onCancel: () => void;
    onComplete: (value: number) => void;
}
export interface IState {
    value: DateTime;
    view: DateTime;
}
export declare class DatePickerDialog extends React.PureComponent<IProps, IState> {
    state: IState;
    private updateValue;
    private prev;
    private next;
    private buildGrid;
    private cell;
    render(): JSX.Element;
}
