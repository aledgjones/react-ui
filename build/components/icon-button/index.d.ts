import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    style?: React.CSSProperties;
    toggle?: boolean;
    path: string;
    color?: string;
    size?: number;
}
export declare class IconButton extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
