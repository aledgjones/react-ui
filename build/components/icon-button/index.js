var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { Icon, Ink } from '..';
import { mergeClasses } from '../../utils';
import './styles.css';
var IconButton = /** @class */ (function (_super) {
    __extends(IconButton, _super);
    function IconButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IconButton.prototype.render = function () {
        var size = this.props.size || 24;
        var color = this.props.color || 'rgba(0,0,0,.87)';
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-icon-button', this.props.className, {
                'ui-icon-button--disabled': this.props.disabled,
                'ui-icon-button--toggle': this.props.toggle !== undefined,
                'ui-icon-button--toggle-on': this.props.toggle === true
            }), onClick: this.props.onClick, style: __assign({ height: size, width: size }, this.props.style) },
            React.createElement(Ink, { center: true }),
            React.createElement(Icon, { path: this.props.path, color: color, size: size })));
    };
    return IconButton;
}(React.PureComponent));
export { IconButton };
//# sourceMappingURL=index.js.map