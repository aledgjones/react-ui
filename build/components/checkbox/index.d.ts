import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value: boolean;
    disabled?: boolean;
    onChange?: (val: boolean) => void;
}
export declare class Checkbox extends React.PureComponent<IProps> {
    onClick: () => void | undefined;
    render(): JSX.Element;
}
