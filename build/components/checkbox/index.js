var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
import { Ink } from '../ink';
import { Icon } from '../icon';
import { mdiCheck } from '@mdi/js';
var Checkbox = /** @class */ (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onClick = function () { return _this.props.onChange && _this.props.onChange(!_this.props.value); };
        return _this;
    }
    Checkbox.prototype.render = function () {
        return React.createElement("div", { id: this.props.id, className: mergeClasses('ui-checkbox', this.props.className, { 'ui-checkbox--active': this.props.value, 'ui-checkbox--disabled': this.props.disabled }), style: this.props.style, onClick: this.onClick },
            React.createElement(Icon, { color: "white", className: "ui-checkbox__icon", path: mdiCheck }),
            React.createElement(Ink, { center: true }));
    };
    return Checkbox;
}(React.PureComponent));
export { Checkbox };
//# sourceMappingURL=index.js.map