var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { DateTime } from 'luxon';
import { mdiClose, mdiCalendarOutline } from '@mdi/js';
import { IconButton } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { Dialog } from '../dialog';
import { DatePickerDialog } from '../date-picker-dialog';
import './styles.css';
var DatePicker = /** @class */ (function (_super) {
    __extends(DatePicker, _super);
    function DatePicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { focus: false, touched: false };
        _this.showDialog = function () {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.hideDialog = function () {
            _this.setState({ focus: false });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.clear = function () {
            _this.props.onChange();
        };
        _this.updateValue = function (date) {
            _this.props.onChange(date);
            _this.hideDialog();
        };
        return _this;
    }
    DatePicker.prototype.hasError = function () {
        return this.props.required && !hasValue(this.props.value);
    };
    DatePicker.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement("div", { className: mergeClasses('ui-date-picker', this.props.className, {
                    'ui-date-picker--disabled': this.props.disabled,
                    'ui-date-picker--focus': this.state.focus,
                    'ui-date-picker--has-value': hasValue(this.props.value),
                    'ui-date-picker--invalid': this.hasError(),
                    'ui-date-picker--touched': this.state.touched
                }) },
                React.createElement("div", { className: "ui-date-picker__placeholder" }, this.props.placeholder + (this.props.required ? '*' : '')),
                React.createElement("div", { className: "ui-date-picker__value", onClick: this.showDialog }, this.props.value ? DateTime.fromMillis(this.props.value).toLocaleString(DateTime.DATE_SHORT) : ''),
                (hasValue(this.props.value) && !this.props.required) && React.createElement(IconButton, { className: "ui-date-picker__clear-icon", onClick: this.clear, path: mdiClose, color: "rgb(150,150,150)" }),
                (!hasValue(this.props.value) || !!this.props.required) && React.createElement(IconButton, { className: "ui-date-picker__open-icon", onClick: this.showDialog, path: mdiCalendarOutline, color: "rgb(150,150,150)" }),
                React.createElement("div", { className: "ui-date-picker__bar" })),
            React.createElement(Dialog, { visible: !!this.state.focus, backdrop: true },
                React.createElement(DatePickerDialog, { value: this.props.value, onCancel: this.hideDialog, onComplete: this.updateValue })));
    };
    return DatePicker;
}(React.PureComponent));
export { DatePicker };
//# sourceMappingURL=index.js.map