import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value?: number;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value?: any) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}
export interface IState {
    focus: boolean;
    touched: boolean;
}
export declare class DatePicker extends React.PureComponent<IProps, IState> {
    state: IState;
    hasError(): boolean | undefined;
    private showDialog;
    private hideDialog;
    private clear;
    private updateValue;
    render(): JSX.Element;
}
