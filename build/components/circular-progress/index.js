var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var CircularProgress = /** @class */ (function (_super) {
    __extends(CircularProgress, _super);
    function CircularProgress() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CircularProgress.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-circular-progress', this.props.className) },
            React.createElement("svg", { className: "ui-circular-progress__box", viewBox: "25 25 50 50" },
                React.createElement("circle", { className: "ui-circular-progress__path", cx: "50", cy: "50", r: "20" }))));
    };
    return CircularProgress;
}(React.PureComponent));
export { CircularProgress };
//# sourceMappingURL=index.js.map