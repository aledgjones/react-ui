var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import * as React from 'react';
import { mdiArrowLeft, mdiCrop, mdiMagnifyPlusOutline, mdiMagnifyMinusOutline, mdiInformationOutline, mdiDotsVertical, mdiRestore, mdiClose } from '@mdi/js';
import { scaleToFit } from '../../utils/scale-to-fit';
import { getImage, getImageSize, getCroppedImage, cropChanged } from '../../utils/image';
import { mergeClasses } from '../../utils/merge-classes';
import { download } from '../../utils/download';
import { toast } from '../toast/store';
import { Toolbar } from '../toolbar';
import { ToolbarLine } from '../toolbar-line';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { Tooltip } from '../tooltip';
import { Menu } from '../menu';
import { List } from '../list';
import { ListItem } from '../list-item';
import { Label } from '../label';
import { Button } from '../button';
import { CircularProgress } from '../circular-progress';
import { ImageViewerChild } from '../image-viewer-child';
import { ResizeListener } from '../../utils/resize';
import './styles.css';
var ImageViewer = /** @class */ (function (_super) {
    __extends(ImageViewer, _super);
    function ImageViewer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.noCrop = [{ x: 0, y: 0 }, { x: 100, y: 100 }];
        _this.state = {
            ready: false,
            x: 0,
            y: 0,
            height: 0,
            width: 0,
            dragging: false,
            info: false,
            cropMode: false,
            crop: _this.props.crop || _this.noCrop,
            zoom: 1,
            saving: false
        };
        _this.onResize = function (parentBox) { return __awaiter(_this, void 0, void 0, function () {
            var image, cropBox, scale, scaledImageBox;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.parentBox = parentBox;
                        return [4 /*yield*/, getImage(this.props.src)];
                    case 1:
                        image = _a.sent();
                        return [4 /*yield*/, getImageSize(image, this.state.crop)];
                    case 2:
                        cropBox = _a.sent();
                        scale = scaleToFit(this.state.cropMode ? image : cropBox, parentBox);
                        scaledImageBox = { height: image.height * scale, width: image.width * scale };
                        this.setState(__assign({}, scaledImageBox, { ready: true }));
                        return [2 /*return*/];
                }
            });
        }); };
        _this.downloadFull = function () { return __awaiter(_this, void 0, void 0, function () {
            var ext, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        ext = this.props.src.split('.').pop();
                        return [4 /*yield*/, download(this.props.src, "image." + ext)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        toast.show({ text: 'Sorry, could not download' });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.downloadCrop = function () { return __awaiter(_this, void 0, void 0, function () {
            var blob, url, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, getCroppedImage(this.props.src, this.state.crop)];
                    case 1:
                        blob = _a.sent();
                        url = URL.createObjectURL(blob);
                        return [4 /*yield*/, download(url, 'image-cropped.jpg')];
                    case 2:
                        _a.sent();
                        URL.revokeObjectURL(url);
                        return [3 /*break*/, 4];
                    case 3:
                        err_2 = _a.sent();
                        toast.show({ text: 'Sorry, could not download' });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        _this.toggleInfo = function () { return _this.setState({ info: !_this.state.info }); };
        _this.zoomIn = function () { return _this.setState({ zoom: 2 }); };
        _this.zoomOut = function () { return _this.setState({ zoom: 1, x: 0, y: 0 }); };
        _this.updateCoordinates = function (x, y) { return _this.setState({ x: x, y: y }); };
        _this.enterCropMode = function () { return _this.setState({ cropMode: true, info: false, zoom: 1, x: 0, y: 0 }, function () { return _this.onResize(_this.parentBox); }); };
        _this.exitCropMode = function () { return _this.setState({ cropMode: false }, function () { return _this.onResize(_this.parentBox); }); };
        _this.updateLocalCrop = function (points) { return _this.setState({ crop: points }); };
        _this.isCropped = function () { return _this.state.crop[0].x !== 0 || _this.state.crop[0].y !== 0 || _this.state.crop[1].x !== 100 || _this.state.crop[1].y !== 100; };
        _this.restoreCrop = function () { return _this.setState({ crop: _this.props.crop || _this.noCrop }); };
        _this.saveCrop = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!cropChanged(this.props.crop || this.noCrop, this.state.crop)) return [3 /*break*/, 3];
                        this.setState({ saving: true });
                        if (!this.props.onCropSave) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.props.onCropSave(this.state.crop)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        this.setState({ saving: false });
                        toast.show({ text: 'Updated' }, 2000);
                        _a.label = 3;
                    case 3:
                        this.exitCropMode();
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    ImageViewer.prototype.render = function () {
        var isCropped = this.isCropped();
        return React.createElement(React.Fragment, null,
            React.createElement("div", { className: "photo-viewer" },
                React.createElement(Toolbar, { className: mergeClasses("photo-viewer__toolbar", { 'photo-viewer__toolbar--info': this.state.info }) },
                    !this.state.cropMode && React.createElement(ToolbarLine, null,
                        React.createElement(IconButton, { className: "ui-icon-button--margin", onClick: this.props.onClose, path: mdiArrowLeft, color: "white" }),
                        React.createElement(Title, null, "Back"),
                        this.props.croppable && React.createElement(Tooltip, { className: "photo-viewer__tool", text: "Edit" },
                            React.createElement(IconButton, { onClick: this.enterCropMode, path: mdiCrop, color: "white" })),
                        this.state.zoom === 1 ?
                            React.createElement(Tooltip, { className: "photo-viewer__tool", text: "Zoom in" },
                                React.createElement(IconButton, { onClick: this.zoomIn, path: mdiMagnifyPlusOutline, color: "white" })) :
                            React.createElement(Tooltip, { className: "photo-viewer__tool", text: "Zoom out" },
                                React.createElement(IconButton, { onClick: this.zoomOut, path: mdiMagnifyMinusOutline, color: "white" })),
                        React.createElement(Tooltip, { className: "photo-viewer__tool", text: "Info" },
                            React.createElement(IconButton, { onClick: this.toggleInfo, path: mdiInformationOutline, color: "white" })),
                        React.createElement(Menu, { className: "photo-viewer__icon", direction: "right", width: 200, target: React.createElement(IconButton, { path: mdiDotsVertical, color: "white" }), menu: React.createElement(List, null,
                                isCropped && React.createElement(React.Fragment, null,
                                    React.createElement(ListItem, { onClick: this.downloadCrop },
                                        React.createElement(Label, null,
                                            React.createElement("p", null, "Download"))),
                                    React.createElement(ListItem, { onClick: this.downloadFull },
                                        React.createElement(Label, null,
                                            React.createElement("p", null, "Download original")))),
                                !isCropped && React.createElement(ListItem, { onClick: this.downloadFull },
                                    React.createElement(Label, null,
                                        React.createElement("p", null, "Download")))) })),
                    this.state.cropMode && React.createElement(ToolbarLine, null,
                        React.createElement("div", { className: "photo-viewer__spacer" }),
                        React.createElement(Tooltip, { className: "photo-viewer__tool", text: "Restore crop" },
                            React.createElement(IconButton, { disabled: this.state.saving || !cropChanged(this.props.crop || this.noCrop, this.state.crop), onClick: this.restoreCrop, path: mdiRestore, color: "white" })),
                        React.createElement(Button, { disabled: this.state.saving, className: "ui-button--compact", onClick: this.saveCrop }, "Done"))),
                React.createElement("div", { className: mergeClasses("photo-viewer__content", { 'photo-viewer__content--info': this.state.info }) },
                    React.createElement("div", { className: "photo-viewer__container" },
                        React.createElement(ResizeListener, { onResize: this.onResize }),
                        !this.state.ready && React.createElement(CircularProgress, null),
                        this.state.ready && React.createElement(ImageViewerChild, { src: this.props.src, height: this.state.height, width: this.state.width, zoom: this.state.zoom, crop: this.state.crop, croppable: this.state.cropMode, onCropUpdate: this.updateLocalCrop, x: this.state.x, y: this.state.y, draggable: this.state.zoom > 1 && !this.state.cropMode, onDragUpdate: this.updateCoordinates, saving: this.state.saving })))),
            React.createElement("div", { className: mergeClasses('photo-viewer__info', { 'photo-viewer__info--show': this.state.info }) },
                React.createElement(Toolbar, null,
                    React.createElement(ToolbarLine, null,
                        React.createElement(Title, null, "Info"),
                        React.createElement(IconButton, { onClick: this.toggleInfo, path: mdiClose }))),
                this.props.children));
    };
    return ImageViewer;
}(React.PureComponent));
export { ImageViewer };
//# sourceMappingURL=index.js.map