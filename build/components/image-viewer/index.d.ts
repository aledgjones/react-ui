import * as React from 'react';
import { ICrop } from '../../utils/image';
import './styles.css';
interface IProps {
    src: string;
    crop?: ICrop;
    croppable?: boolean;
    onClose: () => void;
    onCropSave?: (points: ICrop) => void;
}
interface IState {
    ready: boolean;
    info: boolean;
    x: number;
    y: number;
    height: number;
    width: number;
    dragging: boolean;
    cropMode: boolean;
    crop: ICrop;
    zoom: number;
    saving: boolean;
}
export declare class ImageViewer extends React.PureComponent<IProps, IState> {
    private noCrop;
    private parentBox;
    state: IState;
    private onResize;
    downloadFull: () => Promise<void>;
    downloadCrop: () => Promise<void>;
    private toggleInfo;
    private zoomIn;
    private zoomOut;
    private updateCoordinates;
    private enterCropMode;
    private exitCropMode;
    private updateLocalCrop;
    private isCropped;
    private restoreCrop;
    private saveCrop;
    render(): JSX.Element;
}
export {};
