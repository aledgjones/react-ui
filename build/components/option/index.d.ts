import * as React from 'react';
interface IProps {
    value: any;
    displayAs: string;
}
export declare class Option extends React.PureComponent<IProps> {
    render(): React.ReactNode;
}
export {};
