import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    visible: boolean;
    disabled?: boolean;
    transparent: boolean;
    onClick?: () => void;
}
export declare class Backdrop extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
