var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var Backdrop = /** @class */ (function (_super) {
    __extends(Backdrop, _super);
    function Backdrop() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Backdrop.prototype.render = function () {
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-backdrop', this.props.className, { 'ui-backdrop--visible': this.props.visible, 'ui-backdrop--disabled': this.props.disabled, 'ui-backdrop--transparent': this.props.transparent }), onClick: this.props.onClick }, this.props.children));
    };
    return Backdrop;
}(React.PureComponent));
export { Backdrop };
//# sourceMappingURL=index.js.map