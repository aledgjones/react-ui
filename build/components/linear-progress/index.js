var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import './styles.css';
var LinearProgress = /** @class */ (function (_super) {
    __extends(LinearProgress, _super);
    function LinearProgress() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LinearProgress.prototype.render = function () {
        var hasProgress = this.props.progress !== undefined;
        return (React.createElement("div", { id: this.props.id, className: mergeClasses('ui-linear-progress', { 'ui-linear-progress--animate': !hasProgress }, this.props.className) },
            React.createElement("div", { className: "ui-linear-progress__tick ui-linear-progress__tick--one", style: { width: hasProgress ? this.props.progress + "%" : undefined } }),
            !hasProgress && React.createElement("div", { className: "ui-linear-progress__tick ui-linear-progress__tick--two" })));
    };
    return LinearProgress;
}(React.PureComponent));
export { LinearProgress };
//# sourceMappingURL=index.js.map