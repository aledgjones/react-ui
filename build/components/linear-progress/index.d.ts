import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    progress?: number;
}
export declare class LinearProgress extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
export {};
