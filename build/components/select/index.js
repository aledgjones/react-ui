var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Card, IconButton, List, ListItem } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { mdiClose, mdiChevronDown } from '@mdi/js';
import './styles.css';
var Select = /** @class */ (function (_super) {
    __extends(Select, _super);
    function Select() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { focus: false, touched: false, value: '' };
        _this.close = function () {
            document.removeEventListener('click', _this.close);
            _this.setState({ focus: false });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.open = function () {
            document.addEventListener('click', _this.close);
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.toggle = function () {
            // the document cick listener will close the dialog if open
            if (!_this.state.focus) {
                _this.open();
            }
        };
        _this.clear = function () {
            if (_this.props.onChange) {
                _this.props.onChange(null);
            }
            ;
        };
        return _this;
    }
    Select.prototype.hasError = function () {
        return this.props.required && !hasValue(this.props.value);
    };
    Select.prototype.onChange = function (value, displayAs) {
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    };
    Select.prototype.parseIncomingValue = function (props) {
        var _this = this;
        if (hasValue(props.value)) {
            React.Children.forEach(props.children, function (child) {
                if (child.props.value === props.value) {
                    _this.setState({ value: child.props.displayAs });
                }
            });
        }
        else {
            this.setState({ value: '' });
        }
    };
    Select.prototype.componentDidMount = function () {
        this.parseIncomingValue(this.props);
    };
    Select.prototype.componentWillReceiveProps = function (props) {
        if (this.props.value !== props.value) {
            this.parseIncomingValue(props);
        }
    };
    Select.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", { className: mergeClasses('ui-select', this.props.className, {
                'ui-select--disabled': this.props.disabled,
                'ui-select--focus': this.state.focus,
                'ui-select--has-value': hasValue(this.props.value),
                'ui-select--invalid': this.hasError(),
                'ui-select--touched': this.state.touched
            }) },
            React.createElement("div", { className: "ui-select__placeholder" }, this.props.placeholder + (this.props.required ? '*' : '')),
            React.createElement("div", { className: "ui-select__value", onClick: this.toggle }, this.state.value),
            (hasValue(this.props.value) && !this.props.required) && React.createElement(IconButton, { className: "ui-select__clear-icon", onClick: this.clear, path: mdiClose, color: "rgb(150,150,150)" }),
            (!hasValue(this.props.value) || !!this.props.required) && React.createElement(IconButton, { className: "ui-select__down-icon", onClick: this.toggle, path: mdiChevronDown, color: "rgb(150,150,150)" }),
            React.createElement("div", { className: "ui-select__bar" }),
            !!this.state.focus &&
                React.createElement(Card, { className: "ui-select__card" },
                    React.createElement(List, null, React.Children.map(this.props.children, function (child) {
                        return React.createElement(ListItem, { key: child.props.value, onClick: function () { return _this.onChange(child.props.value, child.props.displayAs); } }, child);
                    })))));
    };
    return Select;
}(React.PureComponent));
export { Select };
//# sourceMappingURL=index.js.map