import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value: any) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}
export interface IState {
    focus: boolean;
    touched: boolean;
    value: string;
}
export declare class Select extends React.PureComponent<IProps, IState> {
    state: IState;
    hasError(): boolean | undefined;
    close: () => void;
    open: () => void;
    toggle: () => void;
    clear: () => void;
    onChange(value: any, displayAs: string): void;
    parseIncomingValue(props: any): void;
    componentDidMount(): void;
    componentWillReceiveProps(props: any): void;
    render(): JSX.Element;
}
