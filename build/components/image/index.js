var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import React, { useState, useEffect } from 'react';
import { mergeClasses, getCroppedImage } from '../../utils';
import './styles.css';
export function Image(props) {
    var _this = this;
    var id = props.id, className = props.className, style = props.style, src = props.src, crop = props.crop, wait = props.wait, onClick = props.onClick;
    var _a = useState(true), waiting = _a[0], setWaiting = _a[1]; // defer loading;
    var _b = useState(true), loading = _b[0], setLoading = _b[1];
    var _c = useState(undefined), url = _c[0], setUrl = _c[1];
    useEffect(function () {
        if (waiting && !wait) {
            setWaiting(false);
        }
    }, [waiting, wait]);
    useEffect(function () {
        var load = function (src, crop) { return __awaiter(_this, void 0, void 0, function () {
            var blob;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, getCroppedImage(src, crop)];
                    case 1:
                        blob = _a.sent();
                        setUrl(URL.createObjectURL(blob));
                        setLoading(false);
                        return [2 /*return*/];
                }
            });
        }); };
        if (!waiting) {
            var _crop = crop || [{ x: 0, y: 0 }, { x: 100, y: 100 }];
            if (src) {
                load(src, _crop);
            }
            else {
                setUrl(undefined);
            }
        }
        return function () {
            if (url) {
                URL.revokeObjectURL(url);
            }
        };
    }, [src, crop]);
    return React.createElement("div", { id: id, className: mergeClasses('ui-image', className), style: style, onClick: onClick }, (src && !loading) && React.createElement("div", { className: "ui-image__paint", style: { backgroundImage: "url(" + src + ")" } }));
}
//# sourceMappingURL=index.js.map