import React from 'react';
import { ICrop } from '../../utils';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    src: string;
    crop?: ICrop;
    wait?: boolean;
    onClick?: () => void;
}
export declare function Image(props: IProps): JSX.Element;
export {};
