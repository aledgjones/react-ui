var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mergeClasses } from '../../utils';
import { Card } from '../card';
import './styles.css';
var Menu = /** @class */ (function (_super) {
    __extends(Menu, _super);
    function Menu() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.card = React.createRef();
        _this.state = { show: false };
        _this.showMenu = function () {
            _this.setState({ show: true });
            document.addEventListener('pointerdown', _this.catchPointer);
        };
        _this.catchPointer = function (e) {
            if (e.target && _this.card.current && !_this.card.current.contains(e.target)) {
                _this.hideMenu();
            }
        };
        _this.hideMenu = function () {
            _this.setState({ show: false });
            document.removeEventListener('pointerdown', _this.catchPointer);
        };
        return _this;
    }
    Menu.prototype.render = function () {
        return React.createElement("div", { id: this.props.id, style: this.props.style, className: mergeClasses("ui-menu", { 'ui-menu--show': this.state.show, 'ui-menu--right': this.props.direction === 'right' }, this.props.className) },
            React.createElement("div", { className: "ui-menu__target", onClick: this.showMenu }, this.props.target),
            React.createElement("div", { ref: this.card },
                React.createElement(Card, { onClick: this.hideMenu, style: { width: this.props.width, maxHeight: this.props.height }, className: "ui-menu__card" }, this.props.menu)));
    };
    return Menu;
}(React.PureComponent));
export { Menu };
//# sourceMappingURL=index.js.map