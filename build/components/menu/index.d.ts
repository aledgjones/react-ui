import * as React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    direction: 'left' | 'right';
    target: JSX.Element;
    menu: JSX.Element;
    width?: number;
    height?: number;
}
interface IState {
    show: boolean;
}
export declare class Menu extends React.PureComponent<IProps, IState> {
    private card;
    state: IState;
    private showMenu;
    private catchPointer;
    private hideMenu;
    render(): JSX.Element;
}
export {};
