import React from 'react';
import './styles.css';
interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    src?: string;
    name: string;
    size?: number;
}
export declare function Avatar(props: IProps): JSX.Element;
export {};
