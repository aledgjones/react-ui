import * as React from 'react';
import './styles.css';
export interface IProps {
    id?: string;
    className?: string;
    value: string | undefined;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value: string | undefined) => void;
    onFocus?: () => void;
    onBlur?: () => void;
    images: string[];
    onUpload: (file: File, update: (progress: number) => void) => Promise<void>;
}
export interface IState {
    focus: boolean;
    touched: boolean;
}
export declare class ImagePicker extends React.PureComponent<IProps, IState> {
    state: IState;
    private hasError;
    private showDialog;
    private hideDialog;
    private clear;
    private updateValue;
    render(): JSX.Element;
}
