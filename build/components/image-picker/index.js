var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { mdiClose, mdiImageSearchOutline } from '@mdi/js';
import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { ImagePickerDialog } from '../image-picker-dialog';
import { Image } from '../image';
import './styles.css';
var ImagePicker = /** @class */ (function (_super) {
    __extends(ImagePicker, _super);
    function ImagePicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            focus: false,
            touched: false
        };
        _this.showDialog = function () {
            _this.setState({ focus: true });
            if (_this.props.onFocus) {
                _this.props.onFocus();
            }
        };
        _this.hideDialog = function () {
            _this.setState({ focus: false });
            if (_this.props.onBlur) {
                _this.props.onBlur();
            }
        };
        _this.clear = function () {
            _this.props.onChange(undefined);
        };
        _this.updateValue = function (src) {
            _this.props.onChange(src);
            _this.hideDialog();
        };
        return _this;
    }
    ImagePicker.prototype.hasError = function () {
        return this.props.required && !hasValue(this.props.value);
    };
    ImagePicker.prototype.render = function () {
        return React.createElement(React.Fragment, null,
            React.createElement("div", { className: mergeClasses('ui-image-picker', this.props.className, {
                    'ui-image-picker--disabled': this.props.disabled,
                    'ui-image-picker--has-value': hasValue(this.props.value),
                    'ui-image-picker--invalid': this.hasError(),
                    'ui-image-picker--touched': this.state.touched
                }) },
                React.createElement("div", { className: "ui-image-picker__preview" },
                    hasValue(this.props.value) && React.createElement("div", { className: "ui-image-picker__hatching" }),
                    this.props.value && React.createElement(Image, { src: this.props.value, className: "ui-image-picker__fill" })),
                React.createElement("div", { className: "ui-image-picker__value", onClick: this.showDialog }, this.props.placeholder + (this.props.required ? '*' : '')),
                (hasValue(this.props.value) && !this.props.required) ?
                    React.createElement(IconButton, { className: "ui-image-picker__clear-icon", onClick: this.clear, path: mdiClose, color: "rgb(150,150,150)" }) :
                    React.createElement(IconButton, { className: "ui-image-picker__down-icon", onClick: this.showDialog, path: mdiImageSearchOutline, color: "rgb(150,150,150)" }),
                React.createElement("div", { className: "ui-image-picker__bar" })),
            React.createElement(Dialog, { visible: !!this.state.focus, backdrop: true },
                React.createElement(ImagePickerDialog, { images: this.props.images, onCancel: this.hideDialog, onUpload: this.props.onUpload, onComplete: this.updateValue })));
    };
    return ImagePicker;
}(React.PureComponent));
export { ImagePicker };
//# sourceMappingURL=index.js.map