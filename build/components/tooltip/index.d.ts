import * as React from 'react';
import './styles.css';
export interface IProps {
    text: string;
    id?: string;
    className?: string;
}
export declare class Tooltip extends React.PureComponent<IProps> {
    render(): JSX.Element;
}
