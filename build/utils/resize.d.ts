import * as React from 'react';
interface IProps {
    debug?: boolean;
    onResize: (box: {
        height: number;
        width: number;
    }) => void;
}
export declare class ResizeListener extends React.Component<IProps> {
    private iframe;
    private handleResize;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
export {};
