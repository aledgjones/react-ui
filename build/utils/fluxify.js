var Fluxify = /** @class */ (function () {
    function Fluxify(initialState) {
        this.debug = false;
        this.listeners = [];
        this.data = initialState;
    }
    Fluxify.prototype.enableDebugging = function () {
        this.debug = true;
    };
    Fluxify.prototype.listen = function (listener) {
        var _this = this;
        this.listeners.push(listener);
        return function () {
            _this.listeners = _this.listeners.filter(function (callback) { return callback !== listener; });
        };
    };
    Fluxify.prototype.emit = function (action, data) {
        this.data = data;
        this.listeners.forEach(function (listener) {
            listener(action, data);
        });
        if (this.debug) {
            console.log(action, data);
        }
    };
    Fluxify.prototype.dispatch = function (action, payload) {
        var data = this.reducer(action, payload, this.data);
        this.emit(action, data);
    };
    return Fluxify;
}());
export { Fluxify };
//# sourceMappingURL=fluxify.js.map