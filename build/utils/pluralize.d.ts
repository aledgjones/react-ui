export declare function pluralize(num: number, single: string, plural: string): string;
