export function pluralize(num, single, plural) {
    return num + ' ' + (num === 1 ? single : plural);
}
//# sourceMappingURL=pluralize.js.map