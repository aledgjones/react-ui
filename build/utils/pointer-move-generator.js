export function pointerMoveGenerator(onDown, onMove, onEnd, onCancel) {
    var pointerId;
    var data = {};
    var move = function (e) {
        if (pointerId !== e.pointerId)
            return;
        onMove(e, data);
    };
    var stop = function (e) {
        if (pointerId !== e.pointerId)
            return;
        onEnd(e, data);
        document.removeEventListener('pointermove', move);
        document.removeEventListener('pointerup', stop);
        document.removeEventListener('pointercancel', cancel);
        pointerId = undefined;
    };
    var cancel = function (e) {
        if (pointerId !== e.pointerId)
            return;
        if (onCancel) {
            onCancel(e, data);
        }
        else {
            onEnd(e, data);
        }
        document.removeEventListener('pointermove', move);
        document.removeEventListener('pointerup', stop);
        document.removeEventListener('pointercancel', cancel);
        pointerId = undefined;
    };
    return function (e) {
        if (pointerId && pointerId !== e.pointerId)
            return;
        pointerId = e.pointerId;
        var proceed = onDown(e, data);
        if (proceed) {
            document.addEventListener('pointermove', move);
            document.addEventListener('pointerup', stop);
            document.addEventListener('pointercancel', cancel);
        }
        else {
            // we can reurn false from the onDown func to terminate execution
            pointerId = undefined;
        }
    };
}
//# sourceMappingURL=pointer-move-generator.js.map