import { hasValue } from '.';
// NB: empty values are valid => ui-required forces non empty values
// WILL ALWAYS BE STRING VALUES SO WE CAN CHECK WITH REGEX
export function isInteger(val) {
    var validNumberRegex = /^[+-]?\d+$/;
    if (!hasValue(val) || validNumberRegex.test(val)) {
        return true;
    }
    else {
        return false;
    }
}
//# sourceMappingURL=is-integer.js.map