import { hasValue } from './';
export function isLongEnough(val, length) {
    if (!hasValue(val) || val.length >= length) {
        return true;
    }
    else {
        return false;
    }
}
//# sourceMappingURL=is-long-enough.js.map