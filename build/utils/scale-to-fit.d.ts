import { IDimensions } from "./image";
/**
 * We want to fit the child inside the parent.
 * fixed forces a scale up if the child is smaller than the parent else we keep the natural size
 */
export declare function scaleToFit(child: IDimensions, parent: IDimensions, fixed?: boolean): number;
