import { isNull, isUndefined } from 'lodash';
// tslint:disable-next-line:no-any
export function hasValue(val, emptyStringIsValue) {
    if (emptyStringIsValue === void 0) { emptyStringIsValue = false; }
    if (emptyStringIsValue) {
        return !isUndefined(val) && !isNull(val);
    }
    else {
        return !isUndefined(val) && !isNull(val) && val !== '';
    }
}
//# sourceMappingURL=has-value.js.map