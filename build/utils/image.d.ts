export interface IBox {
    x: number;
    y: number;
    height: number;
    width: number;
}
export interface IDimensions {
    height: number;
    width: number;
}
export interface IPoint {
    x: number;
    y: number;
}
export declare type ICrop = [IPoint, IPoint];
export interface IImage {
    src: string;
    crop: ICrop;
}
export declare function cropChanged(prev: ICrop, next: ICrop): boolean;
/**
 * Gets a html image element from src
 * useful for images dimentions / size / type etc
 */
export declare function getImage(src: string): Promise<HTMLImageElement>;
/**
 * Calc crop area size in px
 * NB. Crop marks are based on a percent of original image
 */
export declare function getImageSize(image: HTMLImageElement, crop?: ICrop): Promise<{
    height: number;
    width: number;
}>;
/**
 * Calc crop area points in px
 * NB. Crop marks are based on a percent of original image
 */
export declare function getCropPoints(image: HTMLImageElement, crop: ICrop): Promise<{
    x: number;
    y: number;
}[]>;
/**
 * generates a cropped version of an image as blob
 * optionally scales to cover box
 */
export declare function getCroppedImage(src: string, crop: ICrop): Promise<Blob>;
