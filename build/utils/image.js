var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
export function cropChanged(prev, next) {
    return prev[0].x !== next[0].x || prev[0].y !== next[0].y || prev[1].x !== next[1].x || prev[1].y !== next[1].y;
}
/**
 * Gets a html image element from src
 * useful for images dimentions / size / type etc
 */
export function getImage(src) {
    var img = document.createElement('img');
    img.crossOrigin = 'Anonymous';
    return new Promise(function (resolve, reject) {
        img.onload = function () {
            resolve(img);
        };
        img.onerror = function () {
            reject('fail');
        };
        img.src = src;
    });
}
/**
 * Calc crop area size in px
 * NB. Crop marks are based on a percent of original image
 */
export function getImageSize(image, crop) {
    return __awaiter(this, void 0, void 0, function () {
        var onePercentWidth, onePercentHeight;
        return __generator(this, function (_a) {
            if (crop) {
                onePercentWidth = image.width / 100;
                onePercentHeight = image.height / 100;
                return [2 /*return*/, {
                        height: (crop[1].y - crop[0].y) * onePercentHeight,
                        width: (crop[1].x - crop[0].x) * onePercentWidth
                    }];
            }
            else {
                return [2 /*return*/, {
                        height: image.height,
                        width: image.width
                    }];
            }
            return [2 /*return*/];
        });
    });
}
/**
 * Calc crop area points in px
 * NB. Crop marks are based on a percent of original image
 */
export function getCropPoints(image, crop) {
    return __awaiter(this, void 0, void 0, function () {
        var onePercentWidth, onePercentHeight;
        return __generator(this, function (_a) {
            onePercentWidth = image.width / 100;
            onePercentHeight = image.height / 100;
            return [2 /*return*/, [
                    {
                        x: crop[0].x * onePercentWidth,
                        y: crop[0].y * onePercentHeight
                    },
                    {
                        x: crop[1].x * onePercentWidth,
                        y: crop[1].y * onePercentHeight
                    }
                ]];
        });
    });
}
/**
 * generates a cropped version of an image as blob
 * optionally scales to cover box
 */
export function getCroppedImage(src, crop) {
    return __awaiter(this, void 0, void 0, function () {
        var image, cropBox, cropPoints, canvas, ctx;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getImage(src)];
                case 1:
                    image = _a.sent();
                    return [4 /*yield*/, getImageSize(image, crop)];
                case 2:
                    cropBox = _a.sent();
                    return [4 /*yield*/, getCropPoints(image, crop)];
                case 3:
                    cropPoints = _a.sent();
                    canvas = document.createElement('canvas');
                    canvas.height = cropBox.height;
                    canvas.width = cropBox.width;
                    canvas.style.position = 'fixed';
                    canvas.style.visibility = 'hidden';
                    document.body.appendChild(canvas);
                    ctx = canvas.getContext('2d');
                    if (ctx) {
                        ctx.drawImage(image, cropPoints[0].x * -1, cropPoints[0].y * -1);
                    }
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            canvas.toBlob(function (blob) {
                                if (blob) {
                                    canvas.remove();
                                    resolve(blob);
                                }
                                else {
                                    canvas.remove();
                                    reject('fail');
                                }
                            }, 'image/png', 0.95);
                        })];
            }
        });
    });
}
//# sourceMappingURL=image.js.map