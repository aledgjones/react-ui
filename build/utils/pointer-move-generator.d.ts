/// <reference types="react" />
declare type DownCallback<Data> = (e: React.PointerEvent<any>, data: Data | {}) => boolean;
declare type OtherCallback<Data> = (e: PointerEvent, data: Data | {}) => void;
export declare function pointerMoveGenerator<Data>(onDown: DownCallback<Data>, onMove: OtherCallback<Data>, onEnd: OtherCallback<Data>, onCancel?: OtherCallback<Data>): (e: import("react").PointerEvent<any>) => void;
export {};
