import { hasValue } from './';
// NB: empty values are valid => ui-required forces non empty values
// WILL ALWAYS BE STRING VALUES SO WE CAN CHECK WITH REGEX
export function isNumber(val) {
    var validNumberRegex = /^[+-]?\d+(\.\d+)?$/;
    if (!hasValue(val) || validNumberRegex.test(val)) {
        return true;
    }
    else {
        return false;
    }
}
//# sourceMappingURL=is-number.js.map