var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
var ResizeListener = /** @class */ (function (_super) {
    __extends(ResizeListener, _super);
    function ResizeListener() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.iframe = React.createRef();
        _this.handleResize = function () {
            if (_this.iframe.current) {
                var height = _this.iframe.current.scrollHeight;
                var width = _this.iframe.current.scrollWidth;
                if (_this.props.debug)
                    console.log('height', height, 'width', width);
                _this.props.onResize({ height: height, width: width });
            }
        };
        return _this;
    }
    ResizeListener.prototype.componentDidMount = function () {
        if (this.iframe.current && this.iframe.current.contentWindow) {
            this.iframe.current.contentWindow.addEventListener("resize", this.handleResize);
            this.handleResize();
        }
    };
    ResizeListener.prototype.componentWillUnmount = function () {
        if (this.iframe.current && this.iframe.current.contentWindow) {
            this.iframe.current.contentWindow.removeEventListener("resize", this.handleResize);
        }
    };
    ResizeListener.prototype.render = function () {
        return (React.createElement("iframe", { title: Math.random().toString(), ref: this.iframe, style: {
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                visibility: this.props.debug ? "visible" : "hidden",
                border: "none",
                outline: "4px dashed crimson",
                height: "100%",
                width: "100%"
            } }));
    };
    return ResizeListener;
}(React.Component));
export { ResizeListener };
//# sourceMappingURL=resize.js.map