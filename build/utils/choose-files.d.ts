export declare function chooseFiles(config?: {
    accept?: string;
    multiple?: boolean;
}): Promise<File[]>;
