export declare function mergeClasses(...args: Array<string | {
    [prop: string]: boolean | undefined;
} | undefined>): string;
