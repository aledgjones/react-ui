export function copy(str) {
    var input = document.createElement('INPUT');
    document.body.appendChild(input);
    input.style.opacity = 0;
    input.value = str;
    input.select();
    try {
        document.execCommand('copy');
        input.remove();
        return Promise.resolve();
    }
    catch (err) {
        input.remove();
        return Promise.reject();
    }
}
//# sourceMappingURL=copy.js.map