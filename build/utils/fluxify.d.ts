export declare type Listener<T> = (action: string, state: T) => void;
export declare type Unlistener = () => void;
export declare abstract class Fluxify<T> {
    data: T;
    debug: boolean;
    listeners: Array<Listener<T>>;
    constructor(initialState: T);
    abstract reducer(action: string, payload: any, state: T): T;
    enableDebugging(): void;
    listen(listener: Listener<T>): Unlistener;
    emit(action: string, data: T): void;
    dispatch(action: string, payload: any): void;
}
