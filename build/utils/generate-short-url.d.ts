/**
 * Generate a shortURL from any valid url
 */
export declare function generateShortUrl(url: string): Promise<string>;
