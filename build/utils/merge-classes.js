import { isObject, isString } from 'lodash';
export function mergeClasses() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var out = args.reduce(function (arr, arg) {
        if (arg === void 0) { arg = ''; }
        if (isString(arg)) {
            var split = arg.split(' ');
            var clean = split.filter(function (val) { return val !== ''; });
            return arr.concat(clean);
        }
        if (isObject(arg)) {
            var keys = Object.keys(arg);
            var clean = keys.filter(function (key) {
                return arg[key];
            });
            return arr.concat(clean);
        }
        return arr;
    }, []);
    return out.join(' ');
}
//# sourceMappingURL=merge-classes.js.map