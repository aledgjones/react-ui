import * as React from 'react';
import { DateTime } from 'luxon';
import { mdiChevronLeft, mdiChevronRight } from '@mdi/js';

import { Card } from '..';
import { Toolbar } from '../toolbar';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { ToolbarLine } from '../toolbar-line';
import { Content } from '../content';
import { Button } from '../button';
import { Divider } from '../divider';
import { mergeClasses } from '../../utils';

import './styles.css';


export interface IProps {
    id?: string;
    className?: string;
    value?: number;
    required?: boolean;
    onCancel: () => void;
    onComplete: (value: number) => void;
}

export interface IState {
    value: DateTime;
    view: DateTime;
}

export class DatePickerDialog extends React.PureComponent<IProps, IState> {

    public state: IState = (() => {
        const dt = this.props.value ? DateTime.fromMillis(this.props.value) : DateTime.local();
        return {
            value: dt,
            view: dt.set({ day: 1 })
        }
    })();

    private updateValue = (year: number, month: number, day: number) => {
        const timestamp = DateTime.local(year, month, day).toMillis();
        this.props.onComplete(timestamp);
    }

    private prev = () => {
        this.setState({ view: this.state.view.minus({ months: 1 }) });
    }

    private next = () => {
        this.setState({ view: this.state.view.plus({ months: 1 }) });
    }

    private buildGrid(view: DateTime, value: DateTime) {
        const now = DateTime.local();
        const grid = new Array(view.weekday - 1).fill(null);
        for (let i = 1; i <= view.daysInMonth; i++) {
            grid.push({
                current: now.hasSame(view, 'year') && now.hasSame(view, 'month') && now.day === i,
                selected: value.hasSame(view, 'year') && value.hasSame(view, 'month') && value.day === i,
                value: i,
                onClick: () => this.updateValue(view.year, view.month, i)
            });
        }
        return grid;
    }

    private cell(cell: { current: boolean, selected: boolean, value: number, onClick: () => void } | null) {
        if (cell) {
            return <div onClick={cell.onClick} className="ui-date-picker-dialog__cell">
                <div className={mergeClasses("ui-date-picker-dialog__cell-value", { 'ui-date-picker-dialog__cell-value--selected': cell.selected, 'ui-date-picker-dialog__cell-value--current': cell.current })}>
                    <div className="ui-date-picker-dialog__cell-digit">{cell.value}</div>
                </div>
            </div>
        } else {
            return <div className="ui-date-picker-dialog__cell" />
        }
    }

    public render() {

        const grid = this.buildGrid(this.state.view, this.state.value);

        return <Card className="ui-dialog__card ui-date-picker-dialog__card">
            <Toolbar>
                <ToolbarLine>
                    <Title>{this.state.view.toLocaleString({ month: 'long' })} {this.state.view.year}</Title>
                    <IconButton className="ui-icon-button--margin" onClick={this.prev} path={mdiChevronLeft} />
                    <IconButton onClick={this.next} path={mdiChevronRight} />
                </ToolbarLine>
            </Toolbar>
            <div className="ui-date-picker-dialog__header">
                <div className="ui-date-picker-dialog__label">M</div>
                <div className="ui-date-picker-dialog__label">T</div>
                <div className="ui-date-picker-dialog__label">W</div>
                <div className="ui-date-picker-dialog__label">T</div>
                <div className="ui-date-picker-dialog__label">F</div>
                <div className="ui-date-picker-dialog__label">S</div>
                <div className="ui-date-picker-dialog__label">S</div>
            </div>
            <Divider />
            <div className="ui-date-picker-dialog__grid">{grid.map(this.cell)}</div>
            <Content className="ui-content--buttons ui-content--compact">
                <Button className="ui-button--compact" onClick={this.props.onCancel}>Cancel</Button>
            </Content>
        </Card>
    }
}