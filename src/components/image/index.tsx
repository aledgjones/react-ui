import React, { useState, useEffect } from 'react';
import { mergeClasses, getCroppedImage, ICrop } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    src: string;
    crop?: ICrop;
    wait?: boolean;
    onClick?: () => void;
}

export function Image(props: IProps) {

    const { id, className, style, src, crop, wait, onClick } = props;

    const [waiting, setWaiting] = useState<boolean>(true); // defer loading;
    const [loading, setLoading] = useState<boolean>(true);
    const [url, setUrl] = useState<string | undefined>(undefined);

    useEffect(() => {
        if (waiting && !wait) {
            setWaiting(false);
        }
    }, [waiting, wait]);

    useEffect(() => {

        const load = async (src: string, crop: ICrop) => {
            setLoading(true);
            const link = await getCroppedImage(src, crop);
            setUrl(link);
            setLoading(false);
        }

        if (!waiting) {
            const _crop = crop || [{ x: 0, y: 0 }, { x: 100, y: 100 }];
            if (src) {
                load(src, _crop);
            } else {
                setUrl(undefined);
            }
        }

        return () => {
            if (url) {
                URL.revokeObjectURL(url);
            }
        }

    }, [src, crop]);

    return <div
        id={id}
        className={mergeClasses('ui-image', className)}
        style={style}
        onClick={onClick}
    >
        {(src && !loading) && <div className="ui-image__paint" style={{ backgroundImage: `url(${src})` }} />}
    </div>
}