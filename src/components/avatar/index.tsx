import React, { useState, useEffect } from 'react';
import { colors, mergeClasses, textColor } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    src?: string;
    name: string;
    size?: number;
}

export function Avatar(props: IProps) {

    const { id, className, style, src, name, size } = props;
    const letter = name.slice(0, 1).toUpperCase();

    const [isImageValid, setIsImageValid] = useState<boolean>();
    const [styles, setStyles] = useState<{ [style: string]: any }>();

    useEffect(() => {
        const diameter = size || 40;
        const background = colors[letter] || '#ffffff';
        setStyles({
            height: diameter,
            width: diameter,
            fontSize: diameter * .6,
            color: textColor(background),
            backgroundColor: background
        });
    }, [letter, size]);

    useEffect(() => {
        let didCancel = false;

        async function checkImageExists(src: string) {
            setIsImageValid(false);
            try {
                await fetch(src, { mode: 'no-cors' });
                if (!didCancel) {
                    setIsImageValid(true);
                }
            } catch {
                // no catch as isImageValid is already false;
            }
        }

        if (src) {
            checkImageExists(src);
        }

        return () => { didCancel = true };
    }, [src]);

    return <div
        id={id}
        className={mergeClasses('ui-avatar', className)}
        style={{
            ...styles, // calc from props
            backgroundImage: isImageValid ? `url(${src})` : undefined,
            ...style // prop styles to allow overwrites
        }}
    >
        {!isImageValid && <span className="ui-avatar__letter">{letter}</span>}
    </div>

}