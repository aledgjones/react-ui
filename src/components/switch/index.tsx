// tslint:disable:no-any

import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    value: boolean;
    disabled?: boolean;
    onChange?: (val: boolean) => void;
}

export class Switch extends React.PureComponent<IProps> {

    public onClick = () => this.props.onChange && this.props.onChange(!this.props.value);

    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses(
                    'ui-switch',
                    this.props.className,
                    { 'ui-switch--active': this.props.value, 'ui-switch--disabled': this.props.disabled }
                )}
                onClick={this.onClick}
            >
                <div className="ui-switch__track" />
                <div className="ui-switch__button">
                    <Ink center={true} />
                </div>
            </div>
        );
    }
}