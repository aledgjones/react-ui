import * as React from 'react';

import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    path?: string;
    size?: number;
    color?: string;
}

export class Icon extends React.PureComponent<IProps> {
    public render() {

        const size = this.props.size || 24;
        const color = this.props.color || 'rgba(0,0,0,.87)';

        return (
            <svg
                viewBox="0 0 24 24"
                id={this.props.id}
                className={mergeClasses('ui-icon', this.props.className)}
                style={{ width: size, height: size, ...this.props.style }}
            >
                <path
                    d={this.props.path}
                    style={{ fill: color }}
                />
            </svg>
        );
    }
}