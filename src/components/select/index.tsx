import * as React from 'react';
import { Card, IconButton, List, ListItem } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { mdiClose, mdiChevronDown } from '@mdi/js';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    value: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value: any) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}

export interface IState {
    focus: boolean;
    touched: boolean;
    value: string;
}

export class Select extends React.PureComponent<IProps, IState> {

    public state: IState = { focus: false, touched: false, value: '' };

    public hasError() {
        return this.props.required && !hasValue(this.props.value);
    }

    public close = () => {
        document.removeEventListener('click', this.close);
        this.setState({ focus: false });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    public open = () => {
        document.addEventListener('click', this.close);
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    public toggle = () => {
        // the document cick listener will close the dialog if open
        if (!this.state.focus) {
            this.open();
        }
    }

    public clear = () => {
        if (this.props.onChange) { this.props.onChange(null); };
    }

    public onChange(value: any, displayAs: string) {
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    public parseIncomingValue(props: any) {
        if (hasValue(props.value)) {
            React.Children.forEach(props.children, (child: any) => {
                if (child.props.value === props.value) {
                    this.setState({ value: child.props.displayAs });
                }
            });
        } else {
            this.setState({ value: '' });
        }
    }

    public componentDidMount() {
        this.parseIncomingValue(this.props);
    }

    public componentWillReceiveProps(props: any) {
        if (this.props.value !== props.value) {
            this.parseIncomingValue(props);
        }
    }

    public render() {

        return (
            <div className={mergeClasses('ui-select', this.props.className, {
                'ui-select--disabled': this.props.disabled,
                'ui-select--focus': this.state.focus,
                'ui-select--has-value': hasValue(this.props.value),
                'ui-select--invalid': this.hasError(),
                'ui-select--touched': this.state.touched
            })}>

                <div className="ui-select__placeholder">{this.props.placeholder + (this.props.required ? '*' : '')}</div>

                <div className="ui-select__value" onClick={this.toggle}>{this.state.value}</div>

                {(hasValue(this.props.value) && !this.props.required) && <IconButton className="ui-select__clear-icon" onClick={this.clear} path={mdiClose} color="rgb(150,150,150)" />}

                {(!hasValue(this.props.value) || !!this.props.required) && <IconButton className="ui-select__down-icon" onClick={this.toggle} path={mdiChevronDown} color="rgb(150,150,150)" />}

                <div className="ui-select__bar" />

                {
                    !!this.state.focus &&
                    <Card className="ui-select__card">
                        <List>
                            {
                                React.Children.map(this.props.children, (child: any) => {
                                    return <ListItem key={child.props.value} onClick={() => this.onChange(child.props.value, child.props.displayAs)}>{child}</ListItem>
                                })
                            }
                        </List>
                    </Card>
                }
            </div >
        )
    }
}