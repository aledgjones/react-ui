import * as React from 'react';
import { hasValue, mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value?: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange?: (value: string) => void;
    onEnter?: () => void;
    onFocus?: () => void;
    onBlur?: () => void;
}

interface IState {
    focus: boolean;
    touched: boolean;
    height: number;
}

export class Textarea extends React.PureComponent<IProps, IState> {

    private textarea = React.createRef<HTMLDivElement>();
    public state: IState = { focus: false, touched: false, height: 0 };

    public onFocus = () => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    public onBlur = () => {
        this.setState({ focus: false, touched: true });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    public onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => this.props.onChange && this.props.onChange(e.target.value);

    public onEnter = (e: React.KeyboardEvent<HTMLTextAreaElement>) => e.which === 13 && this.props.onEnter && this.props.onEnter();

    public onClear = () => this.props.onChange && this.props.onChange('');

    public getError() {
        if (this.props.required && !hasValue(this.props.value)) {
            return 'Required';
        }
        return null;
    }

    public componentDidUpdate(prev: IProps) {
        if (this.props.value !== prev.value) {

            const split = this.props.value.split('\n');
            this.setState({
                height: this.textarea.current ? this.textarea.current.clientHeight + (split.length > 2 && !split[split.length - 1] ? 16 : 0) : 0
            });
        }
    }

    public render() {

        const error = this.getError();

        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-textarea', this.props.className, {
                    'ui-textarea--disabled': this.props.disabled,
                    'ui-textarea--focus': this.state.focus,
                    'ui-textarea--has-value': hasValue(this.props.value),
                    'ui-textarea--invalid': !!error,
                    'ui-textarea--touched': this.state.touched
                })}
                style={this.props.style}
            >
                <div className="ui-textarea__wrapper">
                    <p className="ui-textarea__placeholder">
                        <span>{this.props.placeholder}</span>
                        {!!this.props.required && <span>*</span>}
                    </p>

                    <textarea
                        value={this.props.value}
                        onFocus={this.onFocus}
                        onBlur={this.onBlur}
                        onChange={this.onChange}
                        onKeyUp={this.onEnter}
                        disabled={this.props.disabled}
                        style={{
                            height: this.state.height
                        }}
                    />

                    <div ref={this.textarea} className="ui-textarea--slave">{this.props.value}</div>

                    <div className="ui-textarea__bar" />

                    {(this.state.touched && !!error) && <div className="ui-textarea__error">{error}</div>}

                </div>
            </div>
        );
    }
}