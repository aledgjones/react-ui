import { Fluxify } from "../../utils/fluxify";

// tslint:disable:no-any
// tslint:disable:align

const OPEN_DRAWER = '@drawer/open';
const CLOSE_DRAWER = '@drawer/close';

class Drawer extends Fluxify<string[]> {

    public reducer(action: string, payload: any, state: string[]) {
        switch (action) {
            case OPEN_DRAWER:
                return state.indexOf(payload) === -1 ? [...state, payload] : state;
            case CLOSE_DRAWER:
                return state.filter(id => id !== payload);
            default:
                return state;
        }
    }

    public show(id: string) {
        this.dispatch(OPEN_DRAWER, id);
    }

    public hide(id: string) {
        this.dispatch(CLOSE_DRAWER, id);
    }
}

export const drawer = new Drawer([]);