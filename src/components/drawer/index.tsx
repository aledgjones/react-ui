import * as React from 'react';
import { mergeClasses } from '../../utils';
import { Unlistener } from '../../utils/fluxify';
import { drawer } from './store';

import './styles.css';

interface IProps {
    id: string;
    className?: string;
    disabled?: boolean;
}

interface IState {
    open: boolean;
    transition: boolean;
    offset: number;
}

export class Drawer extends React.PureComponent<IProps, IState> {

    public sub: Unlistener;
    public startX: number;

    public state = { open: drawer.data.indexOf(this.props.id) > -1, offset: 0, transition: true };

    public start = (e: any) => {
        if (!this.props.disabled && e.pointerType === 'touch') {
            const left = e.screenX;
            if (!this.state.open && left < 5) {
                this.setState({ transition: false });
                this.startX = left;
                addEventListener('pointermove', this.move);
                addEventListener('pointerup', this.end);
            }
        }
    }

    public move = (e: any) => {
        this.setState({ offset: e.screenX - this.startX });
    }

    public end = () => {

        const offset = this.state.offset;

        this.startX = 0;
        this.setState({ offset: 0, transition: true });

        removeEventListener('pointermove', this.move);
        removeEventListener('pointerup', this.end);

        if (offset > 64) {
            drawer.show(this.props.id);
        }

    }

    public componentDidMount() {
        this.sub = drawer.listen((action, state) => {
            this.setState({ open: state.indexOf(this.props.id) > -1 });
        });
        document.addEventListener('pointerdown', this.start)
    }

    public componentWillUnmount() {
        this.sub();
        document.removeEventListener('pointerdown', this.start);
    }

    public get drawerStyles() {
        if (this.state.offset > 360) {
            return {
                transform: `translate3d(0px, 0, 0)`
            }
        }
        if (this.state.offset > 0) {
            return {
                transform: `translate3d(${this.state.offset - 360}px, 0, 0)`
            }
        }
        return {};
    }

    public get backdropStyles() {
        if (this.state.offset > 360) {
            return {
                opacity: 1
            }
        }
        if (this.state.offset > 0) {
            return {
                opacity: this.state.offset / 360
            }
        }
        return {
            opacity: 0
        };
    }

    public render() {
        return (
            <>
                <div style={this.drawerStyles} className={mergeClasses(this.props.className, 'ui-drawer', { 'ui-drawer--open': this.state.open, 'ui-drawer--transition': this.state.transition })}>
                    {this.props.children}
                </div>
                {/* tslint:disable-next-line:jsx-no-lambda */}
                <div style={this.backdropStyles} className={mergeClasses('ui-drawer__backdrop', { 'ui-drawer__backdrop--visible': this.state.open, 'ui-drawer__backdrop--transition': this.state.transition })} onClick={() => drawer.hide(this.props.id)} />
            </>
        );
    }
}