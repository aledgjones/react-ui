import { Fluxify } from "../../utils/fluxify";

export interface IToastConfig {
    text: string;
    button?: string;
    onClick?: () => void;
    onTimeout?: () => void;
}

export interface IToastInstance {
    key: number;
    text: string;
    button?: string;
    onClick?: () => void;
    onTimeout?: () => void;
    hide?: boolean;
    timeout: any;
}

const ADD_TOAST = '@toast/add';
const HIDE_TOAST = '@toast/hide';
const DESTROY_TOAST = '@toast/destroy';

export class Toast extends Fluxify<IToastInstance[]> {

    public reducer(action: string, payload: any, state: IToastInstance[]) {
        switch (action) {
            case ADD_TOAST:
                return [...state, payload];
            case HIDE_TOAST:
                return state.map(instance => {
                    if (instance.key === payload) {
                        clearTimeout(instance.timeout);
                        instance.hide = true;
                        return instance;
                    } else {
                        return instance;
                    }
                });
            case DESTROY_TOAST:
                return state.filter(instance => instance.key !== payload);
            default:
                return state;
        }
    }

    public show(config: IToastConfig, duration: number = 5000) {
        this.cleanup();
        const key = Date.now();
        const instance = {
            key,
            ...config,
            hide: false,
            timeout: setTimeout(() => {
                this.triggerOnTimeout(key);
            }, duration)
        };
        this.dispatch(ADD_TOAST, instance);
    }

    public triggerOnClick(key: number) {
        const instance = this.getInstanceFromKey(key);
        if (instance && instance.onClick) {
            instance.onClick();
        }
        this.hide(key);
    }

    private getInstanceFromKey(key: number) {
        return this.data.find(instance => instance.key === key);
    }

    private triggerOnTimeout(key: number) {
        const instance = this.getInstanceFromKey(key);
        if (instance && instance.onTimeout) {
            instance.onTimeout();
        }
        this.hide(key);
    }

    private hide(key: number) {
        this.dispatch(HIDE_TOAST, key);
        setTimeout(() => {
            this.destroy(key);
        }, 1000);
    }

    private destroy(key: number) {
        this.dispatch(DESTROY_TOAST, key);
    }

    private cleanup() {
        this.data.filter(instance => !instance.hide)
            .forEach(instance => {
                this.triggerOnTimeout(instance.key);
            });
    }
}

export const toast = new Toast([]);