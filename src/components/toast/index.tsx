import * as React from 'react';

import { Button } from '..';
import { Unlistener } from '../../utils/fluxify';
import { IToastInstance, toast } from './store';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IState {
    toasts: IToastInstance[];
}

export class Toast extends React.PureComponent {

    public sub: Unlistener;
    public state: IState = { toasts: [] };

    public componentWillMount() {
        this.setState({ toasts: toast.data });
    }

    public componentDidMount() {
        this.sub = toast.listen((action, data) => {
            this.setState({ toasts: data });
        });
    }

    public componentWillUnmount() {
        this.sub();
    }

    public triggerOnClick = (key: number) => toast.triggerOnClick(key);

    public render() {
        return (
            <>
                {this.state.toasts.map((config: IToastInstance) => {
                    return (
                        <div key={config.key} className={mergeClasses('ui-toast', { 'ui-toast--hide': config.hide })}>
                            <span className="ui-toast__text">{config.text}</span>
                            {!!config.button && <Button className="ui-button--compact" onClick={() => this.triggerOnClick(config.key)}>{config.button}</Button>}
                        </div>
                    );
                })}
            </>
        );
    }
}