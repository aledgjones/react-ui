import { isString, isFunction } from 'lodash';

export type Callback = () => void;
export type Unsubscriber = () => void;
export type Redirect = ((path: Path, params: Params, data: Data) => Promise<string | null>) | ((path: Path, params: Params, data: Data) => string | null) | string | null;
export type ComponentDef = ((path: Path, params: Params, data: Data) => JSX.Element) | JSX.Element;

type Path = string;
type Component = JSX.Element;
type Params = { [name: string]: string };
type Data = { [name: string]: string };

export interface IRouterConfig {
    [url: string]: {
        component?: ComponentDef;
        redirect?: Redirect;
    };
}

export interface IRouteConfig {
    path: string[];
    component?: ComponentDef;
    redirect?: Redirect;
}

export class Router {

    public static route404 = '@@router-404';

    private _debug: boolean = false;
    private _subs: Callback[] = [];
    private _instructions: IRouteConfig[] = [];

    public path: Path;
    public component: Component;
    public params: Params;
    public data: Data;

    public enableDebugging() {
        this._debug = true;
    }

    public subscribe(callback: Callback): Unsubscriber {
        this._subs.push(callback);
        callback();
        return () => {
            this._subs = this._subs.filter((sub: Callback) => sub !== callback);
        }
    }

    public dispatch() {
        this._subs.forEach(callback => callback());
        if (this._debug) {
            console.log(this.path, this.component, this.params, this.data);
        }
    }

    public config(conf: IRouterConfig) {
        const keys = Object.keys(conf);
        this._instructions = keys.map(key => {
            return {
                path: this.urlToParts(key),
                component: conf[key].component,
                redirect: conf[key].redirect
            };
        });
    }

    /**
     * navigate to a given url
     */
    public async navigate(path: Path, data: Data = {}): Promise<void> {

        const config = this.match(path);

        if (config) {

            const params = this.parseParams(path, config.path);
            const redirectTo = await this.conformRedirect(config.redirect, path, params, data);

            if (redirectTo) {
                return this.navigate(redirectTo, data);
            } else {
                if (config.component) {
                    this.path = path;
                    this.params = params;
                    this.data = data;
                    if (isFunction(config.component)) {
                        this.component = config.component(path, params, data);
                    } else {
                        this.component = config.component;
                    }
                    this.dispatch();
                    window.history.pushState('', '', path + this.encodeData(data));
                    window.scrollTo({ left: 0, top: 0 });
                }
                return;
            }

        } else {
            return this.navigate(Router.route404, data);
        }
    }

    private urlToParts(url: string) {
        const cleanup = url.replace(/^\/+/g, '');
        return cleanup.split('/');
    }

    private parseParams(url: string, configParts: string[]) {
        const parts = this.urlToParts(url);
        return configParts.reduce((params: {}, part: string, i: number) => {
            if (part.substr(0, 1) === ':') {
                params[part.substr(1)] = parts[i];
                return params;
            } else {
                return params;
            }
        }, {});
    }


    private encodeData(data: { [key: string]: string }): string {
        const keys = Object.keys(data);
        if (keys.length) {
            const params = keys.map(key => {
                return key + '=' + encodeURIComponent(data[key]);
            }, '');
            return '?' + params.join('&');
        } else {
            return '';
        }
    }

    public decodeData(data: string): { [key: string]: string } {
        if (data) {
            const arr = data.substr(1).split('&');
            return arr.reduce((obj, pair) => {
                const couple = pair.split('=');
                return {
                    [couple[0]]: decodeURIComponent(couple[1]),
                    ...obj
                };
            }, {});
        } else {
            return {};
        }
    }

    private match(url: string) {

        const parts = this.urlToParts(url);

        let configs = this._instructions.filter(entry => entry.path.length === parts.length);
        for (let i = 0; i < parts.length; i++) {
            configs = configs.filter(entry => parts[i] === entry.path[i] || entry.path[i].substr(0, 1) === ':');
        }

        if (configs.length === 0) {
            console.warn('[ROUTER] no instruction match "' + url + '"');
            return null;
        }

        if (configs.length > 1) {
            console.warn('[ROUTER] multiple instruction match "' + url + '"');
        }

        return configs[0];

    }

    private async conformRedirect(redirect: Redirect = null, path: Path, params: Params, data: Data): Promise<(string | null)> {
        if (redirect === null || isString(redirect)) {
            return redirect;
        }
        return await redirect(path, params, data);
    }

}

export const router = new Router();