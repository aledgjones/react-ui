import * as React from 'react';
import { router, Unsubscriber } from './store';

interface IState {
    component?: JSX.Element | null;
}

export class RouterOutlet extends React.PureComponent {

    public state: IState = { component: router.component };
    public sub: Unsubscriber;

    public componentDidMount() {
        this.sub = router.subscribe(() => {
            this.setState({ component: router.component });
        });
    }

    public componentWillUnmount() {
        this.sub();
    }

    public render() {
        return this.state.component || null;
    }
}