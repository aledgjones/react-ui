import * as React from 'react';
import { mdiAccountPlus, mdiMagnify, mdiClose } from '@mdi/js';

import { Content } from '../content';
import { Button } from '../button';
import { IContact } from '../contact-picker';
import { Icon } from '../icon';
import { List } from '../list';
import { ListItem } from '../list-item';
import { Label } from '../label';
import { Avatar } from '../avatar';
import { IconButton } from '../icon-button';
import { Toolbar } from '../toolbar';
import { ToolbarLine } from '../toolbar-line';
import { hasValue } from '../../utils';
import { Divider } from '../divider';
import { Tab } from '../contact-picker-dialog';

import './styles.css';

interface IState {
    search: string;
}

interface IProps {
    contacts: IContact[];
    onCancel: () => void;
    onSelect: (value: IContact) => void;
    onNavigate: (tab: Tab) => void;
}

export class ContactPickerDialogList extends React.PureComponent<IProps> {

    public state: IState = { search: '' };

    private clearSearch = () => this.setState({ search: '' });
    private updateSearch = (e: React.ChangeEvent<HTMLInputElement>) => this.setState({ search: e.target.value });
    private createContact = () => this.props.onNavigate(Tab.CREATE);

    public render() {

        const term = this.state.search.toLocaleUpperCase();
        const contacts = hasValue(this.state.search) ? this.props.contacts.filter(contact => contact.name.toLocaleUpperCase().indexOf(term) > -1) : this.props.contacts;

        return <>
            <Toolbar>
                <ToolbarLine>
                    <Icon color="rgb(150,150,150)" path={mdiMagnify} />
                    <input autoFocus className="ui-contact-picker-dialog-list__search" type="text" value={this.state.search} onChange={this.updateSearch} />
                    {!hasValue(this.state.search) && <IconButton className="ui-contact-picker-dialog-list__clear" onClick={this.createContact} path={mdiAccountPlus} />}
                    {hasValue(this.state.search) && <IconButton color="rgb(150,150,150)" className="ui-contact-picker-dialog-list__clear" onClick={this.clearSearch} path={mdiClose} />}
                </ToolbarLine>
            </Toolbar>
            <Divider className="ui-divider--no-margin" />
            <List className="ui-contact-picker-dialog-list__content">
                {
                    contacts.map(contact => {
                        return <ListItem key={contact.key} onClick={() => this.props.onSelect(contact)}>
                            <Avatar name={contact.name} src={contact.avatar} />
                            <Label>
                                <p>{contact.name}</p>
                                {contact.email && <p>{contact.email}</p>}
                            </Label>
                            {/* <IconButton path={mdiAccountEdit} /> */}
                        </ListItem>
                    })
                }
            </List>
            <Divider className="ui-divider--no-margin" />
            <Content className="ui-contact-picker-dialog-list__buttons ui-content--buttons ui-content--compact">
                <Button className="ui-button--compact" onClick={this.props.onCancel}>Cancel</Button>
            </Content>
        </>
    }
}