// tslint:disable:no-any

import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
}

export class Title extends React.PureComponent<IProps> {
    public render() {
        return (
            <h1
                id={this.props.id}
                className={mergeClasses('ui-title', this.props.className)}
            >
                {this.props.children}
            </h1>
        );
    }
}