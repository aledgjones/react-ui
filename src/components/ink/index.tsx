// tslint:disable:align

import * as React from 'react';

import { mergeClasses } from '../../utils';

import './styles.css';

export interface InkBlob {
    key: number;
    x: number;
    y: number;
    diameter: number;
    fire?: boolean;
    timeout?: any;
}

export interface IProps {
    id?: string;
    className?: string;
    diameter?: number;
    cover?: boolean;
    center?: boolean;
}

export interface IState {
    blobs: InkBlob[];
}

export class Ink extends React.PureComponent<IProps, IState> {

    public timeouts: any[] = [];

    public state: IState = { blobs: [] };

    public componentWillUnmount() {
        this.state.blobs.forEach(blob => {
            clearTimeout(blob.timeout);
        });
    }

    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-ink', this.props.className)}
                onPointerDown={this.create}
            >
                {this.state.blobs.map(blob => {
                    return (
                        <div
                            key={blob.key}
                            className={mergeClasses('ui-ink__blob', { 'ui-ink__blob--fire': blob.fire })}
                            style={{ height: blob.diameter, width: blob.diameter, top: blob.y, left: blob.x }}
                        />
                    );
                })}
            </div>
        );
    }

    private calcDiameter(width: number, height: number, x: number, y: number) {
        if (this.props.cover) {
            if (this.props.center) {
                return Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)) / 2;
            } else {

                const corners = [
                    { a: x, b: y },
                    { a: width - x, b: y },
                    { a: width - x, b: height - y },
                    { a: x, b: height - y }
                ];

                const radius = corners.reduce((max, corner) => {
                    const distance = Math.sqrt(Math.pow(corner.a, 2) + Math.pow(corner.b, 2));
                    return distance > max ? distance : max;
                }, 0);

                return radius;

            }
        } else {
            return (this.props.diameter || 48) / 2;
        }
    }

    private destroy(key: number) {
        const blobs = this.state.blobs.filter(blob => blob.key !== key);
        this.setState({ blobs });
    }

    private fire(key: number) {
        const blobs = this.state.blobs.map(blob => {
            if (blob.key === key) {
                blob.fire = true;
                blob.timeout = setTimeout(() => {
                    this.destroy(key);
                }, 1000);
                return blob;
            } else {
                return blob;
            }
        });
        this.setState({ blobs });
    }

    private catchFire(key: number) {
        const listen = () => {
            this.fire(key);
            document.removeEventListener('pointerup', listen);
            document.removeEventListener('pointercancel', listen);
        };
        document.addEventListener('pointerup', listen);
        document.addEventListener('pointercancel', listen);
    }

    private create = (e: any) => {
        const key = Date.now();
        const rec = e.currentTarget.getBoundingClientRect();
        const blobs = [
            ...this.state.blobs,
            {
                diameter: this.calcDiameter(rec.width, rec.height, e.clientX - rec.left, e.clientY - rec.top),
                key,
                x: this.props.center ? rec.width / 2 : e.clientX - rec.left,
                y: this.props.center ? rec.height / 2 : e.clientY - rec.top,
            }
        ];
        this.setState({ blobs });
        this.catchFire(key);
    }
}