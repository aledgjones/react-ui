import * as React from 'react';

import { mergeClasses } from '../../utils';
import { Card } from '../card';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    direction: 'left' | 'right';
    target: JSX.Element;
    menu: JSX.Element;
    width?: number;
    height?: number;
}

interface IState {
    show: boolean;
}

export class Menu extends React.PureComponent<IProps, IState> {

    private card: React.RefObject<HTMLDivElement> = React.createRef();

    public state: IState = { show: false };

    private showMenu = () => {
        this.setState({ show: true });
        document.addEventListener('pointerdown', this.catchPointer);
    }

    private catchPointer = (e: any) => {
        if (e.target && this.card.current && !this.card.current.contains(e.target)) {
            this.hideMenu();
        }
    }

    private hideMenu = () => {
        this.setState({ show: false });
        document.removeEventListener('pointerdown', this.catchPointer);
    }

    public render() {
        return <div id={this.props.id} style={this.props.style} className={mergeClasses("ui-menu", { 'ui-menu--show': this.state.show, 'ui-menu--right': this.props.direction === 'right' }, this.props.className)}>
            <div className="ui-menu__target" onClick={this.showMenu}>{this.props.target}</div>
            <div ref={this.card}>
                <Card onClick={this.hideMenu} style={{ width: this.props.width, maxHeight: this.props.height }} className="ui-menu__card">{this.props.menu}</Card>
            </div>
        </div>
    }
}