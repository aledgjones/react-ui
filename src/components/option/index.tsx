import * as React from 'react';

interface IProps {
    value: any;
    displayAs: string;
}

export class Option extends React.PureComponent<IProps> {
    public render() {
        return this.props.children;
    }
}