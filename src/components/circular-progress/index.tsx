import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
}

export class CircularProgress extends React.PureComponent<IProps> {
    public render() {
        return (
            <div id={this.props.id} className={mergeClasses('ui-circular-progress', this.props.className)}>
                <svg className="ui-circular-progress__box" viewBox="25 25 50 50">
                    <circle className="ui-circular-progress__path" cx="50" cy="50" r="20" />
                </svg>
            </div>
        );
    }

}
