// tslint:disable:no-any

import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    onClick?: () => void;
}

export class Label extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-label', this.props.className)}
                onClick={this.props.onClick}
            >
                {this.props.children}
            </div>
        );
    }
}