// tslint:disable:no-any

import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
}

export class Subheader extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-subheader', this.props.className)}
                style={this.props.style}
            >
                {this.props.children}
            </div>
        );
    }
}