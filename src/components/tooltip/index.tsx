import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

export interface IProps {
    text: string;

    id?: string;
    className?: string;
}

export class Tooltip extends React.PureComponent<IProps> {
    public render() {
        return (
            <span
                id={this.props.id}
                className={mergeClasses('ui-tooltip', this.props.className)}
            >
                {this.props.children}
                <p className="ui-tooltip__content">{this.props.text}</p>
            </span>
        );
    }
}