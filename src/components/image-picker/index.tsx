import * as React from 'react';
import { mdiClose, mdiImageSearchOutline } from '@mdi/js';

import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { ImagePickerDialog } from '../image-picker-dialog';
import { Image } from '../image';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    value: string | undefined;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value: string | undefined) => void;
    onFocus?: () => void;
    onBlur?: () => void;

    // we need this to be as generic as possible so when an image is added
    // we delegate the actual storing to the host application
    images: string[];
    onUpload: (file: File, update: (progress: number) => void) => Promise<void>;
}

export interface IState {
    focus: boolean;
    touched: boolean;
}

export class ImagePicker extends React.PureComponent<IProps, IState> {

    public state: IState = {
        focus: false,
        touched: false
    };

    private hasError() {
        return this.props.required && !hasValue(this.props.value);
    }

    private showDialog = () => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    private hideDialog = () => {
        this.setState({ focus: false });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    private clear = () => {
        this.props.onChange(undefined);
    }

    private updateValue = (src: string) => {
        this.props.onChange(src);
        this.hideDialog();
    }

    public render() {

        return <>
            <div className={mergeClasses('ui-image-picker', this.props.className, {
                'ui-image-picker--disabled': this.props.disabled,
                'ui-image-picker--has-value': hasValue(this.props.value),
                'ui-image-picker--invalid': this.hasError(),
                'ui-image-picker--touched': this.state.touched
            })}>

                <div className="ui-image-picker__preview">
                    {hasValue(this.props.value) && <div className="ui-image-picker__hatching" />}
                    {this.props.value && <Image src={this.props.value} className="ui-image-picker__fill" />}
                </div>

                <div className="ui-image-picker__value" onClick={this.showDialog}>{this.props.placeholder + (this.props.required ? '*' : '')}</div>

                {(hasValue(this.props.value) && !this.props.required) ?
                    <IconButton className="ui-image-picker__clear-icon" onClick={this.clear} path={mdiClose} color="rgb(150,150,150)" /> :
                    <IconButton className="ui-image-picker__down-icon" onClick={this.showDialog} path={mdiImageSearchOutline} color="rgb(150,150,150)" />
                }

                <div className="ui-image-picker__bar" />

            </div >

            {
                <Dialog visible={!!this.state.focus} backdrop={true}>
                    <ImagePickerDialog images={this.props.images} onCancel={this.hideDialog} onUpload={this.props.onUpload} onComplete={this.updateValue} />
                </Dialog>
            }
        </>
    }
}