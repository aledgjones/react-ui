import * as React from 'react';
import { Ink } from '..';
import { mergeClasses, pointerMoveGenerator } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    value: number;
    max: number;
    color?: string;
    disabled?: boolean;
    onChange: (val: number) => void;
}

interface IState {
    active: boolean;
}

interface ISlideData {
    left: number;
    width: number;
}

export class Slider extends React.PureComponent<IProps, IState> {

    public state: IState = { active: false };

    public setValue = (e: React.MouseEvent<HTMLDivElement>) => {
        const box = e.currentTarget.getBoundingClientRect();
        let left = e.clientX - box.left;
        left = left < 0 ? 0 : left;
        left = left > box.width ? box.width : left;
        const value = Math.round((left / box.width) * this.props.max);
        return this.props.onChange(value);
    }

    private wheel = (e: React.WheelEvent<HTMLDivElement>) => {
        if (e.deltaY > 0 && this.props.value < this.props.max) {
            return this.props.onChange(this.props.value + 1);
        }
        if (e.deltaY < 0 && this.props.value > 0) {
            return this.props.onChange(this.props.value - 1);
        }
    }

    public onPointerDown = pointerMoveGenerator<ISlideData>(
        (e: React.PointerEvent<HTMLDivElement>, data: ISlideData) => {
            const parent = e.currentTarget.closest('.ui-slider');
            if (parent) {
                this.setState({ active: true });
                const box = parent.getBoundingClientRect();
                data.left = box.left;
                data.width = box.width;
                return true;
            } else {
                return false;
            }
        },
        (event: PointerEvent, data: ISlideData) => {
            let left = event.clientX - data.left;
            left = left < 0 ? 0 : left;
            left = left > data.width ? data.width : left;
            const value = Math.round((left / data.width) * this.props.max);

            // only fire if the value has actually changed
            if (value !== this.props.value) {
                this.props.onChange(value);
            }
        },
        () => this.setState({ active: false })
    )

    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-slider', this.props.className, {
                    'ui-slider--disabled': this.props.disabled,
                    'ui-slider--is-zero': this.props.value === 0,
                    'ui-slider--active': this.state.active
                })}
                onClick={this.setValue}
                onWheel={this.wheel}
            >
                <div
                    className="ui-slider__track"
                    style={{
                        left: `${(this.props.value / this.props.max) * 100}%`,
                        width: `${((this.props.max - this.props.value) / this.props.max) * 100}%`
                    }}
                />
                <div
                    className="ui-slider__value"
                    style={{
                        width: `${(this.props.value / this.props.max) * 100}%`,
                        backgroundColor: this.props.color
                    }}
                />

                <div
                    className="ui-slider__button"
                    style={{
                        left: `${(this.props.value / this.props.max) * 100}%`,
                        backgroundColor: this.props.color
                    }}
                    onPointerDown={this.onPointerDown}
                >
                    <Ink center={true} />
                </div>
            </div>
        );
    }
}