import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

export class Card extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-card', this.props.className)}
                style={this.props.style}
                onClick={this.props.onClick}
            >
                {this.props.children}
            </div>
        );
    }
}