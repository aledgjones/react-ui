// tslint:disable:no-any

import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
}

export class Toolbar extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-toolbar', this.props.className)}
            >
                {this.props.children}
            </div>
        );
    }
}