import * as React from 'react';

import { Content } from '../content';
import { Button } from '../button';
import { Tab } from '../contact-picker-dialog';
import { Input } from '../input';
import { Title } from '../title';
import { Avatar } from '../avatar';
import { Label } from '../label';
import { chooseFiles, hasValue } from '../../utils';
import { toast } from '../toast/store';

import './styles.css';
import { IContactCreator } from '../contact-picker';
import { Textarea } from '../textarea';

interface IState {
    working: boolean;

    name: string;
    email: string;
    address: string;
    avatar?: string;
    _avatar?: File;
}

interface IProps {
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onNavigate: (tab: Tab) => void;
}

export class ContactPickerDialogCreate extends React.PureComponent<IProps> {

    public state: IState = { working: false, name: '', email: '', address: '' };

    private updateName = (val: string) => this.setState({ name: val });
    private updateEmail = (val: string) => this.setState({ email: val });
    private updateAddress = (val: string) => this.setState({ address: val });

    private toList = () => this.props.onNavigate(Tab.LIST);

    private selectImage = async () => {
        const files = await chooseFiles({ accept: 'image/*', multiple: false });
        if (this.state.avatar) {
            URL.revokeObjectURL(this.state.avatar);
        }
        if (files.length > 0) {
            this.setState({ avatar: URL.createObjectURL(files[0]), _avatar: files[0] });
        }
    }

    public componentWillUnmount() {
        if (this.state.avatar) {
            URL.revokeObjectURL(this.state.avatar);
        }
    }

    private create = async () => {
        if (hasValue(this.state.name)) {
            this.setState({ working: true });
            const contact: IContactCreator = {
                name: this.state.name,
            }
            if (this.state.email) {
                contact.email = this.state.email;
            }
            if (this.state.address) {
                contact.address = this.state.address;
            }
            if (this.state._avatar) {
                try {
                    const url = await this.props.onUpload(this.state._avatar);
                    contact.avatar = url;
                } catch {
                    toast.show({ text: 'Something went wrong' });
                    this.setState({ working: false });
                    return false;
                }
            }
            await this.props.onCreate(contact);
            this.props.onNavigate(Tab.LIST);
            return true;
        } else {
            toast.show({ text: 'Must have name.' });
            return false;
        }
    }

    public render() {
        return <>
            <Content className="ui-contact-picker-dialog-create">
                <Title>New Contact</Title>
                <div className="ui-contact-picker-dialog-create__avatar-container">
                    <div className="ui-contact-picker-dialog-create__avatar" onClick={this.selectImage}>
                        <Avatar size={64} name={this.state.name || 'A'} src={this.state.avatar} />
                    </div>
                    <Label className="ui-contact-picker-dialog-create__label">
                        <p>{this.state.name || 'New contact'}</p>
                        <p>{this.state.email || 'contact@example.com'}</p>
                    </Label>
                </div>
                <Input type="text" required placeholder="Name" value={this.state.name} onChange={this.updateName} />
                <Input type="email" placeholder="Email" value={this.state.email} onChange={this.updateEmail} />
                <Textarea placeholder="Address" value={this.state.address} onChange={this.updateAddress} />
            </Content>
            <Content className="ui-contact-picker-dialog-create__buttons ui-content--buttons ui-content--compact">
                <Button disabled={this.state.working} className="ui-button--compact" onClick={this.toList}>Cancel</Button>
                <Button disabled={this.state.working} working={this.state.working} className="ui-button--compact" onClick={this.create}>Create</Button>
            </Content>
        </>
    }
}