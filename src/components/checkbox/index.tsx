import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';
import { Ink } from '../ink';
import { Icon } from '../icon';
import { mdiCheck } from '@mdi/js';

export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value: boolean;
    disabled?: boolean;
    onChange?: (val: boolean) => void;
}

export class Checkbox extends React.PureComponent<IProps> {

    public onClick = () => this.props.onChange && this.props.onChange(!this.props.value);

    public render() {
        return <div
            id={this.props.id}
            className={mergeClasses(
                'ui-checkbox',
                this.props.className,
                { 'ui-checkbox--active': this.props.value, 'ui-checkbox--disabled': this.props.disabled }
            )}
            style={this.props.style}
            onClick={this.onClick}
        >
            <Icon color="white" className="ui-checkbox__icon" path={mdiCheck} />
            <Ink center />
        </div>
    }
}