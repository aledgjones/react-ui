import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Backdrop } from '../backdrop';

import './styles.css';


interface IProps {
    id?: string;
    className?: string;
    visible: boolean;
    backdrop: boolean;

    onClick?: () => void;
}

export class Dialog extends React.PureComponent<IProps> {

    private generateContainer() {
        const container = document.createElement('div');
        container.classList.add('ui-dialog');
        if (this.props.id) {
            container.id = this.props.id
        }
        return container;
    }

    private portal = document.getElementById('portal');
    private container = this.generateContainer()

    public componentDidMount() {
        if (this.portal) {
            this.portal.appendChild(this.container);
        }
    }

    public componentWillUnmount() {
        if (this.portal) {
            this.portal.removeChild(this.container);
        }
    }

    public render() {

        this.container.classList.toggle('ui-dialog--visible', this.props.visible);

        return ReactDOM.createPortal(
            <>
                <Backdrop visible={this.props.visible} onClick={this.props.onClick} transparent={!this.props.backdrop} />
                {this.props.visible && this.props.children}
            </>,
            this.container,
        );
    }
}