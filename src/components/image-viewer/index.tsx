import * as React from 'react';
import { mdiArrowLeft, mdiCrop, mdiMagnifyPlusOutline, mdiMagnifyMinusOutline, mdiInformationOutline, mdiDotsVertical, mdiRestore, mdiClose } from '@mdi/js';

import { scaleToFit } from '../../utils/scale-to-fit';
import { ICrop, getImage, getImageSize, getCroppedImage, cropChanged, IDimensions } from '../../utils/image';
import { mergeClasses } from '../../utils/merge-classes';
import { download } from '../../utils/download';
import { toast } from '../toast/store';
import { Toolbar } from '../toolbar';
import { ToolbarLine } from '../toolbar-line';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { Tooltip } from '../tooltip';
import { Menu } from '../menu';
import { List } from '../list';
import { ListItem } from '../list-item';
import { Label } from '../label';
import { Button } from '../button';
import { CircularProgress } from '../circular-progress';
import { ImageViewerChild } from '../image-viewer-child';
import { ResizeListener } from '../../utils/resize';

import './styles.css';

interface IProps {
    src: string;
    crop?: ICrop;
    croppable?: boolean;
    onClose: () => void;
    onCropSave?: (points: ICrop) => void;
}

interface IState {
    ready: boolean;
    info: boolean;
    x: number;
    y: number;
    height: number;
    width: number;
    dragging: boolean;
    cropMode: boolean;
    crop: ICrop; // we make a copy of prop so we can manipulate it locally before commiting.
    zoom: number;
    saving: boolean;
}

export class ImageViewer extends React.PureComponent<IProps, IState> {

    private noCrop: ICrop = [{ x: 0, y: 0 }, { x: 100, y: 100 }];
    private parentBox: IDimensions;

    public state: IState = {
        ready: false,
        x: 0,
        y: 0,
        height: 0,
        width: 0,
        dragging: false,
        info: false,
        cropMode: false,
        crop: this.props.crop || this.noCrop,
        zoom: 1,
        saving: false
    };

    private onResize = async (parentBox: IDimensions) => {
        this.parentBox = parentBox;
        const image = await getImage(this.props.src);
        const cropBox = await getImageSize(image, this.state.crop);
        const scale = scaleToFit(this.state.cropMode ? image : cropBox, parentBox);
        const scaledImageBox = { height: image.height * scale, width: image.width * scale };
        this.setState({ ...scaledImageBox, ready: true });
    }

    public downloadFull = async () => {
        try {
            const ext = this.props.src.split('.').pop()
            await download(this.props.src, `image.${ext}`);
        } catch (err) {
            toast.show({ text: 'Sorry, could not download' });
        }
    }

    public downloadCrop = async () => {
        try {
            const url = await getCroppedImage(this.props.src, this.state.crop);
            await download(url, 'image-cropped.jpg');
            URL.revokeObjectURL(url);
        } catch (err) {
            toast.show({ text: 'Sorry, could not download' });
        }
    }
    private toggleInfo = () => this.setState({ info: !this.state.info });

    private zoomIn = () => this.setState({ zoom: 2 });
    private zoomOut = () => this.setState({ zoom: 1, x: 0, y: 0 });

    private updateCoordinates = (x: number, y: number) => this.setState({ x, y });

    private enterCropMode = () => this.setState({ cropMode: true, info: false, zoom: 1, x: 0, y: 0 }, () => this.onResize(this.parentBox));
    private exitCropMode = () => this.setState({ cropMode: false }, () => this.onResize(this.parentBox));

    private updateLocalCrop = (points: ICrop) => this.setState({ crop: points });
    private isCropped = () => this.state.crop[0].x !== 0 || this.state.crop[0].y !== 0 || this.state.crop[1].x !== 100 || this.state.crop[1].y !== 100;

    private restoreCrop = () => this.setState({ crop: this.props.crop || this.noCrop });

    private saveCrop = async () => {
        if (cropChanged(this.props.crop || this.noCrop, this.state.crop)) {
            this.setState({ saving: true });
            if (this.props.onCropSave) {
                await this.props.onCropSave(this.state.crop);
            }
            this.setState({ saving: false });
            toast.show({ text: 'Updated' }, 2000);
        }
        this.exitCropMode();
    }

    public render() {

        const isCropped = this.isCropped();

        return <>
            <div className="photo-viewer">
                <Toolbar className={mergeClasses("photo-viewer__toolbar", { 'photo-viewer__toolbar--info': this.state.info })}>

                    {!this.state.cropMode && <ToolbarLine>
                        <IconButton className="ui-icon-button--margin" onClick={this.props.onClose} path={mdiArrowLeft} color="white" />
                        <Title>Back</Title>
                        {this.props.croppable && <Tooltip className="photo-viewer__tool" text="Edit">
                            <IconButton onClick={this.enterCropMode} path={mdiCrop} color="white" />
                        </Tooltip>}
                        {
                            this.state.zoom === 1 ?
                                <Tooltip className="photo-viewer__tool" text="Zoom in">
                                    <IconButton onClick={this.zoomIn} path={mdiMagnifyPlusOutline} color="white" />
                                </Tooltip> :
                                <Tooltip className="photo-viewer__tool" text="Zoom out">
                                    <IconButton onClick={this.zoomOut} path={mdiMagnifyMinusOutline} color="white" />
                                </Tooltip>
                        }
                        <Tooltip className="photo-viewer__tool" text="Info">
                            <IconButton onClick={this.toggleInfo} path={mdiInformationOutline} color="white" />
                        </Tooltip>
                        <Menu
                            className="photo-viewer__icon"
                            direction="right"
                            width={200}
                            target={
                                <IconButton path={mdiDotsVertical} color="white" />
                            }
                            menu={
                                <List>
                                    {isCropped && <>
                                        <ListItem onClick={this.downloadCrop}>
                                            <Label>
                                                <p>Download</p>
                                            </Label>
                                        </ListItem><ListItem onClick={this.downloadFull}>
                                            <Label>
                                                <p>Download original</p>
                                            </Label>
                                        </ListItem>
                                    </>}
                                    {!isCropped && <ListItem onClick={this.downloadFull}>
                                        <Label>
                                            <p>Download</p>
                                        </Label>
                                    </ListItem>}
                                </List>
                            }
                        />
                    </ToolbarLine>}

                    {this.state.cropMode && <ToolbarLine>
                        <div className="photo-viewer__spacer" />
                        <Tooltip className="photo-viewer__tool" text="Restore crop">
                            <IconButton disabled={this.state.saving || !cropChanged(this.props.crop || this.noCrop, this.state.crop)} onClick={this.restoreCrop} path={mdiRestore} color="white" />
                        </Tooltip>
                        <Button disabled={this.state.saving} className="ui-button--compact" onClick={this.saveCrop}>Done</Button>
                    </ToolbarLine>}

                </Toolbar>

                <div className={mergeClasses("photo-viewer__content", { 'photo-viewer__content--info': this.state.info })}>
                    <div className="photo-viewer__container">
                        <ResizeListener onResize={this.onResize} />
                        {!this.state.ready && <CircularProgress />}
                        {this.state.ready && <ImageViewerChild
                            src={this.props.src}
                            height={this.state.height}
                            width={this.state.width}
                            zoom={this.state.zoom}

                            crop={this.state.crop}
                            croppable={this.state.cropMode}
                            onCropUpdate={this.updateLocalCrop}

                            x={this.state.x}
                            y={this.state.y}
                            draggable={this.state.zoom > 1 && !this.state.cropMode}
                            onDragUpdate={this.updateCoordinates}

                            saving={this.state.saving}
                        />}
                    </div>
                </div>
            </div>

            <div className={mergeClasses('photo-viewer__info', { 'photo-viewer__info--show': this.state.info })}>
                <Toolbar>
                    <ToolbarLine>
                        <Title>Info</Title>
                        <IconButton onClick={this.toggleInfo} path={mdiClose} />
                    </ToolbarLine>
                </Toolbar>
                {this.props.children}
            </div>
        </>
    }
}