import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

// this is sort of a dummy component to keep the api clean,
// because <Tabs /> actually creactes a different component based on these props
interface IProps {
    id?: string;
    className?: string;
    value: string;
}
export class TabsItem extends React.PureComponent<IProps> {
    public render = () => null;
}

// the actual rendered component
interface IPropsComponent {
    id?: string;
    className?: string;
    onClick: (value: string, e: HTMLDivElement | null) => void;
    onInit: (e: HTMLDivElement | null) => void;
    selected: boolean;
    value: string;
    content: any;
}

export class TabsItemComponent extends React.PureComponent<IPropsComponent> {

    private element: React.RefObject<HTMLDivElement> = React.createRef();

    public componentDidMount() {
        if (this.props.selected) {
            this.props.onInit(this.element.current);
        }
    }

    private onClick = (e: React.MouseEvent<HTMLDivElement>) => {
        this.props.onClick(this.props.value, this.element.current);
    }

    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-tabs-item', this.props.className, { 'ui-tabs-item--selected': this.props.selected })}
                onClick={this.onClick}
                ref={this.element}
            >
                <Ink cover={true} />
                <div className="ui-tabs-item__content">
                    {this.props.content}
                </div>
            </div>
        );
    }

}