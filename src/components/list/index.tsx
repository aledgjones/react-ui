// tslint:disable:no-any

import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
}

export class List extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-list', this.props.className)}
            >
                {this.props.children}
            </div>
        );
    }
}