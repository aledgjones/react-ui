import * as React from 'react';
import { Icon, Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    hidden?: boolean;
    path: string;
    color?: string;
}

export class Fab extends React.PureComponent<IProps> {

    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses(
                    'ui-fab',
                    this.props.className,
                    { 'ui-fab--disabled': this.props.disabled, 'ui-fab--hidden': this.props.hidden }
                )}
                onClick={this.props.onClick}
            >
                <Ink cover={true} />
                <Icon path={this.props.path} color={this.props.color || 'white'} />
            </div>
        );
    }
}