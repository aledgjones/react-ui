import * as React from 'react';

import { Card, Content, Button } from '..';
import { Slider } from '../slider';
import { IColor } from '../color-picker';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    value?: string;
    required?: boolean;
    onCancel: () => void;
    onComplete: (value: string) => void;
}

export interface IState {
    r: number;
    g: number;
    b: number;
    a: number;
    recents: IColor[];
}

export class ColorPickerDialog extends React.PureComponent<IProps, IState> {

    public state: IState = {
        ...this.componentsFromString(this.props.value),
        recents: JSON.parse(localStorage.getItem('ui:colors') || '[]')
    };

    private componentsFromString(value?: string) {
        if (value) {
            const matches = value.match(/[.?\d]+/g);
            if (matches) {
                return { r: parseInt(matches[0]), g: parseInt(matches[1]), b: parseInt(matches[2]), a: parseFloat(matches[3]) * 100 };
            }
        }

        return { r: 0, g: 0, b: 0, a: 100 };
    }

    private addRecent(value: IColor) {
        const updated = this.state.recents.filter(color => this.colorString(color) !== this.colorString(value));
        updated.unshift(value);
        const clipped = updated.slice(0, 6);
        localStorage.setItem('ui:colors', JSON.stringify(clipped));
    }

    private updateValue = () => {
        const value = { r: this.state.r, g: this.state.g, b: this.state.b, a: this.state.a };
        this.addRecent(value);
        this.props.onComplete(this.colorString(value));
    }

    private colorString(value: IColor) {
        return `rgba(${value.r}, ${value.g}, ${value.b}, ${(value.a / 100).toFixed(2)})`;
    }

    private updateR = (val: number) => this.setState({ r: val });
    private updateG = (val: number) => this.setState({ g: val });
    private updateB = (val: number) => this.setState({ b: val });
    private updateA = (val: number) => this.setState({ a: val });

    private updateAll = (color: IColor) => this.setState({ ...color });

    public render() {

        const color = this.colorString({ r: this.state.r, g: this.state.g, b: this.state.b, a: this.state.a });

        return <Card className="ui-dialog__card ui-color-picker-dialog__card">
            <Content>
                <div className="ui-color-picker-dialog__preview">
                    <div className="ui-color-picker-dialog__hatching" />
                    <div className="ui-color-picker-dialog__fill" style={{ backgroundColor: color }} />
                </div>
            </Content>
            <Content className="ui-color-picker-dialog__values">
                <div className="ui-color-picker-dialog__value-container">
                    <span className="ui-color-picker-dialog__value-label">R</span>
                    <Slider max={255} className="ui-slider--dark" value={this.state.r} onChange={this.updateR} />
                    <span className="ui-color-picker-dialog__value">{this.state.r}</span>
                </div>
                <div className="ui-color-picker-dialog__value-container">
                    <span className="ui-color-picker-dialog__value-label">G</span>
                    <Slider max={255} className="ui-slider--dark" value={this.state.g} onChange={this.updateG} />
                    <span className="ui-color-picker-dialog__value">{this.state.g}</span>
                </div>
                <div className="ui-color-picker-dialog__value-container">
                    <span className="ui-color-picker-dialog__value-label">B</span>
                    <Slider max={255} className="ui-slider--dark" value={this.state.b} onChange={this.updateB} />
                    <span className="ui-color-picker-dialog__value">{this.state.b}</span>
                </div>
                <div className="ui-color-picker-dialog__value-container">
                    <span className="ui-color-picker-dialog__value-label">A</span>
                    <Slider color="grey" max={100} className="ui-slider--dark" value={this.state.a} onChange={this.updateA} />
                    <span className="ui-color-picker-dialog__value">{(this.state.a / 100).toFixed(2)}</span>
                </div>
            </Content>
            {this.state.recents.length > 0 && <Content className="ui-color-picker-dialog__recents">
                <div className="ui-color-picker-dialog__recents-grid">
                    {
                        this.state.recents.map((color: IColor) => {
                            const str = this.colorString(color);
                            return <div key={str} className="ui-color-picker-dialog__swatch" onClick={() => this.updateAll(color)}>
                                <div className="ui-color-picker-dialog__hatching" />
                                <div className="ui-color-picker-dialog__fill" style={{ backgroundColor: str }} />
                            </div>
                        })
                    }
                </div>
            </Content>}
            <Content className="ui-content--buttons ui-content--compact">
                <Button className="ui-button--compact" onClick={this.props.onCancel}>Cancel</Button>
                <Button className="ui-button--compact" onClick={this.updateValue}>Select</Button>
            </Content>
        </Card>
    }
}