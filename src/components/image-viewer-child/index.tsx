import * as React from 'react';

import { CircularProgress } from '../circular-progress';
import { mergeClasses } from '../../utils/merge-classes';
import { pointerMoveGenerator } from '../../utils/pointer-move-generator'
import { ICrop, IPoint } from '../../utils/image';

import './styles.css';

interface IProps {
    src: string;
    height: number; // full image scaled dimensions
    width: number;
    zoom: number;

    crop: ICrop;
    croppable: boolean;
    onCropUpdate: (points: ICrop) => void;

    x: number;
    y: number;
    draggable: boolean;
    onDragUpdate: (x: number, y: number) => void;

    saving: boolean;
}

interface IState {
    active: boolean
}

interface IDragData {
    pointer: IPoint;
    box: IPoint;
}

interface ICropDragData {
    pointer: IPoint;
    crop: ICrop;
}

interface ICropData {
    direction: 'n' | 'ne' | 'e' | 'se' | 's' | 'sw' | 'w' | 'nw';
    pointer: IPoint,
    crop: ICrop;
}

export class ImageViewerChild extends React.PureComponent<IProps, IState> {

    public state: IState = { active: false };

    private pxHeightToPercent = (px: number) => px * (100 / this.props.height);
    private pxWidthToPercent = (px: number) => px * (100 / this.props.width);
    private percentHeightTopx = (percent: number) => percent * (this.props.height / 100);
    private percentWidthTopx = (percent: number) => percent * (this.props.width / 100);

    private minHeight = this.pxHeightToPercent(48);
    private minWidth = this.pxWidthToPercent(48);
    private maxHeight = 100;
    private maxWidth = 100;

    private startDrag = pointerMoveGenerator<IDragData>(
        (e: React.PointerEvent, data: IDragData) => {
            if (this.props.draggable) {
                this.setState({ active: true });
                data.pointer = { x: e.screenX, y: e.screenY };
                data.box = { x: this.props.x, y: this.props.y };
                return true;
            } else {
                return false;
            }
        },
        (e: PointerEvent, init: IDragData) => {
            const x = init.box.x + (e.screenX - init.pointer.x);
            const y = init.box.y + (e.screenY - init.pointer.y);
            this.props.onDragUpdate(x, y);
        },
        () => this.setState({ active: false })
    )

    private startCropDrag = pointerMoveGenerator(
        (e: React.PointerEvent, data: ICropDragData) => {
            this.setState({ active: true });
            data.pointer = { x: e.screenX, y: e.screenY };
            data.crop = [
                { x: this.props.crop[0].x, y: this.props.crop[0].y },
                { x: this.props.crop[1].x, y: this.props.crop[1].y }
            ];
            return true;
        },
        (e: PointerEvent, data: ICropDragData) => {

            let crop: ICrop = [{ ...data.crop[0] }, { ...data.crop[1] }];

            const x = this.pxWidthToPercent(e.screenX - data.pointer.x);
            const y = this.pxHeightToPercent(e.screenY - data.pointer.y);

            if (data.crop[0].x + x < 0) {
                crop[0].x = 0;
                crop[1].x = data.crop[1].x - data.crop[0].x;
            } else if (data.crop[1].x + x > this.maxWidth) {
                crop[0].x = this.maxWidth - (data.crop[1].x - data.crop[0].x);
                crop[1].x = this.maxWidth;
            } else {
                crop[0].x = crop[0].x + x;
                crop[1].x = crop[1].x + x;
            }

            if (data.crop[0].y + y < 0) {
                crop[0].y = 0;
                crop[1].y = data.crop[1].y - data.crop[0].y;
            } else if (data.crop[1].y + y > this.maxHeight) {
                crop[0].y = this.maxHeight - (data.crop[1].y - data.crop[0].y);
                crop[1].y = this.maxHeight;
            } else {
                crop[0].y = crop[0].y + y;
                crop[1].y = crop[1].y + y;
            }

            this.props.onCropUpdate(crop);

        },
        () => this.setState({ active: false })
    )

    private startCrop = pointerMoveGenerator<ICropData>(
        (e: any, data: ICropData) => {
            this.setState({ active: true });
            data.direction = e.target.dataset.direction;
            data.pointer = { x: e.screenX, y: e.screenY };
            data.crop = [
                { x: this.props.crop[0].x, y: this.props.crop[0].y },
                { x: this.props.crop[1].x, y: this.props.crop[1].y }
            ];
            return true;
        },
        (e: PointerEvent, data: ICropData) => {

            let crop: ICrop = [{ ...data.crop[0] }, { ...data.crop[1] }];

            if (data.direction.includes('n')) {
                const y = data.crop[0].y + this.pxHeightToPercent(e.screenY - data.pointer.y);
                if (y < 0) {
                    crop[0].y = 0;
                } else if (y > crop[1].y - this.minHeight) {
                    crop[0].y = crop[1].y - this.minHeight;
                } else {
                    crop[0].y = y;
                }
            }

            if (data.direction.includes('e')) {
                const x = data.crop[1].x + this.pxWidthToPercent(e.screenX - data.pointer.x);
                if (x < crop[0].x + this.minWidth) {
                    crop[1].x = crop[0].x + this.minWidth;
                } else if (x > this.maxWidth) {
                    crop[1].x = this.maxWidth;
                } else {
                    crop[1].x = x;
                }
            }

            if (data.direction.includes('s')) {
                const y = data.crop[1].y + this.pxHeightToPercent(e.screenY - data.pointer.y);
                if (y < data.crop[0].y + this.minHeight) {
                    crop[1].y = data.crop[0].y + this.minHeight;
                } else if (y > this.maxHeight) {
                    crop[1].y = this.maxHeight;
                } else {
                    crop[1].y = y;
                }
            }

            if (data.direction.includes('w')) {
                const x = data.crop[0].x + this.pxWidthToPercent(e.screenX - data.pointer.x);
                if (x < 0) {
                    crop[0].x = 0;
                } else if (x > crop[1].x - this.minWidth) {
                    crop[0].x = crop[1].x - this.minWidth;
                } else {
                    crop[0].x = x;
                }
            }

            this.props.onCropUpdate(crop);

        },
        () => this.setState({ active: false })
    )

    private onDrag = (e: React.PointerEvent) => {
        if (this.props.croppable) {
            return this.startCropDrag(e);
        }
        if (this.props.draggable) {
            return this.startDrag(e);
        }
    }

    public render() {

        const top = this.percentHeightTopx(this.props.crop[0].y * -1);
        const left = this.percentWidthTopx(this.props.crop[0].x * -1);

        return <div
            className={mergeClasses("photo", {
                'photo--saving': this.props.saving,
                'photo--croppable': this.props.croppable,
                'photo--draggable': this.props.croppable || this.props.draggable,
                'photo--no-transition': this.state.active
            })}
            style={{
                width: this.percentWidthTopx(this.props.crop[1].x - this.props.crop[0].x),
                height: this.percentHeightTopx(this.props.crop[1].y - this.props.crop[0].y),
                transform: `translate(${this.props.x}px, ${this.props.y}px) scale(${this.props.zoom})`
            }}
        >

            <div
                className="photo__ghost"
                style={{
                    backgroundImage: `url(${this.props.src})`,
                    top,
                    left,
                    width: this.props.width,
                    height: this.props.height
                }}
            />

            <div className="photo__paint-container">
                <div
                    className="photo__paint"
                    style={{
                        backgroundImage: `url(${this.props.src})`,
                        top,
                        left,
                        width: this.props.width,
                        height: this.props.height
                    }}
                />
                {this.props.saving && <CircularProgress className="photo__spinner" />}
            </div>

            <div
                onPointerDown={this.onDrag}
                className="photo__drag-box"
                style={{
                    width: `100%`,
                    height: `100%`,
                    top: 0,
                    left: 0,
                }}
            />

            {this.props.croppable && <>
                <div data-direction="n" onPointerDown={this.startCrop} className="photo__handle photo__handle--n" />
                <div data-direction="ne" onPointerDown={this.startCrop} className="photo__handle photo__handle--ne" />
                <div data-direction="e" onPointerDown={this.startCrop} className="photo__handle photo__handle--e" />
                <div data-direction="se" onPointerDown={this.startCrop} className="photo__handle photo__handle--se" />
                <div data-direction="s" onPointerDown={this.startCrop} className="photo__handle photo__handle--s" />
                <div data-direction="sw" onPointerDown={this.startCrop} className="photo__handle photo__handle--sw" />
                <div data-direction="w" onPointerDown={this.startCrop} className="photo__handle photo__handle--w" />
                <div data-direction="nw" onPointerDown={this.startCrop} className="photo__handle photo__handle--nw" />
            </>}

        </div>

    }
}