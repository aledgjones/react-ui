import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    visible: boolean;
    disabled?: boolean;
    transparent: boolean;
    onClick?: () => void;
}

export class Backdrop extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-backdrop', this.props.className, { 'ui-backdrop--visible': this.props.visible, 'ui-backdrop--disabled': this.props.disabled, 'ui-backdrop--transparent': this.props.transparent })}
                onClick={this.props.onClick}
            >
                {this.props.children}
            </div>
        );
    }
}