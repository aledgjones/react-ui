import * as React from 'react';
import { mdiClose, mdiAccountSearchOutline, mdiAccount } from '@mdi/js';

import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { Label } from '../label';
import { ContactPickerDialog } from '../contact-picker-dialog';
import { Avatar } from '../avatar';
import { Icon } from '../icon';

import './styles.css';

export interface IContactCreator {
    name: string;
    avatar?: string;
    address?: string;
    email?: string;
}

export interface IContact extends IContactCreator {
    key: string;
}

export interface IProps {
    id?: string;
    className?: string;
    value: IContact | undefined;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onChange: (value: IContact | undefined) => void;
    onFocus?: () => void;
    onBlur?: () => void;

    contacts: IContact[];
}

export interface IState {
    focus: boolean;
    touched: boolean;
}

export class ContactPicker extends React.PureComponent<IProps, IState> {

    public state: IState = {
        focus: false,
        touched: false
    };

    private hasError() {
        return this.props.required && !hasValue(this.props.value);
    }

    private showDialog = () => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    private hideDialog = () => {
        this.setState({ focus: false });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    private clear = () => {
        this.props.onChange(undefined);
    }

    private updateValue = (contact: IContact) => {
        this.props.onChange(contact);
        this.hideDialog();
    }

    public render() {

        return <>
            <div className={mergeClasses('ui-contact-picker', this.props.className, {
                'ui-contact-picker--disabled': this.props.disabled,
                'ui-contact-picker--has-value': hasValue(this.props.value),
                'ui-contact-picker--invalid': this.hasError(),
                'ui-contact-picker--touched': this.state.touched
            })}>

                {!this.props.value && <Icon color="rgb(150,150,150)" className="ui-contact-picker__preview" path={mdiAccount} />}
                {this.props.value && <Avatar className="ui-contact-picker__preview" size={24} src={this.props.value ? this.props.value.avatar : undefined} name={this.props.value ? this.props.value.name : 'A'} />}

                {!this.props.value && <div className="ui-contact-picker__placeholder" onClick={this.showDialog}>{this.props.placeholder + (this.props.required ? '*' : '')}</div>}

                {this.props.value && <div className="ui-contact-picker__value" onClick={this.showDialog}>
                    <Label>
                        <p>{this.props.value.name}</p>
                        {this.props.value.email && <p>{this.props.value.email}</p>}
                    </Label>
                </div>}

                {(hasValue(this.props.value) && !this.props.required) ?
                    <IconButton className="ui-contact-picker__clear-icon" onClick={this.clear} path={mdiClose} color="rgb(150,150,150)" /> :
                    <IconButton className="ui-contact-picker__down-icon" onClick={this.showDialog} path={mdiAccountSearchOutline} color="rgb(150,150,150)" />
                }

                <div className="ui-contact-picker__bar" />

            </div >

            {
                <Dialog visible={!!this.state.focus} backdrop={true}>
                    <ContactPickerDialog contacts={this.props.contacts} onCancel={this.hideDialog} onSelect={this.updateValue} onUpload={this.props.onUpload} onCreate={this.props.onCreate} />
                </Dialog>
            }
        </>
    }
}