import * as React from 'react';
import { Icon, Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    style?: React.CSSProperties;
    toggle?: boolean;
    path: string;
    color?: string;
    size?: number;
}

export class IconButton extends React.PureComponent<IProps> {
    public render() {

        const size = this.props.size || 24;
        const color = this.props.color || 'rgba(0,0,0,.87)';

        return (
            <div
                id={this.props.id}
                className={mergeClasses(
                    'ui-icon-button',
                    this.props.className,
                    {
                        'ui-icon-button--disabled': this.props.disabled,
                        'ui-icon-button--toggle': this.props.toggle !== undefined,
                        'ui-icon-button--toggle-on': this.props.toggle === true
                    }
                )}
                onClick={this.props.onClick}
                style={{ height: size, width: size, ...this.props.style }}
            >
                <Ink center={true} />
                <Icon path={this.props.path} color={color} size={size} />
            </div>
        );
    }
}