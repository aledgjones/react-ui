import * as React from 'react';
import { mdiClose, mdiEyedropper } from '@mdi/js';
import { IconButton, Dialog } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { ColorPickerDialog } from '../color-picker-dialog';

import './styles.css';

export interface IColor {
    r: number;
    g: number;
    b: number;
    a: number;
}

export interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    value?: string;
    placeholder: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value?: string) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}

export interface IState {
    focus: boolean;
    touched: boolean;
}

export class ColorPicker extends React.PureComponent<IProps, IState> {

    public state: IState = {
        focus: false,
        touched: false
    };

    private hasError() {
        return this.props.required && !hasValue(this.props.value);
    }

    private showDialog = () => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    private hideDialog = () => {
        this.setState({ focus: false });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    private clear = () => {
        this.props.onChange();
    }

    private updateValue = (color: string) => {
        this.props.onChange(color);
        this.hideDialog();
    }

    private colorString(css: boolean = false) {
        if (this.props.value) {
            return this.props.value;
        } else {
            return css ? 'white' : '';
        }
    }

    public render() {

        return <>
            <div
                id={this.props.id}
                style={this.props.style}
                className={mergeClasses('ui-color-picker', this.props.className, {
                    'ui-color-picker--disabled': this.props.disabled,
                    'ui-color-picker--focus': this.state.focus,
                    'ui-color-picker--has-value': hasValue(this.props.value),
                    'ui-color-picker--invalid': this.hasError(),
                    'ui-color-picker--touched': this.state.touched
                })}
            >

                <div className="ui-color-picker__preview-swatch">
                    <div className="ui-color-picker__hatching" />
                    <div className="ui-color-picker__fill" style={{ backgroundColor: this.colorString(true) }} />
                </div>

                <div className="ui-color-picker__value" onClick={this.showDialog}>{this.props.placeholder + (this.props.required ? '*' : '')}</div>

                {(hasValue(this.props.value) && !this.props.required) ?
                    <IconButton className="ui-color-picker__clear-icon" onClick={this.clear} path={mdiClose} color="rgb(150,150,150)" /> :
                    <IconButton className="ui-color-picker__down-icon" onClick={this.showDialog} path={mdiEyedropper} color="rgb(150,150,150)" />
                }

                <div className="ui-color-picker__bar" />

            </div >

            {
                <Dialog visible={!!this.state.focus} backdrop={true}>
                    <ColorPickerDialog value={this.props.value} onCancel={this.hideDialog} onComplete={this.updateValue} />
                </Dialog>
            }
        </>
    }
}