import * as React from 'react';
import { CircularProgress, Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    working?: boolean;
    href?: string;
}

export class Button extends React.PureComponent<IProps> {

    private button() {
        return <button
            id={this.props.id}
            className={mergeClasses('ui-button', this.props.className, { 'ui-button--disabled': this.props.disabled })}
            onClick={this.props.onClick}
            disabled={this.props.disabled}
        >
            <Ink cover={true} />
            <div className="ui-button__content">
                {this.props.children}
                {!!this.props.working && <CircularProgress className="ui-circular-progress--tiny" />}
            </div>
        </button>
    }

    public render() {
        if (this.props.href) {
            return <a href={this.props.href} target="_blank">{this.button()}</a>
        } else {
            return this.button();
        }
    }
}