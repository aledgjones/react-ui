import * as React from 'react';

import { IContact, IContactCreator } from '../contact-picker';
import { ContactPickerDialogList } from '../contact-picker-dialog-list';
import { ContactPickerDialogCreate } from '../contact-picker-dialog-create';
import { Card } from '../card';

import './styles.css';

export enum Tab {
    LIST = 'list',
    CREATE = 'create',
    EDIT = 'edit'
}

interface IState {
    tab: Tab;
}

interface IProps {
    id?: string;
    className?: string;
    contacts: IContact[];
    onUpload: (file: File) => Promise<string>;
    onCreate: (contact: IContactCreator) => void;
    onCancel: () => void;
    onSelect: (value: IContact) => void;
}

export class ContactPickerDialog extends React.PureComponent<IProps> {

    public state: IState = { tab: Tab.LIST };

    private toTab = (value: Tab) => this.setState({ tab: value })

    public render() {
        switch (this.state.tab) {
            case Tab.LIST:
                return (
                    <Card className="ui-dialog__card ui-contact-picker-dialog__card">
                        <ContactPickerDialogList contacts={this.props.contacts} onNavigate={this.toTab} onCancel={this.props.onCancel} onSelect={this.props.onSelect} />
                    </Card>
                );
            case Tab.CREATE:
                return (
                    <Card className="ui-dialog__card ui-contact-picker-dialog__card">
                        <ContactPickerDialogCreate onNavigate={this.toTab} onUpload={this.props.onUpload} onCreate={this.props.onCreate} />
                    </Card>
                );
            default:
                return null;
        }
    }
}