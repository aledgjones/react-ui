import * as React from 'react';
import { mergeClasses } from '../../utils';
import { TabsItemComponent } from '../tabs-item';
import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    value: string;
    onChange: (value: string) => void;
}

interface IState {
    width: number;
    left: number;
}

export class Tabs extends React.PureComponent<IProps, IState> {

    public state: IState = { left: 0, width: 100 };

    private onChange = (value: string, tab: HTMLDivElement | null) => {
        this.updateBar(tab);
        this.props.onChange(value);
    }

    private updateBar = (tab: HTMLDivElement | null) => {
        if (tab) {
            this.setState({ left: tab.offsetLeft, width: tab.offsetWidth });
        }
    }

    public render() {

        const children = React.Children.map(this.props.children, (tab: any) => {
            return <TabsItemComponent
                id={tab.props.id}
                className={tab.props.className}
                value={tab.props.value}
                selected={this.props.value === tab.props.value}
                onClick={this.onChange}
                onInit={this.updateBar}
                content={tab.props.children}
            />
        });

        return (
            <div id={this.props.id} className={mergeClasses('ui-tabs', this.props.className)}>
                {children}
                <div className="ui-tabs__bar" style={{ left: this.state.left, width: this.state.width }} />
            </div>
        );
    }
}