import React from 'react';
import pretty from 'pretty-bytes';

import { Toolbar } from '../toolbar';
import { IconButton } from '../icon-button';
import { Title } from '../title';
import { ToolbarLine } from '../toolbar-line';
import { Icon } from '../icon';
import { Button } from '../button';
import { Card } from '../card';
import { chooseFiles } from '../../utils/choose-files';
import { Content } from '../content';
import { Image } from '../image';
import { Label } from '../label';
import { LinearProgress } from '../linear-progress';
import { toast } from '../toast/store';
import { Subheader } from '../subheader';
import { pluralize } from '../../utils/pluralize';
import { mergeClasses } from '../../utils/merge-classes';
import { ImageViewer } from '../image-viewer';
import { List } from '../list';
import { ListItem } from '../list-item';
import { copy } from '../../utils/copy';

import { mdiFolderMultipleImage, mdiCloudUploadOutline, mdiContentCopy, mdiMagnifyPlusOutline, mdiClose } from '@mdi/js'

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    required?: boolean;
    images: string[];
    onCancel: () => void;
    onUpload: (file: File, update: (progress: number) => void) => Promise<void>;
    onComplete: (value: string) => void;
}

interface IUpload {
    src: string;
    name: string;
    size: number;
    progress: number;
}

interface IState {
    working: boolean;
    upload: IUpload | null;
    preview: { src: string } | null;
}

export class ImagePickerDialog extends React.PureComponent<IProps, IState> {

    public state: IState = { working: false, upload: null, preview: null };

    private emptyState() {
        return <div className="ui-image-picker-dialog__content ui-image-picker-dialog__content--empty">
            <Icon size={48} color="rgba(0, 0, 0, 0.3)" path={mdiFolderMultipleImage} />
            <p>No images yet...</p>
        </div>
    }

    private chooseFile = async () => {

        this.setState({ working: true });

        let file: File | undefined;
        try {
            const files = await chooseFiles({ accept: 'image/*', multiple: false });
            file = files[0];
        } catch (e) {
            toast.show({ text: 'Something went wrong' });
        }

        if (file) {
            const src = URL.createObjectURL(file);
            const name = file.name;
            const size = file.size;

            this.setState({ upload: { src, name, size, progress: 0 } });
            try {
                await this.props.onUpload(
                    file,
                    (progress: number) => this.setState({ upload: { src, name, size, progress } })
                );
            } catch (err) {
                toast.show({ text: err });
            };

            URL.revokeObjectURL(src);
        }
        this.setState({ upload: null, working: false });

    }

    private copyURL = async () => {
        if (this.state.preview) {
            try {
                copy(this.state.preview.src);
                toast.show({ text: 'Copied to clipboard' }, 2000);
            } catch (err) {
                toast.show({ text: 'Sorry, could not copy to clipboard' });
            }
        }
    }

    private uploads(upload: IUpload) {
        return <Content className="ui-image-picker-dialog__uploads">
            <div key={upload.src} className="ui-image-picker-dialog__upload">
                <Image className="ui-image-picker-dialog__upload-image" src={upload.src} />
                <Label className="ui-image-picker-dialog__upload-label">
                    <p>{upload.name}</p>
                    <LinearProgress progress={Math.round((upload.progress / upload.size) * 100)} className="ui-image-picker-dialog__upload-progress" />
                    <p>{pretty(upload.progress)} / {pretty(upload.size)}</p>
                </Label>
            </div>
        </Content>
    }

    private images(images: string[]) {
        return <div className={mergeClasses("ui-image-picker-dialog__content", { "ui-image-picker-dialog__content--working": this.state.upload !== null })}>
            <Subheader className="ui-subheader--margin">{pluralize(images.length, 'image', 'images')}</Subheader>
            <div className="ui-image-picker-dialog__images">
                {images.map(src => {
                    return <div className="ui-image-picker-dialog__image" key={src}>
                        <Image onClick={() => this.props.onComplete(src)} className="ui-image-picker-dialog__canvas" src={src} />
                        <div className="ui-image-picker-dialog__image-scrim" />
                        <IconButton onClick={() => this.setState({ preview: { src } })} className="ui-image-picker-dialog__image-expand" path={mdiMagnifyPlusOutline} color="white" />
                    </div>
                })}
            </div>
        </div>
    }

    private closePreview = () => this.setState({ preview: null });

    public render() {
        return <>
            <Card className="ui-dialog__card ui-image-picker-dialog__card">
                <Toolbar>
                    <ToolbarLine>
                        <IconButton disabled={this.state.working} onClick={this.props.onCancel} className="ui-icon-button--margin" path={mdiClose} />
                        <Title>Select Image</Title>
                        <Button disabled={this.state.working} working={this.state.working} onClick={this.chooseFile}>
                            <Icon path={mdiCloudUploadOutline} />
                            <span>Upload</span>
                        </Button>
                    </ToolbarLine>
                </Toolbar>

                {this.state.upload && this.uploads(this.state.upload)}

                {this.props.images.length === 0 && this.emptyState()}
                {this.props.images.length > 0 && this.images(this.props.images)}
            </Card>
            {this.state.preview && <ImageViewer croppable={false} src={this.state.preview.src} onClose={this.closePreview}>
                <List>
                    <ListItem onClick={this.copyURL}>
                        <Label className="ui-image-picker-dialog__label">
                            <p>Download URL</p>
                            <p className="ui-label--clipped">{this.state.preview.src}</p>
                        </Label>
                        <Icon path={mdiContentCopy} />
                    </ListItem>
                </List>
            </ImageViewer>}
        </>
    }
}