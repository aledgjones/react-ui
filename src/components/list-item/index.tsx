// tslint:disable:no-any

import * as React from 'react';
import { Ink } from '..';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    disabled?: boolean;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

export class ListItem extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-list-item', this.props.className, { 'ui-list-item--disabled': this.props.disabled })}
                onClick={this.props.onClick}
            >
                <Ink cover={true} />
                <div className="ui-list-item__content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}