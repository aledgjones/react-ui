import * as React from 'react';
import { DateTime } from 'luxon';
import { mdiClose, mdiCalendarOutline } from '@mdi/js';

import { IconButton } from '..';
import { hasValue, mergeClasses } from '../../utils';
import { Dialog } from '../dialog';
import { DatePickerDialog } from '../date-picker-dialog';

import './styles.css';

export interface IProps {
    id?: string;
    className?: string;
    value?: number;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    onChange: (value?: any) => void;
    onFocus?: () => void;
    onBlur?: () => void;
}

export interface IState {
    focus: boolean;
    touched: boolean;
}

export class DatePicker extends React.PureComponent<IProps, IState> {

    public state: IState = { focus: false, touched: false };

    public hasError() {
        return this.props.required && !hasValue(this.props.value);
    }

    private showDialog = () => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    private hideDialog = () => {
        this.setState({ focus: false });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    private clear = () => {
        this.props.onChange();
    }

    private updateValue = (date: number) => {
        this.props.onChange(date);
        this.hideDialog();
    }

    public render() {

        return <>
            <div className={mergeClasses('ui-date-picker', this.props.className, {
                'ui-date-picker--disabled': this.props.disabled,
                'ui-date-picker--focus': this.state.focus,
                'ui-date-picker--has-value': hasValue(this.props.value),
                'ui-date-picker--invalid': this.hasError(),
                'ui-date-picker--touched': this.state.touched
            })}>

                <div className="ui-date-picker__placeholder">{this.props.placeholder + (this.props.required ? '*' : '')}</div>

                <div className="ui-date-picker__value" onClick={this.showDialog}>{this.props.value ? DateTime.fromMillis(this.props.value).toLocaleString(DateTime.DATE_SHORT) : ''}</div>

                {(hasValue(this.props.value) && !this.props.required) && <IconButton className="ui-date-picker__clear-icon" onClick={this.clear} path={mdiClose} color="rgb(150,150,150)" />}

                {(!hasValue(this.props.value) || !!this.props.required) && <IconButton className="ui-date-picker__open-icon" onClick={this.showDialog} path={mdiCalendarOutline} color="rgb(150,150,150)" />}

                <div className="ui-date-picker__bar" />
            </div >

            <Dialog visible={!!this.state.focus} backdrop={true}>
                <DatePickerDialog value={this.props.value} onCancel={this.hideDialog} onComplete={this.updateValue} />
            </Dialog>
        </>
    }
}