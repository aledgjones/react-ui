import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
}

export class Content extends React.PureComponent<IProps> {
    public render() {
        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-content', this.props.className)}
                style={this.props.style}
            >
                {this.props.children}
            </div>
        );
    }
}