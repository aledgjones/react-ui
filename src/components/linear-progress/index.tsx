import * as React from 'react';
import { mergeClasses } from '../../utils';

import './styles.css';

interface IProps {
    id?: string;
    className?: string;
    progress?: number;
}

export class LinearProgress extends React.PureComponent<IProps> {
    public render() {

        const hasProgress = this.props.progress !== undefined;

        return (
            <div id={this.props.id} className={mergeClasses('ui-linear-progress', { 'ui-linear-progress--animate': !hasProgress }, this.props.className)}>
                <div className="ui-linear-progress__tick ui-linear-progress__tick--one" style={{ width: hasProgress ? `${this.props.progress}%` : undefined }} />
                {!hasProgress && <div className="ui-linear-progress__tick ui-linear-progress__tick--two" />}
            </div>
        );
    }

}
