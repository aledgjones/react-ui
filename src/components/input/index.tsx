// tslint:disable:no-any

import * as React from 'react';
import { CircularProgress } from '..';
import { hasValue, isEmail, isLongEnough, mergeClasses, isNumber, isLessThan, isGreaterThan, isInteger } from '../../utils';

import './styles.css';

type InputType = 'text' | 'email' | 'password' | 'pin' | 'number'

interface IProps {
    id?: string;
    className?: string;
    style?: React.CSSProperties;
    type: InputType;
    value?: any;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    autoComplete?: 'new-password' | 'off';
    allowFloat?: boolean; // if the input is number we can allow floats or not
    min?: number; // minimum number !!inclusive!!
    max?: number;  // max number !!inclusive!!
    units?: string;
    onChange?: (value: string) => void;
    onEnter?: () => void;
    onFocus?: () => void;
    onBlur?: () => void;
    working?: boolean;
}

interface IState {
    focus: boolean;
    touched: boolean;
}

export class Input extends React.PureComponent<IProps, IState> {

    public state: IState = { focus: false, touched: false };

    public onFocus = (e: React.FocusEvent<HTMLInputElement>) => {
        this.setState({ focus: true });
        if (this.props.onFocus) { this.props.onFocus(); }
    }

    public onBlur = (e: React.FocusEvent<HTMLInputElement>) => {
        this.setState({ focus: false, touched: true });
        if (this.props.onBlur) { this.props.onBlur(); }
    }

    public onChange = (e: React.ChangeEvent<HTMLInputElement>) => this.props.onChange && this.props.onChange(e.target.value);

    public onEnter = (e: React.KeyboardEvent<HTMLInputElement>) => e.which === 13 && this.props.onEnter && this.props.onEnter();

    public onClear = () => this.props.onChange && this.props.onChange('');

    public getError() {
        if (this.props.required && !hasValue(this.props.value)) {
            return 'Required';
        }
        if (this.props.type === 'email' && !isEmail(this.props.value)) {
            return 'Must be a valid email';
        }
        if (this.props.type === 'password' && !isLongEnough(this.props.value, 8)) {
            return 'Must be at least 8 charecters long';
        }
        if (this.props.type === 'pin') {
            if (!isNumber(this.props.value)) {
                return 'Must contain only digits';
            }
            if (!isLongEnough(this.props.value, 4)) {
                return 'Must be at least 4 digits long';
            }
        }
        if (this.props.type === 'number') {
            if (!isNumber(this.props.value)) {
                return 'Must be a number';
            }
            if (!this.props.allowFloat && !isInteger(this.props.value)) {
                return 'Must be a integer';
            }
            if ((hasValue(this.props.min) && hasValue(this.props.max)) && (isLessThan(this.props.value, this.props.min) || isGreaterThan(this.props.value, this.props.max))) {
                return `Must be between ${this.props.min} and ${this.props.max} inclusive`;
            }
            if (hasValue(this.props.min) && isLessThan(this.props.value, this.props.min)) {
                return `Must be ${(this.props.min || 0)} or greater`;
            }
            if (hasValue(this.props.max) && isGreaterThan(this.props.value, this.props.max)) {
                return `Must be ${(this.props.max || 0)} or less`;
            }
        }

        return null;
    }

    private inputTypeToType(type: InputType) {
        switch (type) {
            case 'pin':
            case 'password':
                return 'password';
            default:
                return 'text';
        }
    }

    public render() {

        const error = this.getError();

        return (
            <div
                id={this.props.id}
                className={mergeClasses('ui-input', this.props.className, {
                    'ui-input--disabled': this.props.disabled,
                    'ui-input--focus': this.state.focus,
                    'ui-input--has-value': hasValue(this.props.value),
                    'ui-input--invalid': !!error,
                    'ui-input--touched': this.state.touched
                })}
                style={this.props.style}
            >
                <div className="ui-input__wrapper">
                    <p className="ui-input__placeholder">
                        <span>{this.props.placeholder}</span>
                        {!!this.props.required && <span>*</span>}
                    </p>

                    <input
                        type={this.inputTypeToType(this.props.type)}
                        value={this.props.value}
                        onFocus={this.onFocus}
                        onBlur={this.onBlur}
                        onChange={this.onChange}
                        onKeyUp={this.onEnter}
                        disabled={this.props.disabled}
                        autoComplete={this.props.autoComplete}
                    />

                    {this.props.units && <span className="ui-input__units">{this.props.units}</span>}

                    {!!this.props.working && <CircularProgress className="ui-circular-progress--mini ui-circular-progress--primary" />}

                    <div className="ui-input__bar" />

                    {(this.state.touched && !!error) && <div className="ui-input__error">{error}</div>}

                </div>
            </div>
        );
    }
}