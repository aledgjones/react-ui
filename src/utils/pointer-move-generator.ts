type DownCallback<Data> = (e: React.PointerEvent<any>, data: Data | {}) => boolean;
type OtherCallback<Data> = (e: PointerEvent, data: Data | {}) => void;

export function pointerMoveGenerator<Data>(onDown: DownCallback<Data>, onMove: OtherCallback<Data>, onEnd: OtherCallback<Data>, onCancel?: OtherCallback<Data>) {

    let pointerId: number | undefined;
    let data: Data | {} = {};

    const move = (e: PointerEvent) => {
        if (pointerId !== e.pointerId) return;
        onMove(e, data);
    }

    const stop = (e: PointerEvent) => {
        if (pointerId !== e.pointerId) return;

        onEnd(e, data);

        document.removeEventListener('pointermove', move);
        document.removeEventListener('pointerup', stop);
        document.removeEventListener('pointercancel', cancel);

        pointerId = undefined;
    }

    const cancel = (e: PointerEvent) => {
        if (pointerId !== e.pointerId) return;

        if (onCancel) {
            onCancel(e, data);
        } else {
            onEnd(e, data);
        }

        document.removeEventListener('pointermove', move);
        document.removeEventListener('pointerup', stop);
        document.removeEventListener('pointercancel', cancel);

        pointerId = undefined;
    }

    return (e: React.PointerEvent<any>) => {

        if (pointerId && pointerId !== e.pointerId) return;

        pointerId = e.pointerId;

        const proceed = onDown(e, data);

        if (proceed) {
            document.addEventListener('pointermove', move);
            document.addEventListener('pointerup', stop);
            document.addEventListener('pointercancel', cancel);
        } else {
            // we can reurn false from the onDown func to terminate execution
            pointerId = undefined;
        }
    };
}