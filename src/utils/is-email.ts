import { hasValue } from './';

// NB: empty values are valid => ui-required forces non empty values
export function isEmail(val: string): boolean {
    const validEmailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
    if (!hasValue(val) || validEmailRegex.test(val)) {
        return true;
    } else {
        return false;
    }
}