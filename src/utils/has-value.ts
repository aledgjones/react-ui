import { isNull, isUndefined } from 'lodash';

// tslint:disable-next-line:no-any
export function hasValue(val: any, emptyStringIsValue: boolean = false): boolean {
    if (emptyStringIsValue) {
        return !isUndefined(val) && !isNull(val);
    } else {
        return !isUndefined(val) && !isNull(val) && val !== '';
    }
}