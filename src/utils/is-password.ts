import { hasValue, isLongEnough } from './';

// NB: empty values are valid => ui-required forces non empty values
export function isPassword(val: string): boolean {
    if (!hasValue(val) || isLongEnough(val, 8)) {
        return true;
    } else {
        return false;
    }
}