import * as React from 'react';

interface IProps {
    debug?: boolean;
    onResize: (box: {height: number, width: number}) => void;
}

export class ResizeListener extends React.Component<IProps> {

    private iframe: React.RefObject<HTMLIFrameElement> = React.createRef();

    private handleResize = () => {
        if (this.iframe.current) {
            const height = this.iframe.current.scrollHeight;
            const width = this.iframe.current.scrollWidth;
            if (this.props.debug) console.log('height', height, 'width', width);
            this.props.onResize({height, width});
        }
    }

    componentDidMount() {
        if (this.iframe.current && this.iframe.current.contentWindow) {
            this.iframe.current.contentWindow.addEventListener("resize", this.handleResize);
            this.handleResize();
        }
    }

    componentWillUnmount() {
        if (this.iframe.current && this.iframe.current.contentWindow) {
            this.iframe.current.contentWindow.removeEventListener("resize", this.handleResize);
        }
    }

    public render() {
        return (
            <iframe
                title={Math.random().toString()}
                ref={this.iframe}
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    visibility: this.props.debug ? "visible" : "hidden",
                    border: "none",
                    outline: "4px dashed crimson",
                    height: "100%",
                    width: "100%"
                }}
            />
        );
    }
}