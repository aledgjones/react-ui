import { hasValue } from './';

export function isLongEnough(val: string, length: number): boolean {
    if (!hasValue(val) || val.length >= length) {
        return true;
    } else {
        return false;
    }
}