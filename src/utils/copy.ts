export function copy(str: string): Promise<void> {
    const input: any = document.createElement('INPUT');
    document.body.appendChild(input);
    input.style.opacity = 0;
    input.value = str;
    input.select();
    try {
        document.execCommand('copy');
        input.remove();
        return Promise.resolve();
    } catch (err) {
        input.remove();
        return Promise.reject();
    }
}