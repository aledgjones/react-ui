export type Listener<T> = (action: string, state: T) => void;
export type Unlistener = () => void;

export abstract class Fluxify<T> {

    public data: T;
    public debug: boolean = false;
    public listeners: Array<Listener<T>> = [];

    constructor(initialState: T) {
        this.data = initialState;
    }

    public abstract reducer(action: string, payload: any, state: T): T;

    public enableDebugging() {
        this.debug = true;
    }

    public listen(listener: Listener<T>): Unlistener {
        this.listeners.push(listener);
        return () => {
            this.listeners = this.listeners.filter(callback => callback !== listener);
        };
    }

    public emit(action: string, data: T) {
        this.data = data;
        this.listeners.forEach(listener => {
            listener(action, data);
        });
        if (this.debug) {
            console.log(action, data);
        }
    }

    public dispatch(action: string, payload: any) {
        const data = this.reducer(action, payload, this.data);
        this.emit(action, data);
    }

}