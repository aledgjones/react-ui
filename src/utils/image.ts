export interface IBox {
    x: number;
    y: number;
    height: number;
    width: number;
}

export interface IDimensions {
    height: number;
    width: number;
}

export interface IPoint {
    x: number;
    y: number;
}

export type ICrop = [IPoint, IPoint];

export interface IImage {
    src: string;
    crop: ICrop;
}

export function cropChanged(prev: ICrop, next: ICrop) {
    return prev[0].x !== next[0].x || prev[0].y !== next[0].y || prev[1].x !== next[1].x || prev[1].y !== next[1].y;
}

/**
 * Gets a html image element from src
 * useful for images dimentions / size / type etc
 */
export function getImage(src: string) {
    const img = document.createElement('img');
    img.crossOrigin = 'Anonymous';
    return new Promise<HTMLImageElement>((resolve, reject) => {
        img.onload = () => {
            resolve(img);
        }
        img.onerror = () => {
            reject('fail');
        }
        img.src = src;
    });
}

/**
 * Calc crop area size in px
 * NB. Crop marks are based on a percent of original image
 */
export async function getImageSize(image: HTMLImageElement, crop?: ICrop) {
    if (crop) {
        const onePercentWidth = image.width / 100;
        const onePercentHeight = image.height / 100;
        return {
            height: (crop[1].y - crop[0].y) * onePercentHeight,
            width: (crop[1].x - crop[0].x) * onePercentWidth
        }
    } else {
        return {
            height: image.height,
            width: image.width
        }
    }

}

/**
 * Calc crop area points in px
 * NB. Crop marks are based on a percent of original image
 */
export async function getCropPoints(image: HTMLImageElement, crop: ICrop) {
    const onePercentWidth = image.width / 100;
    const onePercentHeight = image.height / 100;
    return [
        {
            x: crop[0].x * onePercentWidth,
            y: crop[0].y * onePercentHeight
        },
        {
            x: crop[1].x * onePercentWidth,
            y: crop[1].y * onePercentHeight
        }
    ]
}

/**
 * generates a cropped version of an image as blob 
 * optionally scales to cover box
 */
export async function getCroppedImage(src: string, crop: ICrop) {

    const isCropped = crop[0].x !== 0 || crop[0].y !== 0 || crop[1].x !== 100 || crop[1].y !== 100;
    if (!isCropped) { return src };

    const image = await getImage(src);
    const cropBox = await getImageSize(image, crop);
    const cropPoints = await getCropPoints(image, crop);

    const canvas = document.createElement('canvas');
    canvas.height = cropBox.height;
    canvas.width = cropBox.width;
    canvas.style.position = 'fixed';
    canvas.style.visibility = 'hidden';

    document.body.appendChild(canvas);

    const ctx = canvas.getContext('2d');
    if (ctx) {
        ctx.drawImage(image, cropPoints[0].x * -1, cropPoints[0].y * -1);
    }

    return new Promise<string>((resolve, reject) => {
        canvas.toBlob((blob) => {
            if (blob) {
                canvas.remove();
                resolve(URL.createObjectURL(blob));
            } else {
                canvas.remove();
                reject('fail');
            }
        }, 'image/png', 0.95);
    });

}