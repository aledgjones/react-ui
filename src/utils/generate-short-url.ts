/**
 * Generate a shortURL from any valid url
 */
export async function generateShortUrl(url: string): Promise<string> {
    const apiKey = '03fb4ea7e2f84df85d16d3cda1e717851fab9ad7';
    const resp = await fetch(`https://api-ssl.bitly.com/v3/shorten?access_token=${apiKey}&longUrl=${encodeURIComponent(url)}`);
    const json = await resp.json();
    return json.data.url;
}