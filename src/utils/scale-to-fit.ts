import { IDimensions } from "./image";

/**
 * We want to fit the child inside the parent.
 * fixed forces a scale up if the child is smaller than the parent else we keep the natural size
 */
export function scaleToFit(child: IDimensions, parent: IDimensions, fixed = false): number {
    const childAspect = child.width / child.height;
    const parentAspect = parent.width / parent.height;

    let scale = 1;

    if (parentAspect > childAspect) {
        // shrink along height
        if (fixed || child.height > parent.height) {
            scale = parent.height / child.height;
        }
    } else {
        // shrink along width
        if (fixed || child.width > parent.width) {
            scale = parent.width / child.width;
        }
    }

    return scale;
}