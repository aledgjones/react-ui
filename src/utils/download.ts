export async function download(url: string, name: string): Promise<void> {

    // external urls aren't allowed in downlad attr so create a local one
    const response = await fetch(url);
    const file = await response.blob();
    const localUrl = URL.createObjectURL(file);

    // yes we need to append else it doesn't work
    const a: any = document.createElement('A');
    document.body.appendChild(a);
    a.style.position = 'fixed';
    a.style.visibility = 'hidden';
    a.style.left = '-100000px';
    a.style.top = '-100000px';
    a.href = localUrl;
    a.download = name;

    try {
        a.click();
        a.remove();
        URL.revokeObjectURL(localUrl);
        return;
    } catch (err) {
        a.remove();
        URL.revokeObjectURL(localUrl);
        throw 'fail';
    }

}