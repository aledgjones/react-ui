import { hasValue, isLongEnough } from './';

// NB: empty values are valid => ui-required forces non empty values
export function isPin(val: string): boolean {
    if (!hasValue(val) || isLongEnough(val, 4)) {
        return true;
    } else {
        return false;
    }
}